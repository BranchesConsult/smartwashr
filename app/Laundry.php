<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Laundry extends Model {

    protected $table = 'laundry';
    protected $fillable = [
        'owner_id', 'status', 'user_entered_address', 'code', 'phone', 'email', 'zip', 'logo', 'name', 'location', 'st2', 'st1', 'state', 'country', 'city', 'latitude', 'longitude'
    ];

//    public function user() {
//        return $this->belongsTo("App\User", 'owner_id');
//    }
     public function newQuery($excludeDeleted = true) {
        $id= \Auth::id();
        $user=\App\User::find($id);
        if($user->is_admin==1)
        {  
            return parent::newQuery($excludeDeleted);
        }
        else {
            return parent::newQuery($excludeDeleted)->where('owner_id', '=', $id);
        }
    }
    public function orders() {
        return $this->hasMany("App\Orders");
    }

    public function product() {
        return $this->hasMany("App\Product", 'laundry_id');
    }
    public function laundry_users() {
        return $this->belongsToMany("App\User", 'laundry_driver', 'laundry_id');
    }

}
