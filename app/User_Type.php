<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Type extends Model {

    protected $table = 'user_types';
    protected $fillable = [
        'user_id', 'status'
    ];

    public function user() {
        return $this->belongsTo("App\User", 'user_id');
    }

}
