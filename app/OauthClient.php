<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
   protected $table = 'oauth_clients';
   protected $fillable = [
        'name',
        'user_id',
        'secret',
        'redirect',
        'personal_access_client',
        'revoked', 
        'password_client'
    ];
}
