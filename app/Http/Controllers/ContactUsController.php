<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use App\User;
class ContactUsController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        return view('adminlte::home');
    }

    function contactUs() {
        $data = array(
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'subject' => Input::get('subject'),
            'message' => Input::get('message'),
        );
        $message = [
            'body' => $data['message']
        ];
        $this->sendEmail($message, $data);
        return response()->json([
                    'success' => true, 'message' => 'Email send'
                        ], 200);
    }

    private function sendEmail($data, $input) {
        Mail::send('emails.contactus', $data, function($message) use ($input) {
            $subject = $input['subject'];
            $message->from($input['email']);
            $message->to(['umair_hamid100@yahoo.com'])->subject($subject);//'suhail@smartwashr.com'
            $message->replyTo($input['email'], $input['name']);
        });
    }

    public function activate($code, User $user) {
        if ($user->activateAccount($code)) {
            return redirect('/');
        }
        return response()->json(['success' => false], 401);
    }

}
