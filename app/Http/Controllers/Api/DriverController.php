<?php

namespace App\Http\Controllers\Api;

use App\Orders;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class DriverController extends Controller
{

    public function create()
    {
    }

    public function store()
    {
    }

    public function edit()
    {
    }

    public function update()
    {
    }

    public function destroy()
    {
    }

    public function index()
    {
        $driver = \App\User_Type::with('user')
            ->where('type_id', '2')
            ->get();
        if (!empty($driver)) {
            $driver->toArray();
        }
        return response()->json(['Drivers' => $driver], 200);
    }

    public function searchOrder()
    {

        $id = \Auth::id();
        $status = Input::get('status');
        $date = Input::get('date');
        $datetime = new \DateTime($date);
        $date = $datetime->format('Y-m-d');
        //dd($date);
        $orders = \App\Orders::where('driver_id', $id)->where('status', $status)
            ->where('collection_date_time', '=', $date)->get();
        return response()->json(['message' => 'Order fetched successfully'
            , 'Orders List' => $orders], 200);
    }

    public function show($id)
    {
        $driver = \App\User_Type::with('user')->where('type_id', '2')->where('user_id', $id)->get();
        if (!empty($driver)) {
            $driver->toArray();
        }
        return response()->json(['Drivers' => $driver], 200);
    }

    public function orderHistory()
    {

        $id = \Auth::id();
        $orders = \App\Orders::where('driver_id', $id)->get();
        if (!empty($orders)) {
            $orders = $orders->toArray();
        }
        return response()->json(['Orders List' => $orders], 200);
    }

    public function clientLocation($order_id)
    {
        $user = \App\Orders::with('user')->where('id', $order_id)->get();
        if (!empty($user)) {
            $user = $user->toArray();
        }
        return response()->json(['Client info' => $user], 200);
    }

    public function driverOrders(Request $request, $status_id = 0)
    {
        $driverId = \Auth::user()->id;
        $statusId = $request->get('status_id') ? $request->get('status_id') : $status_id;
//        $driverOrders = \DB::select(\DB::raw("
//            SELECT orders.*, users.name, users.email, users.phone_number FROM orders orders
//             LEFT JOIN users users ON users.id = orders.user_id
//            where
//            `driver_id` = $driverId
//            and `status` = $statusId
//            ORDER  BY orders.id DESC
//        "));
        $driverOrders = Orders::with('user')
            ->where('driver_id', $driverId)
            ->where('status', $statusId)
            ->orderBy('id', 'DESC')
            ->get();
        //$driverOrders['client_name'] = User::find($driverOrders['user_id'])->name;
//        $driverOrders = Orders::where('driver_id', \Auth::user()->id)
//            ->where('status', "$statusId")
//            ->toSql();

        return response()->json([
            'driver_order' => $driverOrders
        ]);
    }
}
