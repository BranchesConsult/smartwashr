<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ChangePasswordApiWhenLogin;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\SocialLogin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use \JWTAuth;
use App\User;
use App\User_Type;
use Namshi\JOSE\JWT;
use Validator;
use Mail;
use URL;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {

    }

    public function store()
    {

    }

    public function show()
    {

    }

    public function edit()
    {

    }

    public function destroy()
    {

    }

    /**
     * Register
     *
     * Register a new user into our system.
     *
     */
    public function create(RegistrationRequest $request)
    {
        //return response()->json($request->all());

        $pic = '';
        if ($request->hasFile('pic')) {
            $pic = URL::to('/uploads') . ('/') . uploadInputs($request->file('pic'), 'pics', 'public');
        }
//        return response()->json([
//            'pic' => $pic
//        ]);
        $newUser = $this->user->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'phone_number' => $request->get('phone_number'),
            'profile_pic' => $pic,
            'active' => 1
        ]);
        $user_type = new \App\User_Type;
        $user_type->type_id = '3';
        $user_type->user_id = $newUser->id;
        $user_type->save();
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];
        if (!$newUser) {
            return response()->json(['failed_to_create_new_user'], 500);
        }
        $user = User::where('id', $newUser->id)->get();
        Mail::send('emails.register', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])
                ->bcc('umair_hamid100@yahoo.com')
                ->subject('SmartWashr Registration');
        });

        //Login the user
        $user = User::where('id', $newUser->id)->first();
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json([
                    'status' => 'fail', 'error' => 'Invalid password or email provided'], 401);
            } else {
                $userObj = User::find($user->id);
                $userObj->last_login = Carbon::now();
                if ($request->has('device_token') && !empty($request->get('device_token'))) {
                    $userObj->device_token = $request->get('device_token');
                }
                $userObj->save();
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'fail', 'error' => 'could_not_create_token'], 500);
        }
        // all good so return the token

        $status = 'success';
        $active_orders = \App\Orders::where('user_id', $user['id'])
            ->where('status', '!=', '4')->where('status', '!=', '5')->with('order_detail')
            ->get()->count();
        //$user = array_merge($user, $order);
        return response()->json(compact('status', 'token', 'user', 'active_orders'));
    }

    /**
     * Login user
     *
     * Login a user and return his token which will be user with header Bearer {{token}}
     *
     */
    public function authenticate(\App\Http\Requests\LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'fail', 'error' => 'Invalid password or email provided'], 401);
            } else {
                $userObj = User::find(Auth::id());
                $userObj->last_login = Carbon::now();
                if ($request->has('device_token') && !empty($request->get('device_token'))) {
                    $userObj->device_token = $request->get('device_token');
                }

                $userObj->save();
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'fail', 'error' => 'could_not_create_token'], 500);
        }
        // all good so return the token
        if (\Auth::user()->active == 0) {
            return response()->json([
                'status' => 'fail', 'error' => 'Verify your email first'], 401);
        }
        $status = 'success';
        //$user = \Auth::id();
        $user = User::find(Auth::id())->toArray();
        $active_orders = \App\Orders::where('user_id', $user['id'])
            ->where('status', '!=', '4')
            ->where('status', '!=', '5')
            ->with('order_detail')
            ->get()
            ->count();
        //$user = array_merge($user, $order);
        return response()->json(compact('status', 'token', 'user', 'active_orders'));
    }

    /**
     * Change user's password
     *
     * This will use to change the pasword of user.
     *
     */
    function changePassword(\App\Http\Requests\ChangePasswordRequest $request)
    {
        $data = $request->all();
        $user = User::where('email', $data['email'])->get();
        //Changing the password only if is different of null
        if (isset($data['oldPassword']) && !empty($data['oldPassword']) && $data['oldPassword'] !== "" && $data['oldPassword'] !== 'undefined') {
            //checking the old password first
            $check = Auth::guard('web')->attempt([
                'email' => $user[0]->email,
                'password' => $data['oldPassword']
            ]);
            if ($check && isset($data['newPassword']) && !empty($data['newPassword']) && $data['newPassword'] !== "" && $data['newPassword'] !== 'undefined') {
                $user[0]->password = bcrypt($data['newPassword']);
                //Changing the type
                $user[0]->save();
                return json_encode(['status' => true, "message" => "Password changed successfully"], 200); //sending the new token
            } else {
                return response()->json(['status' => false, "message" => 'Wrong password information'], 401);
            }
        }
        return response()->json(['status' => false, "message" => 'Wrong password information'], 401);
    }

    public function update(Request $request, $id)
    {
        $user = \App\User::find($id);
        $user->fill($request->all());
        $user->save();
        return response()->json(['message' => "Profile Updated successfully"], 200);
    }

    public function getNearByDriver(Request $request)
    {
        $drivers = [];
        $latitude = $request->get('latitude'); //21.286875556934202;
        $longitude = $request->get('longitude'); //39.33605316406249;
        $user = \App\User::where('ne_latitude', '>=', $latitude)
            //->has('user_laundry')
            ->where('sw_latitude', '<=', $latitude)
            ->where('ne_longitude', '>=', $longitude)
            ->where('sw_longitude', '<=', $longitude)
            //->select(['id'])
            ->with(['user_type' => function ($q) {
                return $q->where('type_id', '2');
            }, 'user_laundry'])
            ->get();
        if (($user->isEmpty())) {
//            if (mail('umair_hamid100@yahoo.com', 'SW Failiure', 'lat => ' . $latitude . '<br/> lng => ' . $longitude . '<br/> user => ' . Auth::user()->email)) {
//
//            }
            return response()->json([
                'error' => 'Sorry! Currently we are not operating in your area.'
            ]);
        }
        $hasUserLaundry = \DB::table('laundry_driver')->where('driver_id', $user[0]['id'])->get();
        if ($hasUserLaundry->isEmpty()) {
            return response()->json([
                'error' => 'Sorry! Currently we are not operating in your area.'
            ]);
        }
        return response()->json([
            'driver_id' => $user[0]['id'],
            'laundry_id' => $hasUserLaundry[0]->laundry_id
        ]);
    }

    /**
     * Change pasword
     */
    public function changePasswordWhenLogin(ChangePasswordApiWhenLogin $request)
    {
        $user = \Auth::user();
        $current_password = $request->get('oldPassword');
        $password = bcrypt($request->get('newPassword'));
        $user_count = \DB::table('users')->where('id', $user->id)->count();
        if (\Hash::check($current_password, $user->password) && $user_count == 1) {
            $user->password = $password;
            try {
                $user->save();
                $flag = TRUE;
            } catch (\Exception $e) {
                $flag = FALSE;
            }
            if ($flag) {
                return response()->json([
                    'message' => 'Password changed successfully'
                ]);
            } else {
                return response()->json([
                    'error' => 'Unable to process request this time'
                ]);
            }
        } else {
            return response()->json([
                'error' => 'Your current password do not match our record'
            ]);
        }
    }

    public function resendEmail(\App\Http\Requests\ResetPasswordRequest $request)
    {
        $email = $request->get('email');
        $user = \App\User::where('email', $email)->get()->toArray();
        //dd(count($user));
        if ((count($user)) != 0) {
            $data = [
                'email' => $email
            ];
            //dd($user->toArray());
            Mail::send('emails.verificationEmail', $data, function ($message) use ($data) {
                // $message->from('noukripk2017@gmail.com', 'Smart Washr');
                $message->to($data['email'])->subject('SmartWashr Registration!');
            });
            return response()->json([
                'message' => 'success'
            ]);
        } else {
            return response()->json([
                'error' => 'Email not found'
            ]);
        }
    }

    function updateProfile(Request $request)
    {
        $user_id = \Auth::id();
        $user = \App\User::find($user_id);
        if (!empty($request->get('name'))) {
            $user->name = $request->get('name');
        }
        if (!empty($request->get('phone'))) {
            $user->phone_number = $request->get('phone');
        }
        if ($request->hasFile('pic')) {
            $pic = URL::to('/uploads') . ('/') . uploadInputs($request->file('pic'), 'pics', 'public');
            $user->profile_pic = $pic;
        }
        if (!empty($request->get('name')) || !empty($request->get('phone'))) {
            $user->save();
        }
        return response()->json([
            'user' => $user
        ], 200);
    }

    public function deleteUser()
    {
        User::find(Auth::id())->delete();
        \JWTAuth::parseToken()->refresh();
        return response()->json([
            'success' => 'User deleted.'
        ]);
    }

    public function userRegisterOrLogin(SocialLogin $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if (empty($user)) {
            $userArr = [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'phone_number' => $request->get('phone_number'),
                'profile_pic' => $request->get('pic'),
                'active' => 1,
            ];
            if ($request->has('device_token') && !empty($request->get('device_token'))) {
                $userArr['device_token'] = $request->get('device_token');
            }
            $newUser = $this->user->create($userArr);
            $user_type = new \App\User_Type;
            $user_type->type_id = '3';
            $user_type->user_id = $newUser->id;
            $user_type->save();
            $data = [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ];
            if ($request->has('device_token') && !empty($request->get('device_token'))) {
                $data['device_token'] = $request->get('device_token');
            }
            if (!$newUser) {
                return response()->json(['failed_to_create_new_user'], 500);
            }
            $user = User::where('email', $request->get('email'))->first();
            Mail::send('emails.register', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject('SmartWashr Registration');
            });
        }
        //Login the user
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json([
                    'status' => 'fail', 'error' => 'Invalid password or email provided'], 401);
            } else {
                $userObj = User::find($user->id);
                $userObj->last_login = Carbon::now();
                if ($request->has('device_token') && !empty($request->get('device_token'))) {
                    $userObj->device_token = $request->get('device_token');
                }
                $userObj->save();
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'fail', 'error' => 'could_not_create_token'], 500);
        }
        // all good so return the token

        $status = 'success';
        $active_orders = \App\Orders::where('user_id', $user['id'])
            ->where('status', '!=', '4')->where('status', '!=', '5')->with('order_detail')
            ->get()->count();
        //$user = array_merge($user, $order);
        return response()->json(compact('status', 'token', 'user', 'active_orders'));
    }
}