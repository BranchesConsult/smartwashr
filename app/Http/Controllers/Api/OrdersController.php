<?php

namespace App\Http\Controllers\Api;

use App\Coupon;
use App\Http\Requests\AddOrderComplainRequest;
use App\Http\Requests\ApplyCouponRequest;
use App\Order;
use App\OrderComplain;
use App\OrderComplainImage;
use App\Orders;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mail;
use Auth;
use Namshi\JOSE\JWT;
use PDF;
use Date;
use App\Orders_detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
////FCM
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class OrdersController extends Controller
{
    public function index()
    {
    }

    public function delieveredOrders()
    {
        $user_id = \Auth::id();
        $orders = Orders::where('user_id', $user_id)
            ->where('status', '=', '4')
            ->orderBy('id', 'desc')
            ->get();
        // dd($orders-)
        $active_orders = $orders->where('status', '=', '4')
            ->count();
        return response()->json(['message' => "Orders fetched successfully",
            'user_order' => $orders,
            'active_orders' => $active_orders], 200);
    }

    public function orderHistory()
    {
        //dd(\Auth::id());
        $user_id = \Auth::id();
        $orders = Orders::where('user_id', $user_id)
            ->where('status', '!=', '5')
            ->where('status', '!=', '4')
            ->orderBy('id', 'desc')
            ->get();
        // dd($orders-)
        $active_orders = $orders->where('status', '!=', '4')
            ->where('status', '!=', '5')
            ->count();
        return response()->json(['message' => "Orders fetched successfully",
            'user_order' => $orders,
            'active_orders' => $active_orders], 200);
    }

    public function store(\App\Http\Requests\OrderAddRequest $request)
    {
        // $product = json_decode($request, true);
        \Log::info(print_r($request->all(), true));
        $order = new Orders;
        $orders_detail = new Orders_detail;
        $price = 0;
        $user_id = \Auth::id();
        $sw_price = 0;
        $dataSet = [];
        $product = $request->toArray();
        //dd($product);
        $order->post_code = $product['post_code'];
        $order->user_id = $user_id;
        $order->laundry_id = $product['laundry_id'];
        $order->driver_id = $product['driver_id'];
        $order->address_one = $product['address_one'];
        $order->user_phone = \Auth::user()->phone_number;
        $order->address_two = $product['address_two'];
        $order->user_email = \Auth::user()->email;
        $order->discount = 0;
        $order->discount_type = isset($product['discount_type']) ? $product['discount_type'] : 0;
        $order->delivery_instruction = $product['delivery_instruction'];
        $order->collection_date_time = date('Y-m-d H:i:s', strtotime($product['collection_date_time']));
        $order->collection_date_time_to = date('Y-m-d H:i:s', strtotime($product['collection_date_time_to']));
        $order->delivery_date_time = date('Y-m-d H:i:s', strtotime($product['delivery_date_time']));
        $order->latitude = $product['latitude'];
        $order->longitude = $product['longitude'];
        //After added coupon and address
        if (isset($product['coupon_code']) && !empty($product['coupon_code'])) {
            $order->coupon_code = $product['coupon_code'];
            $couponInfo = $this->negateDiscount($product['coupon_code'], $product['discounted_price']);
            $order->discount = $couponInfo['coupon']['discount'];
            $order->discount_type = $couponInfo['coupon']['discount_type'];
        }
        //End added coupon
        $order->save();
        //Updata device token
        if (!empty($product['device_token'])) {
            $client = User::where('id', Auth::id());
            $client->device_token = $product['device_token'];
            $client->save();
        }
        foreach ($product['products_order'] as $row) {
            $commission = ($row['sw_price'] * $row['quantity']) - ($row['price'] * $row['quantity']);
            if ($commission < 0) {
                $commission = abs($commission);
            }
            $dataSet[] = [
                'order_id' => $order->id,
                'product_id' => $row['product_id'],
                'product_service' => $row['service'],
                'quantity' => $row['quantity'],
                'price' => $row['price'] * $row['quantity'],
                'sw_price' => $row['sw_price'] * $row['quantity'],
                'commission' => $commission
            ];
            $price = $price + ($row['price'] * $row['quantity']);
            $sw_price = $sw_price + ($row['sw_price'] * $row['quantity']);
        }

        DB::table('orders_detail')->insert($dataSet);
        $order->total_price = $price + config('const.delivery_charges');
        $order->sw_total_price = $sw_price + config('const.delivery_charges');//This is discounted price
        $order->sw_total_orignal_price = (isset($product['discounted_price']) && !empty($product['discounted_price'])) ? $product['discounted_price'] + config('const.delivery_charges') : $sw_price + config('const.delivery_charges');//This is discounted price
        $order->delivery_charges = config('const.delivery_charges');
        $commission = $sw_price - $price;
        if ($commission < 0) {
            $commission = abs($commission);
        }
        $commission = $commission + config('const.delivery_charges_commission');
        $order->commission = $commission;
        $order->delivery_charges_commission = config('const.delivery_charges_commission');
        $order->save();
        $pdf = \App\Orders_detail::where('order_id', $order->id)
            ->with('product')
            ->get()
            ->toArray();
        $this->sendReceipt($pdf, $order->id);
        $usersOrder = Orders::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->get();
        $active_orders = $usersOrder->where('status', '!=', '4')->where('status', '!=', '5')->count();
        $this->sendPushMessage($product['driver_id']);
        return response()->json([
            'message' => "Orders Created successfully",
            'user_order' => $usersOrder,
            'active_orders' => $active_orders
        ], 200);
    }

    public function email($id)
    {
        $user = \App\User::find($id);
        $data = [
            'name' => $user->name,
            'email' => $user->email,
        ];
        Mail::send('emails.orderReceived', $data, function ($message) use ($data) {
            $subject = 'Order Received';
            $message->to($data['email'])
                ->subject($subject)
                ->bcc(['umair_hamid100@yahoo.com']);
            //  $message->replyTo($input['email'], $input['name']);
        });
    }

    function sendReceipt($pdf, $id)
    {
        $email = \Auth::user()->email;
        $data['email'] = $email;
        $data['pdf'] = $pdf;
        $data['id'] = $id;
        $pdf = \PDF::loadView('emails.receipt', $data);
        Mail::send('emails.orderPlaced', $data, function ($message) use ($pdf, $data) {
            $message->from('info@smartwashr.com', 'Smart Washr');
            $message->to($data['email'])->subject('Invoice')
                ->bcc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']);
            $message->attachData($pdf->output(), 'receipt.pdf');
        });
    }

    public function show(Request $request)
    {
        $orderId = $request->get('id');
        try {
            $order = \App\Orders::with('order_detail.product')
                ->where('id', $orderId)
                ->firstOrFail();
            return response()->json([
                'orders' => $order
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([], 200);
        }

    }

    public function getById(Request $request, $orderId)
    {
        //$orderId = $request->get('id');
        try {
            $order = Order::with('order_detail.product')
                ->where('id', $orderId)
                ->firstOrFail();
            $order['order_id'] = str_pad($order['id'], 6, "0", STR_PAD_LEFT);
            return response()->json(['orders' => $order], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([], 200);
        }
    }


    public function cancelOrder(Request $request)
    {
        $order_id = $request->get('id');
        Orders::where('id', $order_id)->update(['status' => 5]);
        $usersOrder = Orders::where('user_id', Auth::id())
            ->where('status', '!=', '5')
            ->orderBy('id', 'desc')
            ->get();
        $active_orders = $usersOrder->where('status', '!=', '4')->where('status', '!=', '5')->count();
        return response()->json([
            'message' => 'Order cancelled!',
            'user_order' => $usersOrder,
            'active_orders' => $active_orders
        ], 200);

    }

    public function updateOrderStatus(Request $request)
    {
        $statusId = $request->get('status_id');
        $orderId = $request->get('order_id');
        $order = Order::find($orderId);
        $order->status = $statusId;
        $order->save();

        $this->orderUpdatePushMesage($orderId);

        return response()->json([
            'messsage' => 'Order updated successfuly!'
        ]);
    }

    public function create()
    {

    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }

    public function rescheduleOrder(Request $request)
    {
        $orderId = $request->get('order_id');
        $order_date_time = date('Y-m-d h:i:s', strtotime($request->get('delivery_date_time')));
        $customerId = Auth::id();
        $customerDeviceToken = User::find($customerId)->device_token;
        $order = Orders::find($orderId);
        $order->delivery_date_time = $order_date_time;
        $order->save();
        return response()->json([
            'success' => 'true'
        ]);
    }

    public function sendPushMessage($driver_id)
    {
        $driver = \App\User::find($driver_id);
        $order = Orders::where('driver_id', $driver_id)
            ->where('status', '0')->get()->count();
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('Smart Washr');
        $notificationBuilder->setBody('Order Notification')
            ->setSound('default');
        $datatest = ["title" => "Pending Order", "message" => "Orders pending for Pick Up", "count" => $order];
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(["data" => $datatest]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        if (empty($driver->device_token)) {
            $driver->device_token = 't65787bt87t87t87t7bt879t87t7tb79bt78bt97t';
        }
        $token = $driver->device_token;
        return $downstreamResponse = response()->json(['notification' => FCM::sendTo($token, $option, $notification, $data)]);
    }


    public function orderUpdatePushMesage($orderId)
    {
        $orderStatuses = [
            0 => 'You order is now Pending',
            1 => 'You order has been Received',
            2 => 'Your laundry is getting washed',
            3 => 'You order has been Completed and will be delivered soon',
            4 => 'You order has been Delivered, Thank you for your loyalty',
            5 => 'You order has been Cancelled',
            6 => 'You order is Ready For Delivery',
        ];
        $orderDetail = Orders::where('id', $orderId)->with('user', 'order_detail')
            ->first()
            ->toArray();
        $device_token = $orderDetail['user']['device_token'];
        if (!empty($device_token)) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('Smart Washr');
            $notificationBuilder
                ->setClickAction('OrderDetail')
                ->setBody($orderStatuses[$orderDetail['status']])
                ->setSound('default');
            $datatest = ["order_id" => $orderId];
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(["data" => $datatest]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            $token = $device_token;
            return $downstreamResponse = response()->json(['notification' => FCM::sendTo($token, $option, $notification, $data)]);
        } else {
            /**
             * \Mail::send('emails.statusUpdate', $data, function ($message) use ($data) {
             * // $message->from('info@smartwashr.com', 'Smart Washr');
             * $message->subject('Order Status Updated');
             * $message->to($data['email'])
             * ->cc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']);
             * });
             */
        }
    }

    public function testReciept()
    {
        return view('emails.register');
    }

    public function addOrderComplain(AddOrderComplainRequest $request)
    {
        //dd($request->all());
        //Saving order
        $orderComplain = new OrderComplain();
        $orderComplain->order_id = ltrim('0', $request->get('order_id'));
        $orderComplain->complain_desc = $request->get('complain_desc');
        $orderComplain->save();

        //Savig its images
        $fileUrl = [];
        $uploadedFiles = [];
        if ($request->hasFile('complain_pic')) {
            $uploadFilesArr = [];
            $uploadedFiles = uploadComplainInput($request->file('complain_pic'), 'complain');
            foreach ($uploadedFiles as $key => $val) {
                $uploadFilesArr[] = [
                    'complain_id' => $orderComplain->id,
                    'image_url' => $val
                ];
            }
            OrderComplainImage::insert($uploadFilesArr);
        }
        //dd($uploadedFiles);

        \Mail::send('emails.email_order_complain', array('feedback' => $request->get('complain_desc')),
            function ($message) use ($request, $uploadedFiles) {
                $message
                    ->to(['umair_hamid100@yahoo.com', 'Info@smartwashr.com'])
                    ->subject('Order Complain ' . $request->get('order_id') . ' || SW')
                    ->replyTo(Auth::user()->email);
                foreach ($uploadedFiles as $key => $val) {
                    //$message->attach($val);
                }
            });

        return response()->json([
            'success' => true
        ]);

        //email_order_complain
    }


    public function apply_coupon(ApplyCouponRequest $request)
    {
        $price = $request->get('price');
        $code = $request->get('code');
        $discount = $this->isValidCoupon($code, $price);
        $msg = ($discount > 0) ? 'Coupon applied successfully' : 'Coupon has been expired or wrong code';
        $success = ($discount > 0) ? true : false;
        return response()->json(
            [
                'message' => $msg,
                'new_price' => (!$discount) ? number_format(0.00) : $discount,
                'success' => $success
            ]
        );
    }

    public function isValidCoupon($coupon_code, $actualPrice)
    {
        $coupon = Coupon::where('code', $coupon_code)
            ->select('discount', 'discount_type')
            ->where('valid_from', '<=', date('Y-m-d h:i:s'))
            ->where('valid_to', '>=', date('Y-m-d h:i:s'))
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->first();

        $discount = 0;
        if (!empty($coupon)) {
            if ($coupon['discount_type'] == Coupon::PERCENTAGE)
                $discount = $actualPrice - ($actualPrice * ($coupon['discount'] / 100));
            else
                $discount = $actualPrice - $coupon['discount'];
        }
        $discount = round($discount, 0);
        return ($discount == 0) ? false : number_format($discount, 2, '.', '');
    }

    public function negateDiscount($coupon_code, $discountPrice)
    {
        $coupon = Coupon::where('code', $coupon_code)
            ->select('discount', 'discount_type')
            ->where('valid_from', '<=', date('Y-m-d h:i:s'))
            ->where('valid_to', '>=', date('Y-m-d h:i:s'))
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->first()->toArray();
        if (!empty($coupon)) {
            $data['coupon'] = $coupon;
            if ($coupon['discount_type'] == Coupon::PERCENTAGE) {
                $actualPrice = $discountPrice + ($discountPrice * ($coupon['discount'] / 100));
            } else {
                $actualPrice = $discountPrice + $coupon['discount'];
            }
            $data['coupon']['actual_price'] = $actualPrice;
            return $data;
        }
        return false;
    }
}
