<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller {

    protected function authenticateClient(Request $request) {

        $credentials = $this->credentials($request);

        $data = $request->all();

        $user = User::where('email', $credentials['email'])->first();

        $request->request->add([
            'grant_type' => $data['grant_type'],
            'client_id' => $data['client_id'],
            'client_secret' => $data['client_secret'],
            'username' => $credentials['email'],
            'password' => $credentials['password'],
            'scope' => null,
        ]);

        $proxy = Request::create(
                        'oauth/token', 'POST'
        );

        return Route::dispatch($proxy);
    }

    protected function authenticated(Request $request, $user) {

        return $this->authenticateClient($request);
    }

    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user());
    }

    public function login(Request $request) {
        if ($this->guard('api')->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }
    }

}
