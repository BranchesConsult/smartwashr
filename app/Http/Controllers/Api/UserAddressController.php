<?php

namespace App\Http\Controllers\Api;

use App\AddressUser;
use App\Http\Requests\UserAddressAddRequest;
use App\Http\Requests\VoteAddressRequest;
use App\User;
use App\VotedAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PayPal\Api\Address;

class UserAddressController extends Controller
{
    public $user_id;

    public function __construct()
    {
        $this->user_id = \Auth::id();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usersAddress = AddressUser::where('user_id', \Auth::id())->get();
        return response()->json(['success' => true, 'addresses' => $usersAddress]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAddressAddRequest $request)
    {
        $input['address_line_1'] = $request->get('address_line_1');
        $input['address_line_2'] = $request->get('address_line_2');
        $input['name'] = $request->get('name');
        $input['building_name'] = $request->get('building_name');
        $input['phone'] = $request->get('phone');
        $input['lat'] = $request->get('lat');
        $input['lng'] = $request->get('lng');
        $input['user_id'] = \Auth::id();
        AddressUser::create($input);
        return response()->json([
            'success' => true,
            'address_stored' => $input
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usersAddress = AddressUser::where('user_id', \Auth::id())->get();
        return response()->json(['success' => true, 'addresses' => $usersAddress]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserAddressAddRequest $request, $id)
    {
        $address = Address::find($id);
        $address->address_line_1 = $request->get('address_line_1');
        $address->address_line_2 = $request->get('address_line_2');
        $address->name = $request->get('name');
        $address->building_name = $request->get('building_name');
        $address->phone = $request->get('phone');
        $address->lat = $request->get('lat');
        $address->lng = $request->get('lng');
        $address->user_id = \Auth::id();
        $address->save();
        return response()->json([
            'success' => true,
            'address_stored' => $address
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateManual(UserAddressAddRequest $request, $id)
    {
        $address = AddressUser::find($id);
        $address->address_line_1 = $request->get('address_line_1');
        $address->address_line_2 = $request->get('address_line_2');
        $address->name = $request->get('name');
        $address->building_name = $request->get('building_name');
        $address->phone = $request->get('phone');
        $address->lat = $request->get('lat');
        $address->lng = $request->get('lng');
        $address->user_id = \Auth::id();
        $address->save();
        return response()->json([
            'success' => true,
            'address_stored' => $address
        ]);
    }

    public function update(UserAddressAddRequest $request, $id)
    {
        //dd($request->all());
        $address = AddressUser::find($id);
        $address->address_line_1 = $request->get('address_line_1');
        $address->address_line_2 = $request->get('address_line_2');
        $address->name = $request->get('name');
        $address->building_name = $request->get('building_name');
        $address->phone = $request->get('phone');
        $address->lat = $request->get('lat');
        $address->lng = $request->get('lng');
        $address->user_id = \Auth::id();
        $address->save();
        return response()->json([
            'success' => true,
            'address_stored' => $address
        ]);
    }


    public function updateWithId(UserAddressAddRequest $request)
    {
        //dd($request->all());
        $id = $request->get('address_id');
        $address = AddressUser::find($id);
        $address->address_line_1 = $request->get('address_line_1');
        $address->address_line_2 = $request->get('address_line_2');
        $address->name = $request->get('name');
        $address->building_name = $request->get('building_name');
        $address->phone = $request->get('phone');
        $address->lat = $request->get('lat');
        $address->lng = $request->get('lng');
        $address->user_id = \Auth::id();
        $address->save();
        return response()->json([
            'success' => true,
            'address_stored' => $address
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AddressUser::where('id', $id)->where('user_id', \Auth::id())->delete();
        return response()->json([
            'success' => true,
            'addresses' => AddressUser::where('user_id', \Auth::id())->get()
        ]);
    }

    /**
     * Vote my address
     * @return \Illuminate\Http\Response
     */
    public function voteAddress(VoteAddressRequest $request)
    {
        $user_id = \Auth::id();
        $votedAddress = new VotedAddress();
        $votedAddress->address = $request->get('address');
        $votedAddress->lat = $request->get('lat');
        $votedAddress->lng = $request->get('lng');
        $votedAddress->user_id = $user_id;
        $votedAddress->save();
        return response()->json([
            'success' => true
        ]);
    }
}
