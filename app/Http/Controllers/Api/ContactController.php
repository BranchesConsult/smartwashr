<?php

namespace App\Http\Controllers\Api;

use App\Page;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function ContactFeed(Request $request)
    {
        $userId = \Auth::id();
        $user = User::find($userId);
        $feedbackDesc = $request->get('desc');
        $images = $request->file('feedback_pics');
        \Mail::send('emails.email_feedback', array('feedback' => $feedbackDesc), function ($message) use ($user, $request) {
            $message->from($user->email, $user->name)
                ->to(['umair@branchezconsulting.com', 'umair_hamid100@yahoo.com',
                    'Info@smartwashr.com'])
                ->subject('Client Feedback || SW')
                ->replyTo($user->email, $user->name);
            for ($i = 0; $i < sizeof($request->file('feedback_pics')); $i++) {
                $message->attach($request->file('feedback_pics')[$i]->getRealPath(), [
                    'as' => $request->file('feedback_pics')[$i]->getClientOriginalName(),
                    'mime' => $request->file('feedback_pics')[$i]->getMimeType()
                ]);
            }
        });
        return response()->json([
            'email_sent' => true
        ]);
    }


    public function getStaticPage($pageSlug)
    {
        $page = Page::where('slug', $pageSlug)->select('title', 'content')->first();
        return response()->json([
            'page' => $page
        ]);
    }
}
