<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\Paginator;
use App\Category;

class ProductController extends Controller
{

    /**
     * Products
     *
     * Display products along with categories
     */
    public function index()
    {
        $category = Category::with(['product' => function ($q) {

        }])->orderBy('name', 'desc')->get();
        if (!empty($category)) {
            $category->toArray();
        }
        return response()->json([
            'Categories' => $category,
            'delivery_charges' => 10
        ], 200);
    }

}
