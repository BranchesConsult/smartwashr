<?php

/**
 * @resource Laundry
 *
 * Listing laundries
 */

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Laundry;
use App\User_Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class LaundryController extends Controller {

    /**
     * Get Near laundry
     *
     * Show nearby laundries
     */
    function getNearByLocation(\App\Http\Requests\NearLaundryRequest $request) {
        $data = $request->all();
        $result = \App\User::getByDistance($data['lat'], $data['lng'], 99999999999);
        return response()->json(['status' => true, "data" => $result], 200);
    }

    /**
     * Get all laundries
     * 
     * This metod will get all laundries... just for testing purpe
     */
    public function laundries() {
        $result = Laundry::all();
        return response()->json(['status' => true, "data" => $result], 200);
    }

}
