<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index() {
        $data['category'] = \App\Category::with('product')->orderBy('name', 'asc')->get()->toArray();
        if (\Session::get('language') == null) {
            \Session::put('language', 'en');
        }
        if (\Session::get('language') == 'ar') {
            app()->setLocale('ar');
        } else {
            app()->setLocale('en');
        }
        $data['code'] = country();
        $data['rate']=1;
        $data['product'] = \App\Product::orderBy('name', 'asc')->get()->toArray();
        //$data['menupages'] =
        return view('home', $data);
    }    
}
