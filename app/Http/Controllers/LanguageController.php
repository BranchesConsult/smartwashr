<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
class LanguageController extends Controller
{
    function __construct(Request $request){
        app()->setLocale($request->lang);
    }
    public function index() {
        
        echo trans('validation.accepted');
    }
    public function language(){
        if(\Session::get('language') == 'en')
            \Session::put('language', 'ar');
        else
            \Session::put('language', 'en');
        return back()->withInput();
    }
}
