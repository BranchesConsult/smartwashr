<?php

namespace App\Http\Controllers;

use App\User_Type;
use Mail;
use Illuminate\Http\Request;
use PDF; // at the top of the file


class OrderController extends Controller
{
    function order()
    {
        $id = \Auth::id();
        $order = \App\Orders::with('user')->where('user_id', $id)->orderBy('id', 'desc')->get();
        $userId = \Auth::id();
        $order = \App\Orders::with('user')->where('user_id', $userId)->get();
        if (!empty($order)) {
            $userId = \Auth::id();
            $userType = User_Type::where('user_id', $userId)->first();
            if ($userType['type_id'] == 2) {
                $order = \App\Orders::with('user')
                    ->where('driver_id', $userId)
                    ->orderBy('id', 'desc')
                    ->get();
            } else {
                $order = \App\Orders::with('user')
                    ->where('user_id', $userId)
                    ->orderBy('id', 'desc')
                    ->get();
            }
            if (!$order->isEmpty()) {
                $order = $order->toArray();
            } else {
                $order = [];
            }
            $data['order'] = $order;
            return view('orders.viewOrders', $data);
        }

    }

    function orderDetail($id)
    {
        $orderDetail = \App\Orders_detail::where('order_id', $id)->with('product')->
        orderBy('id', 'desc')->get()->toArray();
        $data['orderDetail'] = $orderDetail;
        //dd($orderDetail);
        //$data['title']='Order Detail';
        return view('orders.orderDetail', $data);
    }

    function sendReceipt()
    {
//        $data = array(
//            'name' => 'branchez',
//            'address' => 'loreum epsom',
//            'sender' => 'loreum epsom'
//        );
//
//        $html = \View::make('emails.receipt',$data);
//        $html = $html->render();
//        //die;
//        PDF::SetTitle('Invoice');
//        PDF::AddPage();
//        PDF::writeHTML($html, true, false, true, false, 0);
//        return PDF::Output('invoice-'.time().'.pdf', 'D');
        $data = array(
            'name' => 'branchez',
            'address' => 'loreum epsom',
            'sender' => 'loreum epsom'
        );
        $pdf = \PDF::loadView('emails.receipt', $data);
        Mail::send('emails.receipt', $data, function ($message) use ($pdf) {
            $message->to('mahnurz02@gmail.com')->subject('Invoice');
            $message->attachData($pdf->output(), 'receipt.pdf');
        });

    }

    public function sendingEmailToDriver(){

    }
}