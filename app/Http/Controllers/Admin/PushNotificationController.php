<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class PushNotificationController extends Controller
{
    public function sendToAll(Request $request)
    {
        $device_token = User::pluck('device_token')->toArray();
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('Smart Washr');
        $notificationBuilder
            //->setClickAction('OrderDetail')
            ->setTitle($request->get('notification_title'))
            ->setBody($request->get('notification_text'))
            ->setSound('default');
        $datatest = [];
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(["data" => $datatest]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $device_token;
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        return redirect()->back()->with(['status' => 'Message send to all connected devices']);
    }

    public function frmSendToAll()
    {
        $data['title'] = 'Push Notification';
        return view('admin.push_notification.form', $data);
    }
}
