<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Laundry;
use App\Product;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{

    function addProductForm()
    {
        $data['categories'] = Category::attr(['name' => 'categories[]', 'size' => 10, 'required' => true,
            'class' => 'form-control', 'multiple' => true])->selected(NULL);

        $data['laundry'] = Laundry::all()->toArray();
        $data['title'] = 'Add Product';
        return view('admin.product.addProduct', $data);
    }

    function addUpdateProduct($product_id = 0)
    {
        //dd(Input::all());
        $files = null;
        $rules = array(
            'name' => 'required',
            'name_ar' => 'required',
            'status' => 'required',
            'drycleaningprice' => 'required',
            'washingprice' => 'required',
            'pressprice' => 'required',
            'sw_drycleaningprice' => 'required',
            'sw_washingprice' => 'required',
            'sw_pressprice' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->to(route('addProduct'))
                ->withErrors($validator)->withInput();
        } else {
            $product = [
                'name' => Input::get('name'),
                'name_ar' => Input::get('name_ar'),
                'category' => Input::get('category'),
                'status' => Input::get('status'),
                'dryclean_price' => Input::get('drycleaningprice'),
                'washing_price' => Input::get('washingprice'),
                'press' => Input::get('pressprice'),
                'sw_dryclean_price' => Input::get('sw_drycleaningprice'),
                'sw_washing_price' => Input::get('sw_washingprice'),
                'sw_press' => Input::get('sw_pressprice'),
                'laundry_id' => Input::get('laundry_id')

            ];
//            if (Input::hasFile('pic')) {
//                $files = uploadInputs(Input::file('pic'), 'products');
            //           }
            if (empty(Input::get('pic'))) {
                $product['picture'] = 'http://smartwashr.com/img/smartwashr_logo.png';
            } else {
                $product['picture'] = Input::get('pic');
            }
            $product = Product::updateOrCreate(
                ['id' => $product_id], $product);
            $prod = Product::find($product['id']);
            $prod->category()->sync(Input::get('categories'));
            return redirect()->route('editProduct', ['product_id' => $product['id']]);
        }
    }

    function editProductForm($product_id)
    {
        try {
            $data['product'] = Product::where('id', $product_id)->with('category')->firstOrFail()->toArray();
            if (!empty($data['product']['category'])) {
                $data['product']['category'] = array_flatten(array_column($data['product']['category'], 'id'));
            }
            $data['categories'] = Category::attr(['name' => 'categories[]', 'size' => 10, 'required' => true, 'class' => 'form-control',
                'multiple' => true])->selected($data['product']['category']);
            $data['title'] = 'Edit Product';
            return view('admin.product.editProduct', $data);
        } catch (ModelNotFoundException $ex) {
            abort(404);
        }
    }

    function viewProducts()
    {
        $product = Product::with('category')
            ->orderBy('name', 'asc')
            ->paginate(100);
        if (!empty($product)) {
            $product->toArray();
        }
        //dd($product->toArray());
        $data['products'] = $product;
        $data['title'] = 'Products';
        return view('admin.product.viewProduct', $data);
    }

    function deleteProduct($product_id)
    {
        $product = Product::find($product_id);
        // deleteFile('uploads', $product['picture']);
        $product->delete();
        return back()->withInput();
    }

}
