<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
class PageController extends Controller
{
    function loadPages() {
      //  $c=Currency::convert('USD', 'TRY', 20);
      //  dd($c);
        $data['pages'] = Page::all()->toArray();
        $data['title'] = 'Pages';
        return view('admin.pages.pageList',$data);
    }
     
    function addPageForm() {
        $data['title'] = 'Add Page';
        return view('admin.pages.addPage',$data);
    }

    function addpage() {
        $rules = array(
            'title' => 'required',
            'content_ar' => 'required',
            'content' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('addPage')
                            ->withErrors($validator)->withInput();
        } else {
            $title = Input::get('title');
            $slug = slugify(Input::get('title'));
            $content = Input::get('content');
            $pages = new Page;
            $pages->title = $title;
            $pages->slug = $slug;
            $pages->content = $content;
            $pages->content_ar = Input::get('content_ar');
            $pages->save();
            return redirect()->route('editPage', ['id' => $pages->id]);

        }
    }

    function editPage($id) {
        //  $data = [];
        $data['pageid'] = Page::find($id)->toArray();
        $data['title'] = 'Edit Page';
        // echo '<pre>';print_r ($data);echo '</pre>';
        return view('admin.pages.editPage', $data);
    }

    function updateEditPage($id) {
       // dd(Input::get('content_ar'));
        $title = Input::get('title');
        $slug = slugify(Input::get('title'));
        $content = Input::get('content');
        $content_ar = Input::get('content_ar');
        $pages = new Page;
        $pages = Page::find($id);
        $pages->title = $title;
        $pages->slug = $slug;
        $pages->content = $content;
        $pages->content_ar = $content_ar;
        $pages->save();
        return redirect()->route('viewPages');
    }

    function deletePage($id) {
        $pages = Page::find($id);
        $pages->delete();
        return back()->withInput();
    }
}
