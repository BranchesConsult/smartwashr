<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use App\Http\Requests\CouponAddRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function index()
    {
        $data['title'] = 'Coupons';
        $data['coupons'] = Coupon::paginate(10);
        return view('admin.coupon.index', $data);
    }

    public function add()
    {
        $data['title'] = 'Add Coupon';

        return view('admin.coupon.add', $data);
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Coupon';
        $data['coupun'] = Coupon::find($id);
        return view('admin.coupon.edit', $data);
    }

    public function addPost(CouponAddRequest $request)
    {
        $coupon = new Coupon();
        $coupon->code = $request->get('code');
        $coupon->discount = $request->get('discount');
        $coupon->valid_from = $request->get('valid_from');
        $coupon->valid_to = $request->get('valid_to');
        $coupon->status = $request->get('status');
        $coupon->save();
        return redirect()->to(route('all_coupons'))->with(['status' => 'Coupon added successfully']);
    }

    public function postEdit(CouponAddRequest $request, $id)
    {
        $coupon = Coupon::find($request->id);
        $coupon->code = $request->get('code');
        $coupon->discount = $request->get('discount');
        $coupon->valid_from = $request->get('valid_from');
        $coupon->valid_to = $request->get('valid_to');
        $coupon->status = $request->get('status');
        $coupon->save();
        return redirect()->to(route('all_coupons'))->with(['status' => 'Coupon updated successfully']);
    }
}
