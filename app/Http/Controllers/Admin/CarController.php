<?php

namespace App\Http\Controllers\Admin;
use App\Cars;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarController extends Controller {

    function loadCars() {
        $car = Cars::paginate(10);
        if (!empty($car)) {
            $car->toArray();
        }
        
        $data['car'] = $car;
        $data['title'] = 'Cars';
        return view('admin.car.carList', $data);
    }
    function addCarForm() {
        $data['sw_user']= User::all()->toArray();
        $data['title'] = 'Add Car';
        
        return view('admin.car.addCar', $data);
    }

    function addUpdateLaundry($id = 0) {
        $files = null;
        $rules = array(
            'driver_id' => 'required',
            'pic' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator);
        } else {
            $car = [
                'driver_id' => Input::get('name'),
                'reg_book' => Input::get('pic')
            ];
            $car = Cars::updateOrCreate(
                            ['id' => $id], $car);
            return redirect()->route('viewCars');
        }
    }

}
