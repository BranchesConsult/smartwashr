<?php

namespace App\Http\Controllers\Admin;

use App\VotedAddress;
use \Mapper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VotedAddressController extends Controller
{
    public function index()
    {
        $data['title'] = 'Voted Area';
        $data['addresses'] = VotedAddress::with('user')->paginate(20);
        return view('admin.voted_area.map_view', $data);
    }
}
