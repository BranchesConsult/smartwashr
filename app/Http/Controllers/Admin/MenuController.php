<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class MenuController extends Controller {

    function loadMenus() {
        $data['menu'] = Menu::all()->toArray();
        $data['title'] = 'Menus';
        return view('admin.menu.menuList', $data);
    }

    function addMenuForm() {
        $data['page'] = Page::all()->toArray();
        $data['title'] = 'Add Menu';
        return view('admin.menu.addMenu', $data);
    }

    function addMenu() {
        $rules = array(
            'title' => 'required',
            'page_id' => 'required',
            'title_ar'=>'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('addMenu')
                            ->withErrors($validator)->withInput();
        } else {
            $page_id = Input::get('page_id');
            $title = Input::get('title');
            $active = (((Input::get('is_active'))) && !empty(Input::get('is_active'))) ? Input::get('is_active') : '0';
            //echo $active;die;
            $menu = new Menu;
            $menu->title = $title;
            $menu->title_ar = Input::get('title_ar');
            $menu->page_id = $page_id;
            $menu->is_active = $active;
            $menu->save();
            return redirect()->route('editMenu', ['id' => $menu->id]);
            // return Redirect::to('admin/menu/edit/' . $menu->id);
        }
    }

    function editMenu($id) {
        $data['menuid'] = Menu::find($id)->toArray();
        $data['page'] = Page::all()->toArray();
        $data['title'] = 'Edit Menu';
        // echo '<pre>';print_r ($data);echo '</pre>';
        return view('admin.menu.editMenu', $data);
    }

    function updateEditMenu($id) {
        $title = Input::get('title');
        $page_id = Input::get('page_id');
        $active = Input::get('is_active');
        $menu = new Menu;
        $menu = Menu::find($id);
        $menu->title = $title;
        $menu->page_id = $page_id;
        $menu->title_ar = Input::get('title_ar');
        $menu->is_active = $active;
        $menu->save();
        return redirect()->route('viewMenus');
    }

    function deleteMenu($id) {
        $menu = Menu::find($id);
        $menu->delete();
        return back()->withInput();
    }

//    function printMenu() {
//        $menu = Menu::with('page')->get()->toArray();
//        //echo '<pre>';print_r ($menu);echo '</pre>';
//        return view(layouts.header, $menu);
//    }
}
