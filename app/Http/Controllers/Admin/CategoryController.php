<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Category;

class CategoryController extends Controller {

    function addCategoryForm() {
        $data['categories'] = Category::attr(['name' => 'categories', 'class' => 'form-control'])
                ->selected(Null);
        $data['title'] = 'Add Category';
        return view('admin.category.addCategory', $data);
    }

    function addUpdateCategory($category_id = NULL) {
        $rules = array(
            'category' => 'required',
            'category_ar' => 'required',
            'status' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->to(route('addCategory'))
                            ->withErrors($validator)->withInput();
        } else {
            $category = [
                'name' => Input::get('category'),
                'name_ar' => Input::get('category_ar'),
                'slug' => slugify(Input::get('category')),
                'status' => Input::get('status'),
                'description' => Input::get('description'),
            ];
            if(empty(Input::get('pic'))){
                $category['picture']='http://smartwashr.com/img/smartwashr_logo.png';    
            }
            else{
                $category['picture'] = Input::get('pic');
            }
            if (Input::get('is_parent') == 0)
                $category['parent_id'] = Input::get('categories');
            else
                $category['parent_id'] = 0;
            $category = Category::updateOrCreate(
                            ['id' => $category_id], $category);
            return redirect()->route('editCategory', ['category_id' => $category['id']]);
        }
    }

    function editCategoryForm($category_id) {
        try {
            $category = Category::where('id', $category_id)->firstOrFail();
            $data['category'] = Category::all()->toArray();
            if (!empty($category)) {
                $category = $category->toArray();
            }
            $data['category'] = $category;
            $selected_id = $data['category']['parent_id'];
            $data['categories'] = Category::attr(['name' => 'categories', 'class' => 'form-control'])->selected($selected_id);
            $data['title'] = 'Edit Category';
            return view('admin.category.editCategory', $data);
        } catch (ModelNotFoundException $ex) {
            abort(404);
        }
    }

    function viewCategories() {
        $category = Category::paginate(10);
        if (!empty($category)) {
            $category->toArray();
        }
         //dd(count($category));
        $data['category'] = $category;
        $data['title'] = 'Categories';
        return view('admin.category.categoryList', $data);
    }

    function deleteCategory($category_id) {
        $category = Category::find($category_id);
        $category->delete();
        return back()->withInput();
    }

}
