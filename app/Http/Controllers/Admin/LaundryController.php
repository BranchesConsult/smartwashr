<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Laundry;
use App\User_Type;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class LaundryController extends Controller {
function viewLaundry() {
$laundry = Laundry::paginate(10);
if (!empty($laundry)) {
$laundry->toArray();
}

$data['laundry'] = $laundry;
// dd($laundry);
$data['title'] = 'Laundries';
return view('admin.laundry.laundryList', $data);
}


//function laundryNearBy($lat1=31.484085, $lon1=74.3943, $lat2=31.496749, $lon2=74.422823, $unit='K') {
//
//  //$theta = $lon1 - $lon2;
//  $dist =rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) 
//          * cos(deg2rad($lon1 - $lon2))))* 60 * 1.1515* 1.609344;
//  
//   $qry = "SELECT id ,(rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad('latitude')) +  cos(deg2rad($lat1))
//       * cos(deg2rad('latitude')) 
//          * cos(deg2rad($lon1 - 'longitude'))))* 60 * 1.1515* 1.60934) as 'distance' FROM "
//           . "laundry HAVING 'distance' <= 3";
//$result =DB::select(DB::raw($qry));     
//        dd($result);  
////$dist = rad2deg($dist);
//  //$dist = $dist * 60 * 1.1515;
//  $unit = strtoupper($unit);
//  if ($unit == "K") {
//    echo $dist ;
//  } else if ($unit == "N") {
//      echo ($dist * 0.8684);
//    } else {
//        echo $dist;
//      }
//}
    function addLaundryForm() {
        $data['laundry'] = User_Type::with('user')->where('type_id', '=', '1')->get()->toArray();
        $data['title'] = 'Add Laundry';
        $id= \Auth::id();
        $user=\App\User::find($id);
        if($user->is_admin==1)
        {  
            $data['is_admin']=true;
        }
        else{
            $data['is_admin']=false;
        }
        return view('admin.laundry.addLaundry', $data);
    }

    function addUpdateLaundry($laundry_id = 0) {
        $files = null;
        $rules = array(
            'name' => 'required',
            'country' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'status' => 'required',
            'st1' => 'required',
            'user_entered_address' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
           
            return Redirect::back()->withErrors($validator);
        } else {
            if (empty(Input::get('code')))
                $code = slugify(Input::get('name'));
            else
                $code = Input::get('code');
            
            $laundry = [
                'name' => Input::get('name'),
                'code' => $code,
                'owner_id' => Input::get('owner_id'),
                'st1' => Input::get('st1'),
                'st2' => Input::get('st2'),
                'phone' => Input::get('phone'),
                'email' => Input::get('email'),
                'user_entered_address' => Input::get('user_entered_address'),
                'state' => Input::get('state'),
                'status' => Input::get('status'),
                'city' => Input::get('city'),
                'country' => Input::get('country'),
                'longitude' => Input::get('latitude'),
                'latitude' => Input::get('longitude'),
                'zip' => Input::get('zip')
            ];
//            if (Input::hasFile('logo')) {
//                $files = uploadInputs(Input::file('logo'), 'logo');
                $laundry['logo'] = Input::get('logo');
            //}
            $laundry = Laundry::updateOrCreate(
                            ['id' => $laundry_id], $laundry);
            return redirect()->route('editLaundry', ['laundry_id' => $laundry['id']]);
        }
    }

    function editLaundryForm($laundry_id) {
        try {
            $laundry = Laundry::where('id', $laundry_id)->firstOrFail();
            $data['owner_id'] = User_Type::with('user')->where('type_id', '=', '1')->get()->toArray();
            if (!empty($laundry)) {
                $laundry = $laundry->toArray();
            }
            $id= \Auth::id();
        $user=\App\User::find($id);
        if($user->is_admin==1)
        {  
            $data['is_admin']=true;
        }
        else{
            $data['is_admin']=false;
        }
            $data['laundry'] = $laundry;
            $data['title'] = 'Edit Laundry';
            return view('admin.laundry.editLaundry', $data);
        } catch (ModelNotFoundException $ex) {
            abort(404);
        }
    }

    function deleteLaundry($laundry_id) {
        $laundry = Laundry::find($laundry_id);
        deleteFile('uploads', $laundry['logo']);
        //debugArr($laundry);die;
        $laundry->delete();
        return back()->withInput();
    }

}
