<?php

namespace App\Http\Controllers\Admin;

use App\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Mail;
//FCM
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class OrderController extends Controller
{

    function viewOrders()
    {
        $order = \App\Orders::with('laundry', 'user', 'driver')->orderBy('id', 'desc')->paginate(10);
        if (!empty($order)) {
            $order->toArray();
        }
        // dd($order->toArray());
        $data['order'] = $order;
        $data['title'] = 'Orders';
        return view('admin.order.viewOrders', $data);
    }

    function orderDetail($id)
    {
        $orderDetail = \App\Orders_detail::where('order_id', $id)->with('product')->
        orderBy('id', 'desc')->paginate(10);
        $data['orderDetail'] = $orderDetail;
        $data['title'] = 'Order Detail';
        return view('admin.order.viewOrderDetail', $data);
    }

    function editOrderForm($id)
    {
        try {
            $order = \App\Orders::where('id', $id)->firstOrFail();
            if (!empty($order)) {
                $order = $order->toArray();
            }
            $data['order'] = $order;
            //dd($data['owner_id']);
            $data['title'] = 'Edit Order Status';
            return view('admin.order.editOrder', $data);
        } catch (ModelNotFoundException $ex) {
            abort(404);
        }
    }

    function editOrder($id)
    {
        $status = Input::get('status');
        $order = \App\Orders::find($id);
        $order->status = $status;
        $order->save();
        $user_id = $order->user_id;
        $data['email'] = $order->user_email;
        $user = \App\User::find($user_id);
        $data['name'] = $user->name;
        $data['status'] = $order->status;
        $data['order_id'] = $id;
        $this->sendPushMessage($order->id);
        $this->orderUpdatePushMesage($id);
        \Mail::send('emails.statusUpdate', $data, function ($message) use ($data) {
            // $message->from('mahnurz02@gmail.com', 'Smart Washr');
            $message->subject('Order Status Updated');
            //$message->to($data['email'])
            $message->to('suhail@smartwashr.com')
                ->cc(['umair_hamid100@yahoo.com']);
        });

        return redirect()->route('viewOrders')->with('message', 'Order Status has been updated successfully!');
    }

    public function sendPushMessage($order_id)
    {
        $order = \App\Orders::find($order_id);
        $driver_id = $order->driver_id;
        $order = \App\Orders::where('driver_id', $driver_id)->where('status', '3')->get()->count();
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder('Smart Washr');
        $notificationBuilder->setBody('Order Notification')->setSound('default');
        $datatest = ["title" => "Completed Orders", "message" => "Laundry orders completed for delivery", "count" => $order];
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(["data" => $datatest]);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $driver = \App\User::find($driver_id);
        if (empty($driver->device_token)) {
            $driver->device_token = '3243242432432342';
        }
        $token = $driver->device_token;
        return $downstreamResponse = response()->json(['notification' => FCM::sendTo($token, $option, $notification, $data)]);
    }

    function notificationSeen()
    {
        $id = \Auth::id();
        if (\Auth::user()->is_admin == 1) {
            $order = \App\Orders::where('id', '>', '0')->update([
                'is_read_admin' => 1
            ]);
        } else {
            // $laundry = \App\Laundry::where('owner_id', $id)->get(['id'])->toArray();
            // $laundries = array_flatten($laundry);
            $order = \App\Orders::where('id', '>', '0')->update(['is_read_laundry' => 1]);
        }
//        return response()->json([
//                    'success' => true
//        ]);
    }

    public function orderUpdatePushMesage($orderId)
    {
        $orderStatuses = [
            0 => 'You order is now Pending',
            1 => 'You order has been Received',
            2 => 'You order is In-Progress',
            3 => 'You order has been Completed and will be delivered soon',
            4 => 'You order has been Delivered, Thank you for your loyalty',
            5 => 'You order has been Cancelled',
            6 => 'You order is Ready For Delivery',
        ];
        $orderDetail = Orders::where('id', $orderId)->with('user', 'order_detail')
            ->first()
            ->toArray();
        $device_token = $orderDetail['user']['device_token'];
        if (!empty($device_token)) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('Smart Washr');
            $notificationBuilder
                ->setClickAction('OrderDetail')
                ->setBody($orderStatuses[$orderDetail['status']])
                ->setSound('default');
            $datatest = ["order_id" => $orderId];
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(["data" => $datatest]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            $token = $device_token;
            return $downstreamResponse = response()->json(['notification' => FCM::sendTo($token, $option, $notification, $data)]);
        } else {
            /**
             * \Mail::send('emails.statusUpdate', $data, function ($message) use ($data) {
             * // $message->from('info@smartwashr.com', 'Smart Washr');
             * $message->subject('Order Status Updated');
             * $message->to($data['email'])
             * ->cc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']);
             * });
             */
        }
    }
}
