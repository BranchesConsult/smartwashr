<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Laundry;
use App\User_Type;
use \Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {

    function viewUsers() {
        $id = \Auth::id();
        $email=Input::get('email');
        $user = User::find($id);
        $laundry = \App\Laundry::where('owner_id', $id)->get(['id'])->toArray();
        $res = array_flatten($laundry);
        $orders = \App\Orders::whereIn('laundry_id', $res)->get(['user_id'])->toArray();
        $res = array_flatten($orders);
        $userType = Input::get('user_type');
        $query = User::with('user_type')
                ->whereHas(
                'user_type', function($query) use ($userType) {
            if (strlen($userType) > 0) {
                $query->where('type_id', $userType);
            }
        }
        );
        if (($user->is_admin == 1)&&($email==null)) {
        $users = $query->paginate(10);
        }
            else if(($user->is_admin == 1)&&($email!=null)){
        $users = $query->where('email',$email)->get();      
        } 
        else if(($user->is_admin == 0)&&($email!=null)){
            $users = $query->whereIn('id', $res)->paginate(10);
        }
        else{
           $users = $query->whereIn('id', $res)->where('email',$email)->get();
        }
        if (!empty($users)) {
            $users->toArray();
        }
        $data['users']=$users;
        $data['selected'] = $userType;
        $data['title'] = 'Users';
        return view('admin.users.usersList', $data);
    }
//    function searchUser() {
//        $id = \Auth::id();
//        $email=Input::get('email');
//        $user = User::find($id);
//        $laundry = \App\Laundry::where('owner_id', $id)->get(['id'])->toArray();
//        $res = array_flatten($laundry);
//        $orders = \App\Orders::whereIn('laundry_id', $res)->get(['user_id'])->toArray();
//        $res = array_flatten($orders);
//        $userType = Input::get('user_type');
//        $query = User::with('user_type')
//                ->whereHas(
//                'user_type', function($query) use ($userType) {
//            if (strlen($userType) > 0) {
//                $query->where('type_id', $userType);
//            }
//        }
//        );
//        if ($user->is_admin == 1) {
//            
//            $users = $query->where('email',$email)->get()->toArray();
//            dd($users);
//        } else {
//            $users = $query->whereIn('id', $res)->paginate(10);
//        }
//        if (!empty($users)) {
//            $users->toArray();
//        }
//        dd($users->toArray());
//        $data['users']=$users;
//        $data['selected'] = $userType;
//        $data['title'] = 'Users';
//        return view('admin.users.usersList', $data);
//    }
    function addUserForm() {
        $data['title'] = 'Add User';
        return view('admin.users.addUsers', $data);
    }

    function addUser() {
       // dd(Input::all());
        $files = null;
        $user_type = Input::get('user_type');
        $pass1 = Input::get('password');
        $pass2 = Input::get('password2');
        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'password2' => 'required',
            'email'  => 'unique:users,email',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('addUser')
                            ->withErrors($validator)->withInput();
        } else if ($pass1 != $pass2) {
            return redirect()->to(route('addUser'))->with('message', 'Password does not match');
        } else {
            $user = new User;
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = bcrypt(Input::get('password'));
            $user->active = Input::get('status');
            $user->phone_number = Input::get('phone');
            $user->profile_pic = Input::get('pic');
            $user->sw_latitude = Input::get('sw_lat');
            $user->sw_longitude = Input::get('sw_long');
            $user->ne_latitude = Input::get('ne_lat');
            $user->ne_longitude = Input::get('ne_long');
            $user->save();
            for ($i = 0; $i < count($user_type); $i++) {
                $usertype = new User_Type;
                $usertype->user_id = $user['id'];
                $usertype->type_id = $user_type[$i];
                $usertype->save();
            }
            return redirect()->route('editUserForm', ['id' => $user['id']]);
        }
    }

    function editUserForm($user_id) {
        try {
            $user = User::where('id', $user_id)->with([
                        'user_type' => function($query) {
                            $query->select('type_id', 'user_id');
                        }
                    ])->firstOrFail();
            if (!empty($user)) {
                $user = $user->toArray();
                $user['user_type'] = array_flatten(array_column($user['user_type'], 'type_id'));
            }
            //Convert user_type to single array and getting on type_id
            $data['swUser'] = $user;
            $data['title'] = 'Edit User';
            //dd($data['swUser']);
            return view('admin.users.editUser', $data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
            abort(404);
        }
    }

    function editUser($user_id) {
        //dd(Input::all());
        $file = null;
        $data['password'] = Input::get('password');
        $data['password_confirmation'] = Input::get('password_confirmation');
        $rules = array(
            'password' => 'confirmed',
            'password_confirmation' => '',
            'email'  => 'unique:users,email',
        );
        // Create a new validator instance.
        $validator = Validator::make($data, $rules);
        //dd(Input::get('user_type'));
        if ($validator->passes()) {
            $user_type = Input::get('user_type');
            $user = User::find($user_id);
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            if (!empty(Input::get('password'))) {
                $user->password = bcrypt(Input::get('password'));
            }
            $user->active = Input::get('active');
            $user->phone_number = Input::get('phone');
            $user->profile_pic = Input::get('pic');
            $user->sw_latitude = Input::get('sw_lat');
            $user->sw_longitude = Input::get('sw_long');
            $user->ne_latitude = Input::get('ne_lat');
            $user->ne_longitude = Input::get('ne_long');
            $user->save();
            $usertype = User_Type::where('user_id', $user_id);
            $usertype->delete();
            for ($i = 0; $i < count($user_type); $i++) {
                //echo($user_type[$i]);
                $usertype = new User_Type;
                $usertype->user_id = $user['id'];
                $usertype->type_id = $user_type[$i];
                $usertype->save();
            }
        }
        return Redirect()->back()->withErrors($validator);
    }

    function deleteUser($user_id) {
        $user_id = (int) $user_id;
        $user = User::find($user_id);
        $user->delete();
        return back();
    }

    function attachLaundryForm($id) {
        $laundry = Laundry::all();
        $attachlaundry = User::where('id', $id)->with(['user_laundry' => function($query) {
                        $query->select('laundry_id');
                    }
                ])->firstOrFail();
        if (!empty($attachlaundry)) {
            $attachlaundry = $attachlaundry->toArray();
            $data['attachlaundry'] = array_flatten(array_column($attachlaundry['user_laundry'], 'laundry_id'));
        }
        if (!empty($laundry)) {
            $data['laundry'] = $laundry->toArray();
        }
        $data['sw_user'] = User::find($id);
        $data['sw_user_id'] = $id;
        $data['title'] = 'Attach Laundry';
        return view('admin.users.attachLaundry', $data);
    }

    function attachLaundry($id) {
        $rules = array(
            'laundry' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->back()
                            ->withErrors($validator)->withInput();
        }
        $user = new User;
        $user->user_laundry()->detach($id);
        $user = User::find($id);
        $user->user_laundry()->sync(Input::get('laundry'));
        return redirect()->back()->with('message', 'Laundry added');
    }

}
