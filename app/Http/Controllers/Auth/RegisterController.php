<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\User_Type;
use App\Laundry;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use Mail;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        if (\Session::get('language') == 'ar') {
            app()->setLocale('ar');
        } else
            app()->setLocale('en');
        $data['menupages'] = \App\Menu::with('page')->where('is_active','1')->get()->toArray();
        $user_type= \App\User_Type::where('user_id',\Auth::id())->where('type_id','1')->get()->toArray();
        count($user_type)?$data['is_laundry']=true:$data['is_laundry']=false;
        return view('adminlte::auth.register', $data);
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/registration_request';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'username' => 'sometimes|required|max:255|unique:users',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
                     // 'terms'    => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        $fields = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activation_code' => str_random(60) . $data['email'],
            'active' => 0
        ];
        if (config('auth.providers.users.field', 'email') === 'username' && isset($data['username'])) {
            $fields['username'] = $data['username'];
        }
//dd($data['user_type']);
        $id = User::create($fields)->id;
        $user_type = new User_Type;
        $user_type->type_id = '3';
        $user_type->user_id = $id;
        $user_type->save();
        $data = [
            'name' => $fields['name'],
            'code' => $fields['activation_code'],
            'password' => Input::get('password'),
            'email' => Input::get('email')
        ];
        $this->sendEmail($data);
        return User::updateOrCreate($fields);
    }

    private function sendEmail($data) {
        Mail::send('emails.register', $data, function($message) use ($data) {
             $message->from('no-reply@smartwashr.com', 'Smart Washr');
            $message->to($data['email'], $data['name'])->subject('SmartWashr Registration!');
        });
    }

}
