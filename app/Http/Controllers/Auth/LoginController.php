<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers {
        attemptLogin as attemptLoginAtAuthenticatesUsers;
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
         if (\Session::get('language') == 'ar') {
        app()->setLocale('ar');
        }
        else{
        app()->setLocale('en');
        
        }
        $user_type= \App\User_Type::where('user_id',\Auth::id())->where('type_id','1')->get()->toArray();
        count($user_type)?$data['is_laundry']=true:$data['is_laundry']=false;
        $data['menupages'] = \App\Menu::with('page')->where('is_active','1')->get()->toArray();
        return view('adminlte::auth.login',$data);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    protected function redirectTo() {
        return (\Auth::user()->is_admin == 1) ? '/admin/users' : '/';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest', ['except' => 'logout']);
    }
/**
     * Show admin login form
     */
    public function showAdminLoginForm() {

        return view('adminlte::auth.login');
    }
    /**
     * Returns field name to use at login.
     *
     * @return string
     */
    public function username() {
        return config('auth.providers.users.field', 'email');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request) {
        $user=$request->toArray();
        $user_active=\App\User::where('email',$user['email'])->first();
       
        if ($this->username() === 'email')
            return $this->attemptLoginAtAuthenticatesUsers($request);
        if (!$this->attemptLoginAtAuthenticatesUsers($request)) {
            return $this->attempLoginUsingUsernameAsAnEmail($request);
        }
        return false;
    }

    /**
     * Attempt to log the user into application using username as an email.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attempLoginUsingUsernameAsAnEmail(Request $request) {
        return $this->guard()->attempt(
                        ['email' => $request->input('username'), 'password' => $request->input('password')], $request->has('remember'));
    }

    /**
     * Login user from front end using ajax
     * @param Request $request
     * @return type
     */
    public function ajax_login(Request $request) {
        if ($this->attemptLogin($request)) {
            return response()->json([
                        'success' => true
                            ], 200);
        }
        return response()->json([
                    'success' => false,'message' => 'Invalid Password or Email'
                        ], 401);
    }

}
