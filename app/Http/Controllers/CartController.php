<?php

namespace App\Http\Controllers;

use App\Order;
use App\Orders;
use App\Orders_detail;
use App\User;
use Illuminate\Http\Request;
use Darryldecode\Cart\Cart;
use \Auth;
use PDF;
use Mail;
use Illuminate\Support\Facades\Input;

//use Symfony\Component\Validator\Constraints\Null;

class CartController extends Controller
{

    protected $cart;

    public function __construct()
    {
        // $this->cart = new Cart();
    }

    public function cartDetail()
    {
        $data = [];
        return view('cart.cart_detail', $data);
    }

    public function userInfo(Request $request)
    {
        $id = Auth::id();
        $data['user'] = \App\User::find($id);
        $data['userLat'] = $request->get('user-lat');
        $data['userLng'] = $request->get('user-lng');
        $data['driverId'] = $request->get('driver');
        $data['laundryId'] = \DB::table('laundry_driver')
            ->where('driver_id', $data['driverId'])
            ->first()->laundry_id;
        if (\Session::get('language') == 'ar') {
            app()->setLocale('ar');
        } else {
            app()->setLocale('en');
        }
        $data['product'] = \App\Product::all()->toArray();
        $data['menupages'] = \App\Menu::with('page')->where('is_active', '1')->get()->toArray();
        $data['cartProducts'] = \Cart::getContent()->toArray();
        $data['cartSubtotal'] = \Cart::getSubTotal();
        $data['cartTotal'] = \Cart::getTotal() + config('const.delivery_charges');
        $data['currency'] = 'USD';
        $data['location'] = $request->get('address_location');
        return view('cart.user_info', $data);
    }

    public function addItem(Request $request)
    {
        $productId = $request->get('item_id');
        $price = \App\Product::where('id', $productId)->first();
        $price = $price->toArray();
        if ($request->get('service_selected') == 'press') {
            $price = $price['press'];
        } else if ($request->get('service_selected') == 'washing') {
            $price = $price['washing_price'];
        } else {
            $price = $price['dryclean_price'];
        }
        $productPrice = $request->get('item_price');
        $productName = $request->get('item_name');
        $serviceSelected = $request->get('service_selected');
        \Cart::add(array(
            'id' => $productId,
            'name' => $productName,
            'price' => $productPrice,
            'quantity' => ($request->get('qty')) ? $request->get('qty') : 1,
            'attributes' => [
                'service_selected' => $serviceSelected,
                'original_price' => $price
            ]
        ));
        $itemsInCart = 0;
        foreach (\Cart::getContent()->toArray() as $row) {
            $itemsInCart++;
        }
        return response()->json(['total_items' => $itemsInCart]);
    }

    public function countCartContents()
    {
        $itemsInCart = 0;
        foreach (\Cart::getContent()->toArray() as $row) {
            $itemsInCart++;
        }
        return response()->json(['total_items' => $itemsInCart]);
    }

    function updateQty(Request $request)
    {
        $productId = $request->get('item_id');
        $qty = $request->get('quantity');
        \Cart::update($productId, array(
            'quantity' => [
                'relative' => false,
                'value' => $qty
            ],
        ));
        return response()->json([
            'quantity' => $qty,
            'subTotal' => \Cart::getSubTotal(),
            'total' => \Cart::getTotal()
        ]);
    }

    function deleteCartProduct(Request $request)
    {
        $productId = $request->get('item_id');
        \Cart::remove($productId);
        return response()->json(['removed_product' => $productId]);
    }

    function orderComplete(Request $request)
    {
        $laundry_price = 0;
        $cartItems = \Cart::getContent()->toArray();
        foreach ($cartItems as $row) {
            $laundry_price += ($row['attributes']['original_price'] * $row['quantity']);
        }
        $commission = \Cart::getTotal() - $laundry_price;
        if ($commission < 0) {
            $commission = abs($commission);
        }
        $commission = $commission + config('const.delivery_charges_commission');
        session(['cart_user' => $request->all()]);
        //Saving order
        $orderMdl = new Order;
        $orderMdl->laundry_id = session('cart_user.laundryId');
        $orderMdl->user_id = \Auth::user()->id; //register new user and save his email here.
        $orderMdl->user_phone = session('cart_user.user_phone');
        $orderMdl->driver_id = session('cart_user.driverId');
        $orderMdl->total_price = $laundry_price + config('const.delivery_charges');
        $orderMdl->user_email = Input::get('user_email');
        $orderMdl->sw_total_price = \Cart::getTotal() + config('const.delivery_charges');
        $orderMdl->delivery_charges = config('const.delivery_charges');
        $orderMdl->commission = $commission; //Calculate proper commision using cart
        $orderMdl->discount = 0;
        $orderMdl->discount_type = 0;
        $orderMdl->post_code = session('cart_user.post_code');
        $orderMdl->address_one = session('cart_user.user-address');
        $orderMdl->address_two = Input::get('user-address');
        $orderMdl->latitude = session('cart_user.user-lng');
        $orderMdl->longitude = session('cart_user.user-lat');
        $orderMdl->delivery_instruction = session('cart_user.laundry_instructions');
        $orderMdl->status = 0;
        $orderMdl->delivery_charges_commission = config('const.delivery_charges_commission');
        $orderMdl->collection_date_time = date('Y-m-d h:i:s', strtotime(session('cart_user.collection_date_time')));
        $orderMdl->delivery_date_time = date('Y-m-d h:i:s', strtotime(session('cart_user.delivery_date_time')));
        $orderMdl->payment_method = $request->get('payment-method');
        $orderMdl->save();
        //Saving order detail
        $orderId = $orderMdl->id;
        $orderDetail = new Orders_detail();
        $orderDetailArr = [];
        foreach ($cartItems as $key => $val) {
            $orderDetailArr[$key]['order_id'] = $orderId;
            $orderDetailArr[$key]['product_id'] = $val['id'];
            $orderDetailArr[$key]['quantity'] = $val['quantity'];
            $orderDetailArr[$key]['product_service'] = $val['attributes']['service_selected'];
            $orderDetailArr[$key]['price'] = $val['attributes']['original_price'] * $val['quantity']; //Getting it from product table when added to cart
            $orderDetailArr[$key]['sw_price'] = $val['price'] * $val['quantity'];
            $commission = ($val['price'] * $val['quantity']) - ($val['attributes']['original_price'] * $val['quantity']);
            if ($commission < 0) {
                $commission = abs($commission);
            }

            $orderDetailArr[$key]['commission'] = $commission; //Calculate per product commotion
            $orderDetailArr[$key]['created_at'] = date('Y-m-d h:i:s');
            $orderDetailArr[$key]['updated_at'] = date('Y-m-d h:i:s');
        }
        $orderDetail->insert($orderDetailArr);
        $pdf = \App\Orders_detail::where('order_id', $orderId)->with('product')->get()->toArray();
        $this->sendReceipt($pdf, $orderId);
        //Getting driver email
        $driver = User::where('id', session('cart_user.driverId'))->first()->toArray();
        $driver['client_phone'] = session('cart_user.user_phone');
        $driver['client_email'] = session('cart_user.user_email');
        $driver['client_address'] = session('cart_user.user-address');
        $driver['collection_date_time'] = session('cart_user.collection_date_time');
        $driver['delivery_date_time'] = session('cart_user.delivery_date_time');
        $driver['laundry_instructions'] = session('cart_user.laundry_instructions');
        $driver['user_glocation'] = "http://www.google.com/maps/place/" . session('cart_user.user-lat') . " ," . session('cart_user.user-lng');
        $driver['order_id'] = str_pad($orderId, 6, "0", STR_PAD_LEFT);
        \Mail::send('emails.notify_driver_order', $driver, function ($message) use ($driver) {
            $message->from('Info@smartwashr.com', 'Smart Washr');
            $message->subject('New order placed');
            $message->to($driver['email'])
                ->cc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']); //$driver['email']
        });
        //Getting laundry email
        $laundry = \App\Laundry::where('id', session('cart_user.laundryId'))->first()->toArray();
        $driver['laundry_email'] = $laundry['email'];
        \Mail::send('emails.notify_laundry', $driver, function ($message) use ($driver) {
            $message->from('Info@smartwashr.com', 'Smart Washr');
            $message->subject('New order placed');
            $message->to($driver['laundry_email'])
                ->cc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']); //$laundry['email']
        });
        \Cart::clear();
        return view('cart.complete-order');
    }

    function getNearByLaundry(Request $request)
    {
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $result = \App\User::getByDistance($lng, $lat, 1000);
        $laundriesLocation = [];
        foreach ($result as $key => $val) {
            $laundriesLocation[] = [
                $val->user_entered_address,
                $val->longitude,
                $val->latitude,
            ];
        }
        return response()->json($laundriesLocation, 200);
    }

    function getNearByDriver(Request $request)
    {
        // dd(Input::get('location'));

        $drivers = [];
        $latitude = $request->get('lat'); //21.286875556934202;
        $longitude = $request->get('lng'); //39.33605316406249;
        $user = \App\User::where('ne_latitude', '>=', $latitude)
            //->has('user_laundry')
            ->where('sw_latitude', '<=', $latitude)
            ->where('ne_longitude', '>=', $longitude)
            ->where('sw_longitude', '<=', $longitude)
            ->with(['user_type' => function ($q) {
                return $q->where('type_id', '2');
            }, 'user_laundry'])
            ->get();

        if (($user->isEmpty())) {
            return response()->json([]);
        }
        $hasUserLaundry = \DB::table('laundry_driver')->where('driver_id', $user[0]['id'])->get();
        if ($hasUserLaundry->isEmpty()) {
            return response()->json([]);
        }
        return response()->json($user[0]);
        if (!empty($user)) {
            $user = $user->toArray();
        }
        foreach ($user as $item) {
            if (($item['user']['sw_latitude'] != null) && ($item['user']['sw_longitude'] != null) &&
                ($item['user']['ne_latitude'] != null) && ($item['user']['ne_longitude'] != null)
            ) {
                if (($latitude <= $item['user']['ne_latitude']) && ($latitude >= $item['user']['sw_latitude'])) {
                    if (($item['user']['ne_longitude'] >= $item['user']['sw_longitude']) && ($longitude <= $item['user']['ne_longitude']) &&
                        ($longitude >= $item['user']['sw_longitude'])
                    ) {
                        array_push($drivers, $item['user']['id']);
                    } else if ($longitude >= $item['user']['sw_longitude']) {
                        array_push($drivers, $item['user']['id']);
                    }
                }
            }
        }
        dd($drivers);
    }

    function getUserLocation()
    {
//        $query = User::with('user_type')
//                ->whereHas(
//                'user_type', function($query) {
//                $query->where('type_id', '2');
//        }
//        )->get(['ne_latitude','ne_longitude','sw_latitude','sw_longitude']);
//        $data['service']=$query;
        return view('cart.user_location');
    }

    function sendReceipt($pdf, $id)
    {
        $email = \Auth::user()->email;
        $data['email'] = $email;
        $data['pdf'] = $pdf;
        $data['id'] = $id;
        $pdf = \PDF::loadView('emails.receipt', $data);
        Mail::send('emails.orderPlaced', $data, function ($message) use ($pdf, $data) {
            $message->from('Info@smartwashr.com', 'Smart Washr');
            $message->to($data['email'])->subject('Invoice')
                ->cc(['umair_hamid100@yahoo.com', 'suhail@smartwashr.com']);
            $message->attachData($pdf->output(), 'receipt.pdf');
        });
    }
}
