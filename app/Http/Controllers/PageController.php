<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller {

    public static function is_laundry(){
        $user_type= \App\User_Type::where('user_id',\Auth::id())->where('type_id','1')->get()->toArray();
        $is_laundry=count($user_type)?true:false;
        return $is_laundry;
    }
    public static function notifications(){
        if(\Auth::user()->is_admin==1){
            $orders= \App\Orders::where('is_read_admin','0')->with('laundry')->get();
        }
        else{
            $orders= \App\Orders::where('is_read_laundry','0')->with('laundry')->get();   
        }
        if(!empty($orders)){
            $orders=$orders->toArray();
        }
        $data['orders']=$orders;
        $data['count']=count($orders);
        return $data;
        //return $is_laundry;
    }
    public function termsAndCondition() {
        return view('pages.terms');
    }
    public function privacyPolicy() {
        return view('pages.privacyPolicy');
    }
    public function page($slug){
        //dd($slug);
        $data['page'] = \App\Page::where('slug',$slug)->with('menu')->first();
        //dd($data['page']->toArray());
        $data['menupages'] = \App\Menu::with('page')->where('is_active','1')->get()->toArray();
        return view('pages.content',$data);
    }
    
    public function home() {
        $data['category'] = \App\Category::with('product')->orderBy('name', 'asc')->get()->toArray();
        //dd($data['category']);
        if (\Session::get('language') == null) {
            \Session::put('language', 'en');
        }
        if (\Session::get('language') == 'ar') {
            app()->setLocale('ar');
        } else {
            app()->setLocale('en');
        }
        $data['code'] = country();
        //$data['currencyCode']=currencyCode();
        //$data['rate'] = currencyConverter('USD', currencyCode(), 1);
        //dd($data['rate']);
        $data['rate']=1;
        $data['product'] = \App\Product::orderBy('name', 'asc')->get()->toArray();
        $data['menupages'] = \App\Menu::with('page')->where('is_active','1')->get()->toArray();
        return view('home', $data);
    }

    public function registrationRequest() {
        return view('pages.registrationRequest');
    }

    public function verifyUser($email) {

        $user = \App\User::where('email', $email)->first();
        $user->active = 1;

        $user->save();
        return redirect('/login');
    }

    public function verifyEmail($email) {
        $user = \App\User::where('email', $email)->first();
        $user->active = 1;
        $user->save();
        return view('pages.userVerified');
    }

    public function editProfileForm() {
        $user_id = Auth::id();
        try {
            $user = \App\User::where('id', $user_id)->firstOrFail();
            if (!empty($user)) {
                $user = $user->toArray();
            }
            $data['swUser'] = $user;
            return view('editProfile', $data);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
            abort(404);
        }
    }

    public function editProfile(Request $request) {
        Validator::make($request->all(), [
            'first_name' => 'required'
        ])->validate();
        $file = null;
        $user_id = Auth::id();
        $data['password'] = Input::get('password');
        $data['password_confirmation'] = Input::get('password_confirmation');
        $user = \App\User::find($user_id);
        $user->name = Input::get('first_name');
        if (!empty(Input::get('password'))) {
            $user->password = bcrypt(Input::get('password'));
        }
        if (Input::hasFile('pic')) {
            $files = uploadInputs(Input::file('pic'), 'pics');
            $user->profile_pic = $files;
        }
        $user->phone_number = Input::get('phone');
        $user->save();
        return Redirect()->back()->with('status', 'Profile changed successfuly.');
    }
    public function resendEmail(){
        $id=\Auth::id();
        
        $user=\App\User::find($id);
        $data=[
            'name'=>$user->name,
            'password'=>$user->password,
            'email'=>$user->email
        ];
        //dd($user->toArray());
        Mail::send('emails.verificationEmail', $data, function($message) use ($data) {
            // $message->from('noukripk2017@gmail.com', 'Smart Washr');
            $message->to($data['email'], $data['name'])->subject('SmartWashr Registration!');
        });
        return Redirect()->back()->with('message', 'An Email has been sent on your account');
    }
}
