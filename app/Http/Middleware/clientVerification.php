<?php

namespace App\Http\Middleware;

use Illuminate\Http\Response;
use Closure;

class clientVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::Check() && \Auth::user()->active == 1) {
            return $next($request);
        } else {
            return new Response(view('pages.verifyEmail'));
        }
    }
}
