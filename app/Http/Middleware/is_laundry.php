<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class is_laundry {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $id = \Auth::id();
        $user_type = \App\User_Type::where('user_id',$id)->where('type_id','1')->first();
        if ((Auth::check() && \App\User::isAdmin()) || (Auth::check() && $user_type != null)) {
            return $next($request);
        } else {
            return abort(401, 'Unauthorized action.');
        }
    }

}
