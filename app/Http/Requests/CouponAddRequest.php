<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $code = request('code');
        return [
            //'code' => 'required|unique:coupons,code,' . $code,
            'code' => 'required',
            'discount' => 'required|numeric',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'status' => 'required',
        ];
    }

    /**
     * Get the failed validation response for the request.
     *
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $transformed = [];

        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }

        return response()->json([
            'errors' => $transformed
        ], \Illuminate\Http\JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
