<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_detail extends Model {
    protected $table = 'orders_detail';
    //
    protected $guard = [];

    public function orders() {
        return $this->belongsTo("App\Orders", 'order_id');
    }

    public function product() {
        return $this->hasMany("App\Product",'id','product_id');
    }

}
