<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotedAddress extends Model
{
    protected $table = 'voted_address';
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
