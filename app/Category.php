<?php

namespace App;

use Nestable\NestableTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends \Eloquent {

    use NestableTrait;

    protected $parent = 'parent_id';
    protected $dates = ['deleted_at'];
    protected $table = 'categories';
    protected $fillable = [
        'status', 'name','name_ar','description', 'parent_id', 'slug','picture'
    ];
public function product() {
        return $this->belongsToMany("App\Product");
    }
}
