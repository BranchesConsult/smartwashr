<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderComplainImage extends Model
{
    protected $table = 'order_complain_images';
    public $timestamps = false;

    public function complain()
    {
        return $this->belongsTo('App\OrderComplain', 'complain_id');
    }
}
