<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {

    protected $table = 'product';
    protected $fillable = [
        'status', 'dryclean_price', 'name','name_ar','press', 'picture', 'washing_price','sw_dryclean_price',
        'sw_washing_price','sw_press'
    ];

    public function laundry() {
        return $this->belongsTo("App\Laundry", 'laundry_id');
    }
    
     public function orders_detail() {
        return $this->belongsTo("App\Orders_detail",'product_id');
    }

    public function category() {
        return $this->belongsToMany("App\Category");
    }

// this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();
        static::deleting(function($product) { // before delete() method call this
            //$product->category()->delete();
            // do the rest of the cleanup...
        });
    }

    public function getProductOrignalPrice($productId, $service){

    }
}
