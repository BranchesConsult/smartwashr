<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{

    use Notifiable;

    static public $userType = array("Laundry" => 2, "Driver" => 3, "Client" => 4);

    /**
     * Type casting of db columns
     * @var type
     */
    protected $casts = [
        'is_admin' => 'int',
    ];
    protected $fillable = [
        'name', 'phone_number',
        'profile_pic',
        'email',
        'password',
        'active',
        'activation_code',
        'st1',
        'st2', 'city', 'state/province', 'zip', 'country', 'sw_latitude', 'sw_longitude',
        'ne_latitude', 'ne_longitude', 'status', 'last_login', 'device_token'
    ];
//        public function newQuery($excludeDeleted = true) {
//        $id= \Auth::id();
//        $user=\App\User::find($id);
//        if($user->is_admin==1)
//        {  
//            return parent::newQuery($excludeDeleted);
//        }
//        else {
//            $laundry=\App\Laundry::where('owner_id',$id)->get(['id'])->toArray();
//                        $res = array_flatten($laundry);
//                        $orders=\App\Orders::whereIn('laundry_id', $res)->get(['user_id'])->toArray();
//                        $res = array_flatten($orders);
//            return parent::with('user')->newQuery($excludeDeleted)->whereIn('id', $res);
//        }
//    }
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function order()
    {
        return $this->hasMany("App\Orders", 'user_id');
    }

    public function user_type()
    {
        return $this->hasMany("App\User_Type", 'user_id');
    }

    public function car()
    {
        return $this->hasMany("App\Cars", 'driver_id');
    }

    public function laundry()
    {
        return $this->hasMany("App\Laundry", 'owner_id');
    }

    public function user_laundry()
    {
        return $this->belongsToMany("App\Laundry", 'laundry_driver', 'driver_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($user) { // before delete() method call this
            $user->user_type()->delete();
            //  $user->laundry()->delete();
            // do the rest of the cleanup...
        });
    }

    /**
     * Check if this is an admin
     * @return type
     */
    public static function isAdmin()
    {
        return (\Auth::user()->is_admin == 1) ? true : false;
    }

    public function activateAccount($code)
    {

        $user = User::where('activation_code', $code)->first();

        if ($user) {
            $user->update(['active' => 1, 'activation_code' => null]);
            return true;
        }
        return false;
    }

    public static function getByDistance($lat, $lng, $distance)
    {
        $distanceQuery = "SELECT
    *,
    ( 6371 * acos( cos( radians({$lat})) * cos( radians( `latitude` )) * cos( radians( `longitude` ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( `latitude` ) ) ) ) AS distance
FROM `laundry`
HAVING distance <= {$distance}
ORDER BY distance ASC";
        $results = DB::select(DB::raw($distanceQuery));
        return $results;
    }

    /**
     * User addresses
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_addresses()
    {
        return $this->hasMany('App\AddressUser');
    }

    public function voted_locations()
    {
        return $this->hasMany('App\VotedAddress');
    }
}
