<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Order extends Model
{
    protected $table = 'orders';

    public function order_detail()
    {
        return $this->hasMany("App\Orders_detail", 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo("App\User", 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo("App\User", 'driver_id');
    }

    public function laundry()
    {
        return $this->belongsTo("App\Laundry");
    }


//implement the attribute
    public function getOrderIdAttribute()
    {
        //if (!count(Input::all())) {
        return str_pad($this->id, 6, "0", STR_PAD_LEFT);
        //}
    }
}
