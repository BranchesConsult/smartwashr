<?php

function currencyConverter($from_Currency, $to_Currency, $amount) {
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);
    $encode_amount = $amount;
    $get = file_get_contents("https://www.google.com/finance/converter?a=$encode_amount&from=$from_Currency&to=$to_Currency");
    
    $get = explode("<span class=bld>", $get);
    
    $get = explode("</span>", $get[1]);
    $converted_currency = preg_replace("/[^0-9\.]/", "", $get[0]);
    return round($converted_currency, 2);
}


//function showRegionalPrice($amount, $from_Currency = 'USD') {
//    $code= country();
//    $to_Currency = ($code == 'SAR') ? 'SAR' : 'PKR';
//    return currencyConverter($from_Currency, $to_Currency, $amount);
//}

function numToArabic($str) {
    if ($str == '-')
        return $str;
    else if ($str == 'Free') {
        return "حر";
    } else {
        $ards = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $result = [];
        $str = str_split($str);
        for ($i = 0; $i < count($str); $i++) {
            $t = $str[$i];
            $result[$i] = $ards[$t];
        }
        $result = implode("", $result);
        return $result;
    }
}

function country() {
    $query = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . \Request::ip()));
   
    return (!empty($query['geoplugin_countryCode'])) ? $query['geoplugin_countryCode'] : 'SAR';
}
function currencyCode() {
    $query = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . \Request::ip()));
   return (!empty($query['geoplugin_currencyCode'])) ? $query['geoplugin_currencyCode'] : 'SAR';
}
?>