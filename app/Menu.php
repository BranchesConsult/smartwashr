<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Menu extends Model {
    use SoftDeletes;
    protected $table = 'menus';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'title', 'title_ar', 'parent_id', 'is_active', 'page_id'
    ];
    protected $softDelete = true;

    public function parent()
    {
    return $this->belongsTo(self::class, 'id');

}

public function children() {
    return $this->hasMany(self::class, 'id');
}

public function page() {
    return $this->belongsTo("App\Page", "page_id", 'id');
}

}
