<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    const PERCENTAGE = '0';
    const FLAT = 1;
    protected $table = 'coupons';
    protected $guarded = [];

    /**
     * Set start date time
     * @param $value
     */
    public function setValidToAttribute($value)
    {
        $this->attributes['valid_to'] = Carbon::parse($value);
    }

    /**
     * Set end date time
     * @param $value
     */
    public function setValidFromAttribute($value)
    {
        $this->attributes['valid_from'] = Carbon::parse($value);
    }

}
