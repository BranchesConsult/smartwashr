<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Page extends Model
{
    use SoftDeletes;
     protected $table = 'pages';
     protected $dates = ['deleted_at'];
    protected $fillable = [
        'title','slug','content','content_ar','deleted_at'
    ];
    protected $softDelete = true;
    public function menu() {
        return $this->hasOne('App\Menu', 'page_id', 'id');
    }
}
