<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
class Orders extends Model
{

    //
    protected $guarded = [];
//    protected $fillable = [
//        'laundry_id',
//        'user_id',
//        'total_price',
//        'post_code',
//        'address_one',
//        'address_two',
//        'status',
//        'discount',
//        'discount_type',
//        'delivery_instruction',
//        'collection_date_time',
//        'collection_date_time_to',
//        'delivery_date_time',
//        'total_price',
//    ];

    //protected $orderDirection = 'desc';
    public function newQuery($excludeDeleted = true)
    {
        $id = \Auth::id();
        $user = \App\User::find($id);
        $user_type = \App\User_Type::where('user_id', $id)->where('type_id', '1')->get()->toArray();
        if (@$user->is_admin == 1) {
            return parent::newQuery($excludeDeleted);
        } else if (count($user_type)) {
            $laundry = \App\Laundry::where('owner_id', $id)->get(['id'])->toArray();
            $res = array_flatten($laundry);
            return parent::newQuery($excludeDeleted)->whereIn('laundry_id', $res);
        } else {
            return parent::newQuery($excludeDeleted);
        }
    }

    public function order_detail()
    {
        return $this->hasMany("App\Orders_detail", 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo("App\User", 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo("App\User", 'driver_id');
    }

    public function laundry()
    {
        return $this->belongsTo("App\Laundry");
    }


	
//implement the attribute
    public function getOrderIdAttribute()
    {
        //if (!count(Input::all())) {
            return str_pad($this->id, 6, "0", STR_PAD_LEFT);
        //}
    }
}
