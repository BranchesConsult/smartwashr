<?php
/*
  template name: Front
 */

get_header();
?>
<style>
    #camera_my-first-slideshow{
        max-height: 379px !important;
        height: 379px !important;
        padding-top: 40px;

    }
    #camera_my-first-slideshow #home-right{
        text-align: left !important;
    }
</style>
<div id="ja-container" class="wrap ja-mf">
    <div class="main clearfix">
        <div id="ja-mainbody" style="width: 100%;">
            <!-- CONTENT -->
            <div id="ja-main" style="width: 100%;">
                <div class="inner clearfix">
                    <div id="ja-contentwrap" class="clearfix ">
                        <div id="ja-content" class="column" style="width: 100%;">
                            <div id="ja-current-content" class="column" style="width: 100%;">
                                <div id="ja-content-main" class="ja-content-main clearfix">
                                    <div class="blog-featured">
                                        <div class="items-leading">
                                            <div class="leading leading-0 clearfix">
                                                <div class="contentpaneopen">
                                                    <div class="article-content">
                                                        <?php echo do_shortcode("[camera slideshow='my-first-slideshow']"); ?>
                                                    </div>
                                                </div>
                                                <div class="item-separator"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //CONTENT -->

        </div>
    </div>
</div>
<div id="ja-botsl" class="wrap ">
    <div class="main">
        <div class="main-inner1 clearfix">

            <!-- SPOTLIGHT -->
            <div class="ja-box column ja-box-full" style="width: 100%;">
                <div id="Mod66" class="ja-moduletable moduletable clearfix">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();