<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressUser extends Model
{
    protected $table = 'addresses_user';
    protected $guarded = [];
    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
