<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fav_Location extends Model
{
    protected $table = 'favourite_location';
     protected $fillable = [
        'user_id', 'location'
    ];
}
