<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderComplain extends Model
{
    protected $table = 'order_complains';
    public $timestamps = false;
    protected $guarded = [];

    public function images()
    {
        return $this->hasMany('App\OrderComplainImage', 'complain_id');
    }
}
