/**
 * Auto compllete
 */
var autocompletePickup, autocompleteDrop;


function initAutocomplete() {
    jQuery(document).ready(function ($) {
        var currentUrl = window.location.pathname;
        var currentPath = currentUrl.split('/').filter((v) => (!!(v) == true));
        if (currentPath[currentPath.length - 1] == "select-car-form") {
            initMap();
        }
        //Appending input to form
        $('<input>').attr({
            type: 'hidden',
            id: 'journey_distance',
            name: 'journey_distance'
        }).appendTo('form#booking-distance');

        $('<input>').attr({
            type: 'hidden',
            id: 'pickup_lat',
            name: 'pickup_lat'
        }).appendTo('form#booking-distance');
        $('<input>').attr({
            type: 'hidden',
            id: 'pickup_lng',
            name: 'pickup_lng'
        }).appendTo('form#booking-distance');

        $('<input>').attr({
            type: 'hidden',
            id: 'dropoff_lat',
            name: 'dropoff_lat'
        }).appendTo('form#booking-distance');
        $('<input>').attr({
            type: 'hidden',
            id: 'dropoff_lng',
            name: 'dropoff_lng'
        }).appendTo('form#booking-distance');
    });

    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var autoCompleteOptions = {
        types: ['geocode', 'establishment'],
        componentRestrictions: {country: 'uk'}
    };

    autocompletePickup = new google.maps.places.Autocomplete(
        (document.getElementById('pickup_dest')),
        autoCompleteOptions
    );
    autocompleteDrop = new google.maps.places.Autocomplete(
        (document.getElementById('dropoff_dest')),
        autoCompleteOptions
    );

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocompletePickup.addListener('place_changed', fillInAddress);
    autocompleteDrop.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    if (typeof autocompletePickup.getPlace() != 'undefined') {
        var placePickup = autocompletePickup.getPlace();
        var placePickupLat = placePickup.geometry.location.lat();
        var placePickupLng = placePickup.geometry.location.lng();
        document.getElementById("pickup_lat").value = placePickupLat;
        document.getElementById("pickup_lng").value = placePickupLng;
    }

    if (typeof autocompleteDrop.getPlace() != 'undefined') {
        var placeDrop = autocompleteDrop.getPlace();
        var placeDropLat = placeDrop.geometry.location.lat();
        var placeDropLng = placeDrop.geometry.location.lng();
        document.getElementById("dropoff_lat").value = placeDropLat;
        document.getElementById("dropoff_lng").value = placeDropLng;
    }

    if (typeof autocompletePickup.getPlace() != 'undefined' && typeof autocompleteDrop.getPlace() != 'undefined') {
        var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(placePickupLat, placePickupLng), new google.maps.LatLng(placeDropLat, placeDropLng));
        var distance = Math.ceil(distance * 0.000621371192);
        document.getElementById('journey_distance').value = distance;
    }
}

function goToSelectCar() {
    jQuery(document).ready(function ($) {
        if ($("#pickup_dest").val().length > 0 && $("#dropoff_dest").val().length && $("#pickup_datetime").val().length > 0) {
            //Setting forms param
            var queryFormParams = {
                distance: document.getElementById('journey_distance').value,
                pickup_dest: document.getElementById('pickup_dest').value,
                dropoff_dest: document.getElementById('dropoff_dest').value,
                pickup_datetime: document.getElementById('pickup_datetime').value,
                dropoff_lat: document.getElementById('dropoff_lat').value,
                dropoff_lng: document.getElementById('dropoff_lng').value,
                pickup_lat: document.getElementById('pickup_lat').value,
                pickup_lng: document.getElementById('pickup_lng').value
            };
            jQuery("form#booking-distance").attr('action', "http://localhost/cabsnearme/select-car-form/" + decodeURIComponent(jQuery.param(queryFormParams)));
            // $("form#booking-distance").submit();
        } else {
            return false;
        }
    });
}


//Draw location point on map
function initMap() {
    var styledMapType = new google.maps.StyledMapType(
        [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]}, {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]}, {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]}, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]}, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}],
        {name: 'Styled Map'});

    var uluru = {};
    var usersCountry = document.getElementById('users_country_code').value;
    switch (usersCountry) {
        case 'PK':
            uluru = {lat: 31.549613, lng: 74.360588};
            break;
        case 'USA':
            uluru = {lat: 42.239003, lng: -83.260532};
            break;
        default:
            uluru = {lat: 21.347545, lng: 39.216549};
    }
    var map = new google.maps.Map(document.getElementById('map-contact'), {
        zoom: 4,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
    directionsService.route({
        origin: pointA,
        destination: pointB,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            console.error('Directions request failed due to ' + status);
        }
    });
}

