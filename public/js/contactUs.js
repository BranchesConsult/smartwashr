$(function () {
    
    $('#contactus').on('submit', function (e) {
        if(grecaptcha.getResponse()!=null)
            console.log('not null')
        else
            console.log('null')
        $.ajaxSetup({
            header: $('meta[name="_token"]').attr('content')
        })
        var name = $('#name').val();
        var email = $('#contactemail').val();
        var subject = $('#subject').val();
        var message = $('#contactmessage').val();
        var captcha = grecaptcha.getResponse();
        e.preventDefault(e);
        $('button[data-loading-text]').button('loading');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "POST",
            url: '/contactUs',
            data: {
                _token: CSRF_TOKEN, 
                name: name, 
                email: email, 
                subject: subject, 
                message: message,
                'g-recaptcha-response': captcha
            },
            dataType: 'json',
            success: function (data) {
                if (data.success == 1) {

                    $.toast({
                        heading: 'Success',
                        text: 'Message sent successfully',
                        icon: 'success',
                        position: 'bottom-right',
                        hideAfter: false
                    })
//                    $('#message').html('<div class="alert alert-success">' + data.message + '</div>');
//                    $('#message .alert').delay(3000).fadeOut();
//                    $("input[type=text],input[type=email], textarea").val("");
                } else {
                    $.toast({
                        heading: 'Warning',
                        text: 'Message sending failed',
                        icon: 'warning',
                        position: 'bottom-right',
                        hideAfter: false
                    })
//                    $('#message').html('<div class="alert alert-warning">' + data.message + '</div>');
//                    $('#message .alert').delay(3000).fadeOut();
                }
                $('button[data-loading-text]').button('reset');
            },
            error: function (data) {
                $('button[data-loading-text]').button('reset');
            }
        })
    });
});