<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'dryclean'=>'تنظيف جاف',
    'washing'=>'واش',
    'press'=>'كى الملابس',
    'profile' => 'الملف الشخصي',
    'order' => 'أوامر',
    'order_now'=>'اطلب الان',
    'whysmartwashr-style'=>'float:right;',
    'howitworks-style'=>'padding-top: 0cm;',
    'submit_now'=>'أرسل الآن',
    'product'=>'المنتج',
    'service'=>'خدمات',
    'quantity'=>'كمية',
    'unit_price'=>'سعر الوحدة',
    'total'=>'مجموع',
    'subtotal'=>'المجموع الفرعي',
    'checkout'=>'الدفع',
    'Phone#'=>'تقديم رقم الاتصال الخاص بك للاتصال',
    'collection_time'=>'جمع التاريخ والوقت',
    'delivery_time'=>'تاريخ التسليم والوقت',
    'currency'=>'الريال',
    
    'email-success' => 'تم إرسال البريد الإلكتروني بنجاح.',
    'email-failure' => 'حدث خطأ',
    'field-required'=>'الرجاء ملء هذه الخانة',
    'name'=>'اﻹﺳم',
    'Password'=>'كلمه السر',
    'contact_subject'=>'الموضوع',
    'email'=>'عنوان البريد الإلكتروني',
    'contact_msg'=>'اﻟرﺳﺎﻟﺔ',
    'remember_me'=>'تذكرنى',
    'forgot_password'=>'نسيت رقمك السري؟',
    'confirm_password'=>'تأكيد كلمة المرور',
    'register_as'=>'تسجيل باسم',
    'user_type'=>'حدد نوع المستخدم',
    'client'=>'زبون',
    'driver'=>'سائق',
    'laundry'=>'غسيل ملابس',
    
    'whysmartwashr_moto1' => 'اﺗﺻل ﺑﻧﺎ اﻟﺗواﺻل اﻹﺟﺗﻣﺎﻋﻲ اﻟﺗﺳﻌﯾرة ﺧدﻣﺎﺗﻧﺎ ﺳﻣﺎرت واﺷر ﺑﺎﻟﻘرب ﻣﻧك ﻛﯾف ﻧﻌﻣل',
    'moto1_p' => 'ونحن سوف يسلم اليكم، في أي وقت وفي أي مكان',
    'whysmartwashr_moto2' => 'اﺗﺻل ﺑﻧﺎ اﻟﺗواﺻل اﻹﺟﺗﻣﺎﻋﻲ اﻟﺗﺳﻌﯾرة ﺧدﻣﺎﺗﻧﺎ ﺳﻣﺎرت واﺷر ﺑﺎﻟﻘرب ﻣﻧك ﻛﯾف ﻧﻌﻣل',
    'moto2_p' => 'ونحن سوف يسلم اليكم، في أي وقت وفي أي مكان',
    'whysmartwashr_moto3' => 'اﺗﺻل ﺑﻧﺎ اﻟﺗواﺻل اﻹﺟﺗﻣﺎﻋﻲ اﻟﺗﺳﻌﯾرة ﺧدﻣﺎﺗﻧﺎ ﺳﻣﺎرت واﺷر ﺑﺎﻟﻘرب ﻣﻧك ﻛﯾف ﻧﻌﻣل',
    'moto3_p' => 'ونحن سوف يسلم اليكم، في أي وقت وفي أي مكان',

    'howitworks_moto1' => 'ﻧوﺻل ﻣﻼﺑﺳك ﻧظﯾﻔﺔ وﺑﺎﻟك ﻣرﺗﺎح',
    'howitworks_reason' => 'أﺳﺑﺎب إﺧﺗﯾﺎرك ﻟﻧﺎ',
    'howitworks_moto2' => 'اﺣﺟز ﻣن ﺧﻼل ﺗطﺑﯾق ﺳﻣﺎرت واﺷر اﻟذﻛﻲ',
    'moto2_p' => 'في نقرة واحدة حدد الوقت البيك اب التي تناسبك',
    'howitworks_moto3' => 'ﺗﺟﻣﯾﻊ ﻣن اﻟﻣﻧﺎزل',
    'howitworks_moto3_p' => 'يتم تسليم الغسيل المفضلة لديك مباشرة تنظيفها مباشرة لك',
    'howitworks_moto4' => 'دارك',
    'howitworks_moto4_p' => 'يتم تسليم الغسيل المفضلة لديك مباشرة تنظيفها مباشرة لك',
    
    'smartwashr_near_me_moto1' => 'أطﻠب ﺧدﻣﺔ اﻟﻐﺳﯾل واﻟﺗﻧظﯾف اﻟﺟﺎف ﻋﻠﻰ ھﺎﺗﻔك اﻵن ﺟﻣﻊ وﺗﺳﻠﯾم ﻣﺟﺎﻧﻲ',
    'smartwashr_near_me_collection' => 'جمع مجاني والتسليم',
    'smartwashr_near_me_app' => 'تحميل من المتجر',
    'smartwashr_near_me_playstore' => 'تحميل من جوجل اللعب',
    
    'services_moto1' => 'ﻣﻼﺑس ﻧظﯾﻔﺔ ! ﻟم ﺗﻛن أﺑداً ﺳﮭﻠﺔ؟ ',
    'services_how_it_works' => 'ﻛﯾف ﻧﻌﻣل ﻟﺧدﻣﺗك',
    'services_signup' => 'سجل',
    'services_signup_p' => 'يحصل جميع الأعضاء على مكافآت و / أو خصومات. اشترك للحصول على مزيد من المعلومات.في نقرة واحدة حدد الوقتﺣدد وﻗت اﻟﺟﻣﻊ
  الذي يناسبك',
    'services_pickup' => 'اﻟﺟﻣﻊ',
    'services_pickup_p' => 'اذا ﻛﻧت ﻓﻲ ﺣﺎﺟﺔ ﻟﻐﺳﯾل اﻟﻣﻼﺑس، ﯾﻣﻛﻧﻧﺎ أن ﻧﺄﺗﻲ ﻟﺟﻣﻌﮭﺎ ﻣن ﻣﻛﺎﻧك',
    'services_cleaning' => 'اﻟﺗﻧظﯾف',
    'services_cleaning_p' => 'ﻧﺣن ﻧﺳﺗﺧدم اﻟﻣواد اﻟﻣﻧﺎﺳﺑﺔ ﻟﻧوع اﻟﻐﺳﯾل ﻟدﯾك ﺑﺗﻘﻧﯾﺎﺗﻧﺎ اﻟﺣدﯾﺛﺔ، ﻧﺿﻣن ﻟك اﻟﻌﻧﺎﯾﺔ اﻟﻔﺎﺋﻘﺔ',
    'services_delivery' => 'اﻟﺗوﺻﯾل',
    'services_delivery_p' => 'التوصيل المجاني لكل طلب بناء على طلب مسبق في غضون 24 ساعة بعد التنظيف',
    
    'pricing_moto1' => 'التنظيف الجاف والغسيل بسيط',
    'pricing' => 'أسعارنا',
    'pricing_no_offers' => 'حاليا لا تتوفر عروض',
    'pricing_item'=>'بند',
    'pricing_dryclean'=>'تنظيف جاف وصحافة ريال',
    'pricing_washing'=>'غسل & صحافة ريال',
    'pricing_press'=>'فقط اضغط الريال ',
    'others' => 'سعر آخر الريال ',
    
    'community' => 'ﺗﻠﺗزم ﺳﻣﺎرت واﺷر ﺑﺗﻛرﯾس أﻓﺿل ﺟﮭودھﺎ ﻟﺗﻌزﯾز اﻟﺗﻧﻣﯾﺔ اﻟﻣﺟﺗﻣﻌﯾﺔ وﺗﻘدر إﻗﺗراﺣﺎت اﻟﺷرﻛﺎء اﻟﻣﺣﻠﯾﯾن ﻟﺗﺣﻘﯾق ھذا اﻟﮭدف، ﻧﻠﺗزم أﯾﺿﺎً  ( ﯾوﻣﺎً 60 ﺑﺗﻘدﯾم اﻟﻣﻼﺑس ﻏﯾر اﻟﻣطﺎﻟﺑﺔ) أﻛﺛر ﻣن ',
    'community_collection' => 'ﻟﻠﺟﻣﻌﯾﺎت اﻟﺧﯾرﯾﺔ',
    
    'contact' => 'راﺳﻠﻧﺎ',
    
    'footer'=>'سمارت واشر تساعد في معرفة عملائها على المستوى الشخصي والوصول إليهم بعروض ذات مغزى لهم. نقدم أيضاً خدمة جمع وتوصيل من وإلى المنازل والتي لا مثيل لها أمام منافسينا',
    'footer-contact'=>'معلومات الاتصال',
    'footer_KSA'=>'جدة، المملكة العربية السعودية',
    'footer_PK'=>'لاهور، باكستان',
    'copyright'=>'حقوق النشر&copy; -',
    'rights'=>'كل الحقوق محفوظة',
    'terms'=>'الأحكام والشروط',
    'policy'=>'سياسة الخصوصية',
    'processing'=>'معالجة الطلب',
    'send'=>'إرسال',
    
    'login'=>'تسجيل الدخول',
    'register'=>'تسجيل',
    'language'=>'اختار اللغة',
    'Arabic'=>'عربى',
    'English'=>'الإنجليزية',
    '24/7'=>'نحن منفتحون 7أيام في الأسبوع 8:00حتي 09:00'
    ];
