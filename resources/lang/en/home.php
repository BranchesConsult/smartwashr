<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */
    'dryclean'=>'Dryclean',
    'washing'=>'Washing',
    'press'=>'Press',
    'profile' => 'Profile',
    'order' => 'Orders',
    'order_now'=>'Add to cart',
    'submit_now'=>'Submit Now',
    'product'=>'Product',
    'service'=>'Services',
    'quantity'=>'Quantity',
    'unit_price'=>'Unit Price',
    'total'=>'Total',
    'subtotal'=>'Sub-Total',
    'checkout'=>'CheckOut',
    'Phone#'=>'Your phone (we will contact you on this number)',
    'collection_time'=>'Collection Date & Time',
    'delivery_time'=>'Delivery Date & Time',
    'currency'=>'SAR',
    
    'whysmartwashr-style' => 'float:left;',
    'howitworks-style' => 'padding-top: 0cm;',
    'email-success' => 'Email sent Successfully.',
    'email-failure' => 'Ooopps ! error occurred.',
    'field-required' => 'This Fields id required',
    'email' => 'E-Mail Address',
    'Password' => 'Password',
    'name' => 'Enter Name',
    'contact_subject' => 'Your Subject',
    'contact_msg' => 'Message',
    'remember_me' => 'Remember Me',
    'forgot_password' => 'Forgot Your Password?',
    'confirm_password' => 'Confirm Password',
    'register_as' => 'Register As',
    'user_type' => 'Select User Type',
    'client' => 'Client',
    'driver' => 'Driver',
    'laundry' => 'Laundry',
    'KSA#' => '+96 654 7771807',
    'PK#' => '+92 321 8450305',
    'whysmartwashr_moto1' => 'Dry Cleaning & Laundry to Your Door',
    'moto1_p' => 'We will deliver back to you, anytime and anywhere',
    'whysmartwashr_moto2' => 'Dry Cleaning & Laundry to Your Door',
    'moto2_p' => 'We will deliver back to you, anytime and anywhere',
    'whysmartwashr_moto3' => 'Dry Cleaning & Laundry to Your Door',
    'moto3_p' => 'We will deliver back to you, anytime and anywhere',
    'howitworks_moto1' => 'Delivering Clean Clothes and Peace of Mind',
    'howitworks_reason' => 'Reasons to choose us',
    'howitworks_moto2' => 'Book through your Smart washr app',
    'moto2_p' => 'In one click select the pick-up time that best suits you.',
    'howitworks_moto3' => 'Pick Up Pilot',
    'howitworks_moto3_p' => 'Your favorite laundry delivered freshly cleaned direct to you.',
    'howitworks_moto4' => 'Delivery At Your Doorstep',
    'howitworks_moto4_p' => 'Your favorite laundry delivered freshly cleaned direct to you.',
    'smartwashr_near_me_moto1' => 'Order Laundry and Dry Cleaning From Your Smart Phone',
    'smartwashr_near_me_collection' => '15 SAR Delivery Fees',
    'smartwashr_near_me_app' => 'download from the app store',
    'smartwashr_near_me_playstore' => 'download from the google play',
    'services_moto1' => 'Clean Clothes Have Never Been This Easy!',
    'services_how_it_works' => 'how our service works',
    'services_signup' => 'Sign Up',
    'services_signup_p' => 'All members receive bonuses and/or discounts. Sign up for more information.In one click select the pick-up time that best suits you',
    'services_pickup' => 'Pick Up',
    'services_pickup_p' => 'If you are in an urgent need of laundry, we can come right to you for a pick-up.',
    'services_cleaning' => 'Cleaning',
    'services_cleaning_p' => 'We use premium materials, technologies and guarantee treatment with care.',
    'services_delivery' => 'Delivery',
    'services_delivery_p' => '15 SAR for every order upon prior request within 24 hours after cleaning.',
    'pricing_moto1' => 'Dry Cleaning and Laundry Made Simple',
    'pricing' => 'Our Prices',
    'pricing_no_offers' => 'Currently no offers available',
    'pricing_item' => 'Item',
    'pricing_dryclean' => 'Dry Clean & Press (SAR)',
    'pricing_washing' => 'Wash & Press (SAR)',
    'pricing_press' => 'Only Press (SAR)',
    'others' => 'Other Price (SAR)',
    'community' => 'Smartwashr is committed to devote its best efforts in promoting community development and appreciates suggestions from local partners to achieve this objective. Unclaimed clothes (more then sixty days) will be offered to local charity organizations.',
    'community_collection' => '10 SAR Delivery Fees',
    'contact' => 'Contact Us',
    'footer' => 'Smart Washr enables laundry industry to know their customers on a personal level and reach them with meaningful offers. Our value add proposition includes a door step pick up and delivery services that are unmatched by any other solution.',
    'footer-contact' => 'Contact info',
    'footer_KSA' => 'Jeddah, KSA',
    'footer_PK' => 'Lahore, PK',
    'footer_USA' => '9509 Birch Street, Taylor, Michigan 48180, United states',
    'copyright' => 'Copyright &copy; -',
    'rights' => ' All rights reserved.',
    'terms' => 'Terms And Conditions',
    'policy' => 'Privacy Policy',
    'processing' => 'Processing Request',
    'send' => 'SEND',
    'login' => 'LOGIN',
    'register' => 'REGISTER',
    'language' => 'Select Language',
    'Arabic' => 'Arabic',
    'English' => 'English',
    '24/7' => 'We are open 7 days a week 8am - 9pm'
];


