<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
    @endif

    <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Users') }}</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                    <li><a href="{{route('viewUsers')}}"></i> <span>{{ trans('All Users') }}</span></a></li>
                    @if( Auth::user()->is_admin == 1)
                        <li><a href="{{route('addUser')}}">{{ trans('Add User') }}</a></li>
                    @endif
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Orders') }}</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('viewOrders')}}">{{ trans('View Orders') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Category') }}</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('viewCategories')}}">{{ trans('View Categories') }}</a></li>
                    @if( Auth::user()->is_admin == 1)
                        <li><a href="{{route('addCategory')}}">{{ trans('Add Category') }}</a></li>
                    @endif
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Products') }}</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('viewProducts')}}">{{ trans('View Products') }}</a></li>
                    @if( Auth::user()->is_admin == 1)
                        <li><a href="{{route('addProduct')}}">{{ trans('Add Products') }}</a></li>
                    @endif
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Laundry') }}</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('viewLaundry')}}">{{ trans('View Laundry') }}</a></li>
                    @if( Auth::user()->is_admin == 1)
                        <li><a href="{{route('addLaundry')}}">{{ trans('Add Laundry') }}</a></li>
                    @endif
                </ul>
            </li>
            @if( Auth::user()->is_admin == 1)
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-link'></i> <span>
                            Coupons
                        </span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('all_coupons')}}">All Coupons</a></li>
                        <li><a href="{{route('add_coupons')}}">Add Coupon</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-link'></i> <span>
                            Push notifications
                        </span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        {{--<li><a href="#">All Notifications</a></li>--}}
                        <li><a href="{{route('push.frm')}}">Add Notification</a></li>
                    </ul>
                </li>
            @endif
        <!--            <li class="treeview">
                            <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Car') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('viewCars')}}">{{ trans('View Car') }}</a></li>
                                <li><a href="{{route('addCar')}}">{{ trans('Add Car') }}</a></li>
                            </ul>
                        </li>-->
            @if( Auth::user()->is_admin == 1)
                <li class="treeview">
                    <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Menu') }}</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('viewMenus')}}">{{ trans('View Menu') }}</a></li>
                        <li><a href="{{route('addMenu')}}">{{ trans('Add Menu') }}</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-link'></i> <span>{{ trans('Page') }}</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('viewPages')}}">{{ trans('View Page') }}</a></li>
                        <li><a href="{{route('addPage')}}">{{ trans('Add Page') }}</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{!! route('getVotedAddress') !!}"><span>Voted locations</span></i></a>
                </li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
