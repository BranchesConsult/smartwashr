<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show

<!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
<body class="skin-green-light sidebar-mini">
<div id="app">
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('main-content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        @include('adminlte::layouts.partials.controlsidebar')
        @include('adminlte::layouts.partials.footer')

    </div><!-- ./wrapper -->
</div>
@include('adminlte::layouts.partials.scripts')
{!! Html::script('js/jquery-3.1.1.min.js') !!}
<script type="text/javascript">
    function changeOrderReadStatus() {
        $.ajax({
            url: "{{route('orderSeenByAdmin')}}",
            type: "GET",
            async: false,
            dataType: 'json',
            data: {},
            success: function (res) {
                console.log(res, 'success');
                $("#count-order-notification").html('0');
            },
            error: function (xhr, status, errorThrown) {
                console.error(xhr.status, xhr.responseText);
            }
        });
    }
</script>
@section('scripts')
@show
</body>
</html>
