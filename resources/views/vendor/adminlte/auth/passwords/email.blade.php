@extends('layouts.app')
@section('title')
Reset Password
@stop
@section('content')
 <style>
    .content-section{
        min-height: 68%;
    }
    .reset_pass{
            padding-left: 15px;
    }
</style>  
<section class="bg-white content-section">
<div class="col-md-3"></div>
<div class="col-md-6">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg">Reset Password</p>
            <form action="{{ url('/password/email') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Enter Your Email" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="row reset_pass">
                  
                  
                        <button type="submit" class="btn btn-primary ">{{ trans('adminlte_lang::message.sendpassword') }}</button>
                    
                    
                </div>
            </form>

        </div><!-- /.login-box-body -->
</div>
        <div class="col-md-3"></div>
</section>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

@stop
