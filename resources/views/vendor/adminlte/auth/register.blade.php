@extends('layouts.app')
@section('title')
Register
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1 class="text-center">
            {{ __('home.register') }}
            <hr class="light" />
        </h1>
        <form class="form-horizontal" role="form" id="user_signup" method="POST" action="{{ route('register') }}"  >
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">{{ __('home.name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">{{ __('home.email') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">{{ __('home.Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">{{ __('home.confirm_password') }}</label>

                <div class="col-md-6">
                    <input id="password_confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <!--<div class="form-group" id="type">
                <label for="password-confirm" class="col-md-4 control-label">{{ __('home.register_as') }}</label>
                <div class="col-md-6">
                    <select class="form-control" id="user_type" name='user_type' required>
                        <option value="">{{ __('home.user_type') }}</option>
                        <option value="2">{{ __('home.driver') }}</option>
                        <option value="3">{{ __('home.client') }}</option>
                    </select>
                </div>
            </div>-->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('home.register') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="js/jquery/dist/jquery.validate.min.js"></script>
@if((\Session::get('language') == 'ar'))
<script src="js/jquery/dist/localization/messages_ar.min.js"></script>
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$('#user_signup').validate({
    lang: "{!! (\Session::get('language') == 'ar') ? 'ar':'en' !!}",
    rules: {
        password: {
            pwRestrict: true,
            minlength: 6
        },
        password_confirm: {
            required: true,
            equalTo: '#password'
        }

    }
});
$.validator.addMethod("pwRestrict", function (value) {
    return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
}, "Password must have at least six characters with at least one alphabet and one number");
</script>
@stop
