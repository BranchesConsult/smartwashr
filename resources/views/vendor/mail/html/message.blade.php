     @extends('emails.emailLayout')
@section('content')
    {{-- Body --}}
    {{ $slot }}
    {{-- Subcopy --}}
    @if (isset($subcopy))
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endif
@stop