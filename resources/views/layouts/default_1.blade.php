<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SmartWashr</title>

        <!-- Bootstrap Core CSS -->
        <link href="../theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link property="stylesheet" rel="stylesheet" id="gowash-shortcodes-style-css" href="wp-content/plugins/trx_utils/shortcodes/theme.shortcodes.css" type="text/css" media="all">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link property="stylesheet" rel="stylesheet" id="gowash-fontello-style-css" href="wp-content/themes/gowash/css/fontello/css/fontello.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-theme-style-css" href="wp-content/themes/gowash/css/theme.css" type="text/css" media="all">
        <!-- Plugin CSS -->
        <link href="../theme/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="../theme/css/half-slider.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="../theme/css/creative.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="wp-content/themes/gowash/style.css">
        <link href="../theme/css/style.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top">
        <div id="image">

        </div>
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll imgwrapper" href="#page-top">
                        <img src="img/logo_100x180_2.png" class="logo_main img-responsive" alt="">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="page-scroll" href="#about">Why SmartWashr</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#services">How It Works</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#portfolio">SmartWashr Near Me</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Services</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Pricing</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Community</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- Half Page Image Background Carousel Header -->
        <header id="myCarousel" class="carousel slide">

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('../theme/img/header.jpg');"></div>
                   
                    <div class="header-content">
                        <div class="header-content-inner">
                            <h1 class="homeHeading">
                                Save Time for More<br>
                                Important Things
                            </h1>
                            <hr>
                            <p>We�ll take care about cleanness </p>
                            <a href="#about" class="btn btn-primary btn-xl page-scroll">ORDER NOW</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('../theme/img/slide2.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Caption 2</h2>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('../theme/img/slide3.jpg');"></div>
                    <div class="carousel-caption">
                        <h2>Caption 3</h2>
                    </div>
                </div>
            </div>
        </header>
        <section class="bg-primary" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-heading">We've got what you need!</h2>
                        <hr class="light">
                        <p class="text-faded">Start Bootstrap has everything you need to get your new website up and running in no time! All of the templates and themes on Start Bootstrap are open source, free to download, and easy to use. No strings attached!</p>
                        <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Get Started!</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Delivering Clean Clothes and Peace of Mind</h2>
                        <hr class="primary">
                        <p>reasons to choose us:</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <div class="sc_services_item_featured post_featured">
                                <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-3.jpg" data-title="Professional Care">
                                    <a class="hover_icon hover_icon_link" href="services/professional-care/index.html">
                                        <img class="wp-post-image" width="300" height="300" alt="Professional Care" src="img/img_2.png">
                                    </a> 
                                </div>
                            </div>
                            <h3 style="font-weight:bold;font-size:14px;">Book through your Smart washr app</h3>
                            <p class="text-muted">In one click select the pick-up time that best suits you.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <div class="sc_services_item_featured post_featured">
                                <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-2.jpg" data-title="Fast Delivery">
                                    <a class="hover_icon hover_icon_link" href="img/img_3.png">
                                        <img class="wp-post-image" width="300" height="300" alt="Fast Delivery" src="img/img_3.png">
                                    </a> 
                                </div>
                            </div>
                            <h3 style="font-weight:bold;font-size:14px;">Pick Up Pilot</h3>
                            <p class="text-muted">Your favorite laundry delivered freshly cleaned direct to you.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <div class="sc_services_item_featured post_featured">
                                <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-1.jpg" data-title="Excellent Results">
                                    <a class="hover_icon hover_icon_link" href="services/excellent-results/index.html">
                                        <img class="wp-post-image" width="300" height="300" alt="Excellent Results" src="img/img_4.png">
                                    </a> 
                                </div>
                            </div>
                            <h3 style="font-weight:bold;font-size:14px;">Delivery at your doorstep</h3>
                            <p class="text-muted">Your favorite laundry delivered freshly cleaned direct to you.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="no-padding vc_custom" id="portfolio">
            <div class="container-fluid">
                <div class="row no-gutter popup-gallery">

                </div>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Clean Clothes Have Never Been This Easy!</h2>
                        <hr class="primary">
                        <p>how our service works:</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div id="sc_services_1543219316_1" class="sc_services_item sc_services_item_1 odd first">
                            <a href="services/sign-up/index.html">
                                <span class="sc_icon icon-icon1"></span>
                            </a> 
                            <div class="sc_services_item_content">
                                <h4 class="sc_services_item_title">
                                    <a href="services/sign-up/index.html">Sign Up</a>
                                </h4>
                                <div class="sc_services_item_description">
                                    <p>All members receive bonuses and/or discounts. Sign up for more information.</p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3"> 
                        <div id="sc_services_1543219316_2" class="sc_services_item sc_services_item_2 even">
                            <a href="services/pick-up/index.html"><span class="sc_icon icon-icon2"></span></a> <div class="sc_services_item_content">
                                <h4 class="sc_services_item_title"><a href="services/pick-up/index.html">Pick Up</a></h4>
                                <div class="sc_services_item_description">
                                    <p>If you are in an urgent need of laundry, we can come right to you for a pick-up.</p> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3"> 
                        <div id="sc_services_1543219316_3" class="sc_services_item sc_services_item_3 odd">
                            <a href="services/cleaning/index.html"><span class="sc_icon icon-icon3"></span></a> <div class="sc_services_item_content">
                                <h4 class="sc_services_item_title"><a href="services/cleaning/index.html">Cleaning</a></h4>
                                <div class="sc_services_item_description">
                                    <p>We use premium materials, technologies and guarantee treatment with care.</p> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div id="sc_services_1543219316_4" class="sc_services_item sc_services_item_4 even">
                            <a href="services/delivery/index.html"><span class="sc_icon icon-icon4"></span></a> <div class="sc_services_item_content">
                                <h4 class="sc_services_item_title"><a href="services/delivery/index.html">Delivery</a></h4>
                                <div class="sc_services_item_description">
                                    <p>Free delivery for every order upon prior request within 24 hours after cleaning</p> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1467891437670">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="sc_promo sc_promo_style_1 sc_promo_size_large">
                                            <div class="sc_promo_inner">
                                                <div class="sc_promo_image" style="background-image:url(wp-content/uploads/2016/06/bg_banner.jpg);width:49%;left: 0;"></div>
                                                <div class="sc_promo_block sc_align_none" style="width: 51%; float: right;">
                                                    <div class="sc_promo_block_inner">
                                                        <h2 class="sc_promo_title sc_item_title">Shirt Service from $2 per Shirt</h2>
                                                        <div class="sc_promo_button sc_item_button">
                                                            <a href="#" class="sc_button sc_button_round sc_button_style_border sc_button_size_small inverse_color ">Full price list</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Dry Cleaning and Laundry Made Simple</h2>
                        <hr class="primary">
                        <p>how our service works:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <!-- Plan One -->
                        <div class="col-lg-4 col-md-4">
                            <div class="sc_price_block sc_price_block_style_1">
                                <p>6 clothes</p>
                                <p>Dry Cleaning / Launder & Press</p>
                                <ul>
                                    <li>Shirts &#8211; $2.50</li>
                                    <li>Pants &#8211; $7</li>
                                    <li>Blouses &#8211; $7</li>
                                </ul>
                                <div class="sc_price">
                                    <span class="sc_price_currency">$</span>
                                    <span class="sc_price_money">9</span>
                                    <span class="sc_price_info">
                                        <span class="sc_price_penny">00</span>
                                        <span class="sc_price_period_empty"></span>
                                    </span>
                                </div>
                                <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
                            </div>
                        </div>
                        <!-- Plan Two -->
                        <div class="col-lg-4 col-md-4">
                            <div class="sc_price_block sc_price_block_style_1">
                                <p>6 clothes</p>
                                <p>Dry Cleaning / Launder & Press</p>
                                <ul>
                                    <li>Shirts &#8211; $2.50</li>
                                    <li>Pants &#8211; $7</li>
                                    <li>Blouses &#8211; $7</li>
                                </ul>
                                <div class="sc_price">
                                    <span class="sc_price_currency">$</span>
                                    <span class="sc_price_money">9</span>
                                    <span class="sc_price_info">
                                        <span class="sc_price_penny">00</span>
                                        <span class="sc_price_period_empty"></span>
                                    </span>
                                </div>
                                <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
                            </div>
                        </div>
                        <!-- plan three -->
                        <div class="col-lg-4 col-md-4">
                            <div class="sc_price_block sc_price_block_style_1">
                                <p>6 clothes</p>
                                <p>Dry Cleaning / Launder & Press</p>
                                <ul>
                                    <li>Shirts &#8211; $2.50</li>
                                    <li>Pants &#8211; $7</li>
                                    <li>Blouses &#8211; $7</li>
                                </ul>
                                <div class="sc_price">
                                    <span class="sc_price_currency">$</span>
                                    <span class="sc_price_money">9</span>
                                    <span class="sc_price_info">
                                        <span class="sc_price_penny">00</span>
                                        <span class="sc_price_period_empty"></span>
                                    </span>
                                </div>
                                <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-background-color">
                        <h6>
                            Smartwashr is committed to devote its best efforts in promoting community development and appreciates suggestions from local partners to achieve this objective.

                            Unclaimed clothes (more then sixty days) will be offered to local charity organizations.</h6><h6 class="sc_section_subtitle sc_item_subtitle">free collection and delivery</h6><div class="sc_section_content_wrap"><a href="#" class="sc_button sc_button_round sc_button_style_border sc_button_size_small alignleft margin_top_null margin_right_null margin_bottom_null  sc_button_iconed none">more details</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Dry Cleaning and Laundry Made Simple</h2>
                        <hr class="primary">
                        <p>how our service works:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="col-lg-4 col-md-4">
                            <img src="wp-content/uploads/2016/06/post-7.jpg" alt="">
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <img src="wp-content/uploads/2016/06/post-6.jpg" alt="">
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <img src="wp-content/uploads/2016/06/post-5.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer_wrap widget_area scheme_original">
            <div class="container">
                <div class="content_wrap">
                    <div class="columns_wrap">
                        <aside id="gowash_widget_socials-2" class="widget_number_1 column-1_4 widget widget_socials"> <div class="widget_inner">
                                <div class="logo">
                                    <a href="index.html"><img src="img/logo_100x180_2.png" class="logo_main" alt="" width="324" height="124"></a>
                                </div>
                                <div class="logo_descr">Smart Washr enables laundry industry to know their customers on a personal level and reach them with meaningful offers. Our value add proposition includes a door step pick up and delivery services that are unmatched by any other solution. </div>
                            </div>
                        </aside>
                        <aside id="nav_menu-3" class="widget_number_3 column-1_4 widget widget_nav_menu"><h3 class="widget_title">Services</h3><div class="menu-footer-menu-container">
                                <ul id="menu-footer-menu" class="menu">
                                    <li id="menu-item-282" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-282"><a href="shirt-laundry/index.html">Laundry</a></li>
                                    <li id="menu-item-284" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-284"><a href="silk-and-suede/index.html">Dry Clean</a></li>
                                    <li id="menu-item-285" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-285"><a href="alternations-and-repairs/index.html">Rugs</a></li>
                                </ul>
                            </div>
                        </aside>
                        <aside id="text-3" class="widget_number_4 column-1_4 widget widget_text"><h3 class="widget_title">Contact Info</h3> <div class="textwidget">
                                Jeddah, KSA<div class="info icon-phone">00966 54 777 1807</div>
                                Lahore, PK<div class="info icon-phone">00966 54 777 1807</div>
                                <span style="text-decoration: underline;">
                                    info@smartwashr.com
                                    <a class="__cf_email__" href="cdn-cgi/l/email-protection.html" data-cfemail=".com">[email&#160;protected]</a>                                         
                                </span>
                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny"><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div></div></div>
                        </aside>

                    </div>  
                </div>  
            </div>  
        </footer>  


        <!-- jQuery -->
        <script src="../theme/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../theme/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="../theme/vendor/scrollreveal/scrollreveal.min.js"></script>
        <script src="../theme/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

        <!-- Theme JavaScript -->
        <script src="../theme/js/creative.min.js"></script>
        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 100000 //changes the speed
            })
        </script>
    </body>
</html>
