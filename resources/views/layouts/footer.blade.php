<div class="copyright_wrap copyright_style_text  scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
            <div class="copyright_text">Copyright&copy; - {{date('Y')}}.All rights reserved. <a href="{{route('termsAndCondition')}}">Terms of Use</a> & <a href="{{route('privacyPolicy')}}">Privacy Policy</a></div>
        </div>
    </div>
</div>

