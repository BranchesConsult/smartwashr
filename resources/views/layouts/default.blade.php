<!doctype html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700"><style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style type="text/css">.gm-style { font: 400 11px Roboto, Arial, sans-serif;text-decoration: none; }.gm-style img { max-width: none; }</style>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="pingback" href="xmlrpc.html">
        <title>Smart Washr</title>
        <link rel="stylesheet" type="text/css" href="wp-content/themes/gowash/style.css">
        <link rel="dns-prefetch" href="http://maps.google.com/">
        <link rel="dns-prefetch" href="http://fonts.googleapis.com/">
        <link rel="dns-prefetch" href="http://s.w.org/">
        <link rel="alternate" type="application/rss+xml" title="GoWash � Laundry » Comments Feed" href="comments/feed/index.html">
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/", "ext":".png", "svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/", "svgExt":".svg", "source":{"concatemoji":"http:\/\/gowash.ancorathemes.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.3"}};
!function(a, b, c){function d(a){var c, d, e, f, g, h = b.createElement("canvas"), i = h.getContext && h.getContext("2d"), j = String.fromCharCode; if (!i || !i.fillText)return!1; switch (i.textBaseline = "top", i.font = "600 32px Arial", a){case"flag":return i.fillText(j(55356, 56806, 55356, 56826), 0, 0), !(h.toDataURL().length < 3e3) && (i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), c = h.toDataURL(), i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 55356, 57096), 0, 0), d = h.toDataURL(), c !== d); case"diversity":return i.fillText(j(55356, 57221), 0, 0), e = i.getImageData(16, 16, 1, 1).data, f = e[0] + "," + e[1] + "," + e[2] + "," + e[3], i.fillText(j(55356, 57221, 55356, 57343), 0, 0), e = i.getImageData(16, 16, 1, 1).data, g = e[0] + "," + e[1] + "," + e[2] + "," + e[3], f !== g; case"simple":return i.fillText(j(55357, 56835), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]; case"unicode8":return i.fillText(j(55356, 57135), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]; case"unicode9":return i.fillText(j(55358, 56631), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]}return!1}function e(a){var c = b.createElement("script"); c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)}var f, g, h, i; for (i = Array("simple", "flag", "unicode8", "diversity", "unicode9"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]); c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function(){c.DOMReady = !0}, c.supports.everything || (g = function(){c.readyCallback()}, b.addEventListener?(b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)):(a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function(){"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji?e(f.concatemoji):f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))}(window, document, window._wpemojiSettings);
        </script><script src="http://gowash.ancorathemes.com/wp-includes/js/wp-emoji-release.min.js?ver=4.6.3" type="text/javascript"></script>
        <style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-0.1em!important;background:none!important;padding:0!important;}</style>
        <link property="stylesheet" rel="stylesheet" id="booked-tooltipster-css" href="wp-content/plugins/booked/js/tooltipster/css/tooltipster9b70.css?ver=3.3.0" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="booked-tooltipster-theme-css" href="wp-content/plugins/booked/js/tooltipster/css/themes/tooltipster-light9b70.css?ver=3.3.0" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="booked-animations-css" href="wp-content/plugins/booked/css/animations055b.css?ver=1.9.6" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="booked-styles-css" href="wp-content/plugins/booked/css/styles055b.css?ver=1.9.6" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="booked-responsive-css" href="wp-content/plugins/booked/css/responsive055b.css?ver=1.9.6" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="essential-grid-plugin-settings-css" href="wp-content/plugins/essential-grid/public/assets/css/settings5bfb.css?ver=2.0.9.1" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="tp-open-sans-css" href="http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&amp;ver=4.6.3" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="tp-raleway-css" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&amp;ver=4.6.3" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="tp-droid-serif-css" href="http://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&amp;ver=4.6.3" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="rs-plugin-settings-css" href="wp-content/plugins/revslider/public/assets/css/settingsa88c.css?ver=5.3.0.2" type="text/css" media="all">
        <style id="rs-plugin-settings-inline-css" type="text/css">#rs-demo-id{}.tp-caption.Default-Title,.Default-Title{color:rgba(255,255,255,1.00);font-size:55px;line-height:60px;font-weight:400;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Descr,.Default-Descr{color:rgba(255,255,255,1.00);font-size:14px;line-height:26px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Button,.Default-Button{color:rgba(255,255,255,1.00);font-size:14px;line-height:16px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Clear,.Default-Clear{color:rgba(255,255,255,1.00);font-size:14px;line-height:22px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.ares.tp-bullets {
            }
            .ares.tp-bullets:before {
                content:" ";
                position:absolute;
                width:100%;
                height:100%;
                background:transparent;
                padding:10px;
                margin-left:-10px;margin-top:-10px;
                box-sizing:content-box;
            }
            .ares .tp-bullet {
                width:13px;
                height:13px;
                position:absolute;
                background:rgba(229, 229, 229, 1);
                border-radius:50%;
                cursor: pointer;
                box-sizing:content-box;
            }
            .ares .tp-bullet:hover,
            .ares .tp-bullet.selected {
                background:rgba(255, 255, 255, 1);
            }
            .ares .tp-bullet-title {
                position:absolute;
                color:136, 136, 136;
                font-size:12px;
                padding:0px 10px;
                font-weight:600;
                right:27px;
                top:-4px;  
                background:rgba(255,255,255,0.75);
                visibility:hidden;
                transform:translatex(-20px);
                -webkit-transform:translatex(-20px);
                transition:transform 0.3s;
                -webkit-transition:transform 0.3s;
                line-height:20px;
                white-space:nowrap;
            }     

            .ares .tp-bullet-title:after {
                width: 0px;
                height: 0px;
                border-style: solid;
                border-width: 10px 0 10px 10px;
                border-color: transparent transparent transparent rgba(255,255,255,0.75);
                content:" ";
                position:absolute;
                right:-10px;
                top:0px;
            }

            .ares .tp-bullet:hover .tp-bullet-title{
                visibility:visible;
                transform:translatex(0px);
                -webkit-transform:translatex(0px);
            }

            .ares .tp-bullet.selected:hover .tp-bullet-title {
                background:rgba(255, 255, 255, 1);}
            .ares .tp-bullet.selected:hover .tp-bullet-title:after {
                border-color:transparent transparent transparent rgba(255, 255, 255, 1);
            }
            .ares.tp-bullets:hover .tp-bullet-title {
                visibility:hidden;

            }
            .ares.tp-bullets:hover .tp-bullet:hover .tp-bullet-title {
                visibility:visible;
                transform:translateX(0px) translatey(0px);
                -webkit-transform:translateX(0px) translatey(0px);
            }


            /* VERTICAL */
            .ares.nav-dir-vertical.nav-pos-hor-left .tp-bullet-title { right:auto; left:27px;  transform:translatex(20px); -webkit-transform:translatex(20px);}  
            .ares.nav-dir-vertical.nav-pos-hor-left .tp-bullet-title:after { 
                border-width: 10px 10px 10px 0 !important;
                border-color: transparent rgba(255,255,255,0.75) transparent transparent;
                right:auto !important;
                left:-10px !important;   
            }
            .ares.nav-dir-vertical.nav-pos-hor-left .tp-bullet.selected:hover .tp-bullet-title:after {
                border-color:  transparent rgba(255, 255, 255, 1) transparent transparent !important;
            }



            /* HORIZONTAL BOTTOM && CENTER */
            .ares.nav-dir-horizontal.nav-pos-ver-center .tp-bullet-title,
            .ares.nav-dir-horizontal.nav-pos-ver-bottom .tp-bullet-title { top:-35px; left:50%; right:auto; transform: translateX(-50%) translateY(-10px);-webkit-transform: translateX(-50%) translateY(-10px); }  

            .ares.nav-dir-horizontal.nav-pos-ver-center .tp-bullet-title:after,
            .ares.nav-dir-horizontal.nav-pos-ver-bottom .tp-bullet-title:after { 
                border-width: 10px 10px 0px 10px;
                border-color: rgba(255,255,255,0.75) transparent transparent transparent;
                right:auto;
                left:50%;
                margin-left:-10px;
                top:auto;
                bottom:-10px;

            }
            .ares.nav-dir-horizontal.nav-pos-ver-center .tp-bullet.selected:hover .tp-bullet-title:after,
            .ares.nav-dir-horizontal.nav-pos-ver-bottom .tp-bullet.selected:hover .tp-bullet-title:after {
                border-color:  rgba(255, 255, 255, 1) transparent transparent transparent;
            }

            .ares.nav-dir-horizontal.nav-pos-ver-center .tp-bullet:hover .tp-bullet-title,
            .ares.nav-dir-horizontal.nav-pos-ver-bottom .tp-bullet:hover .tp-bullet-title{
                transform:translateX(-50%) translatey(0px);
                -webkit-transform:translateX(-50%) translatey(0px);
            }


            /* HORIZONTAL TOP */
            .ares.nav-dir-horizontal.nav-pos-ver-top .tp-bullet-title { top:25px; left:50%; right:auto; transform: translateX(-50%) translateY(10px);-webkit-transform: translateX(-50%) translateY(10px); }  
            .ares.nav-dir-horizontal.nav-pos-ver-top .tp-bullet-title:after { 
                border-width: 0 10px 10px 10px;
                border-color:  transparent transparent rgba(255,255,255,0.75) transparent;
                right:auto;
                left:50%;
                margin-left:-10px;
                bottom:auto;
                top:-10px;

            }
            .ares.nav-dir-horizontal.nav-pos-ver-top .tp-bullet.selected:hover .tp-bullet-title:after {
                border-color:  transparent transparent  rgba(255, 255, 255, 1) transparent;
            }

            .ares.nav-dir-horizontal.nav-pos-ver-top .tp-bullet:hover .tp-bullet-title{
                transform:translateX(-50%) translatey(0px);
                -webkit-transform:translateX(-50%) translatey(0px);
            }


        </style>
        <link property="stylesheet" rel="stylesheet" id="woocommerce-layout-css" href="wp-content/plugins/woocommerce/assets/css/woocommerce-layout91ac.css?ver=2.6.8" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="woocommerce-smallscreen-css" href="wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen91ac.css?ver=2.6.8" type="text/css" media="only screen and (max-width: 768px)">
        <link property="stylesheet" rel="stylesheet" id="woocommerce-general-css" href="wp-content/plugins/woocommerce/assets/css/woocommerce91ac.css?ver=2.6.8" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="wsl-widget-css" href="wp-content/plugins/wordpress-social-login/assets/css/style4698.css?ver=4.6.3" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-font-google_fonts-style-css" href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,300italic,400,400italic,700,700italic&amp;subset=latin,latin-ext" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-fontello-style-css" href="wp-content/themes/gowash/css/fontello/css/fontello.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-main-style-css" href="wp-content/themes/gowash/style.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-animation-style-css" href="wp-content/themes/gowash/fw/css/core.animation.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-shortcodes-style-css" href="wp-content/plugins/trx_utils/shortcodes/theme.shortcodes.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-theme-style-css" href="wp-content/themes/gowash/css/theme.css" type="text/css" media="all">
        <style id="gowash-theme-style-inline-css" type="text/css">.sidebar_outer_logo .logo_main,.top_panel_wrap .logo_main,.top_panel_wrap .logo_fixed,.widget_area .widget_socials .logo img{height:62px}</style>
        <link property="stylesheet" rel="stylesheet" id="gowash-plugin.booked-style-css" href="wp-content/themes/gowash/css/plugin.booked.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="gowash-responsive-style-css" href="wp-content/themes/gowash/css/responsive.css" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="mediaelement-css" href="wp-includes/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="wp-mediaelement-css" href="wp-includes/js/mediaelement/wp-mediaelement.min4698.css?ver=4.6.3" type="text/css" media="all">
        <link property="stylesheet" rel="stylesheet" id="js_composer_front-css" href="wp-content/plugins/js_composer/assets/css/js_composer.min972f.css?ver=5.0.1" type="text/css" media="all">
        <script type="text/javascript" src="wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>
        <script type="text/javascript" src="wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1"></script>
 <!--       <script type="text/javascript" src="../use.fontawesome.com/fcc8474e794698.js?ver=4.6.3"></script> -->
        <script type="text/javascript" src="wp-content/plugins/essential-grid/public/assets/js/lightbox5bfb.js?ver=2.0.9.1"></script>
        <script type="text/javascript" src="wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min5bfb.js?ver=2.0.9.1"></script>
        <script type="text/javascript" src="//gowash.ancorathemes.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
 <!--       <script type="text/javascript" src="//gowash.ancorathemes.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="//gowash.ancorathemes.com/wp-content/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js"></script>-->
        <script type="text/javascript" src="wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.essential.min5bfb.js?ver=2.0.9.1"></script>
        <script type="text/javascript" src="wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.mina88c.js?ver=5.3.0.2"></script>
       <!-- <script type="text/javascript">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php", "wc_ajax_url":"\/?wc-ajax=%%endpoint%%", "i18n_view_cart":"View Cart", "cart_url":"http:\/\/gowash.ancorathemes.com\/cart\/", "is_cart":"", "cart_redirect_after_add":"no"};
            /* ]]> */
        </script>-->
        <script type="text/javascript" src="wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min91ac.js?ver=2.6.8"></script>
        <script type="text/javascript" src="wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart972f.js?ver=5.0.1"></script>
        <script type="text/javascript" src="wp-content/themes/gowash/fw/js/photostack/modernizr.min.js"></script>
        <!--<link rel='https://api.w.org/' href='wp-json/index.html'/>-->
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.html?rsd">
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml">
        <meta name="generator" content="WordPress 4.6.3">
        <meta name="generator" content="WooCommerce 2.6.8">
        <link rel="canonical" href="index.html">
        <link rel="shortlink" href="index.html">
        <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed3b85.json?url=http%3A%2F%2Fgowash.ancorathemes.com%2F">
        <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed3f04?url=http%3A%2F%2Fgowash.ancorathemes.com%2F&amp;format=xml">
        <script type="text/javascript">
var ajaxRevslider;
jQuery(document).ready(function() {
// CUSTOM AJAX CONTENT LOADING FUNCTION
ajaxRevslider = function(obj) {
var content = "";
data = {};
data.action = 'revslider_ajax_call_front';
data.client_action = 'get_slider_html';
data.token = 'c3809315aa';
data.type = obj.type;
data.id = obj.id;
data.aspectratio = obj.aspectratio;
// SYNC AJAX REQUEST
jQuery.ajax({
type:"post",
        url:"http://gowash.ancorathemes.com/wp-admin/admin-ajax.php",
        dataType: 'json',
        data:data,
        async:false,
        success: function(ret, textStatus, XMLHttpRequest) {
        if (ret.success == true)
                content = ret.data;
        },
        error: function(e) {
        console.log(e);
        }
});
// FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
return content;
};
// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
var ajaxRemoveRevslider = function(obj) {
return jQuery(obj.selector + " .rev_slider").revkill();
};
// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
var extendessential = setInterval(function() {
if (jQuery.fn.tpessential != undefined) {
clearInterval(extendessential);
if (typeof (jQuery.fn.tpessential.defaults) !== 'undefined') {
jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider", func:ajaxRevslider, killfunc:ajaxRemoveRevslider, openAnimationSpeed:0.3});
// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
// func: the Function Name which is Called once the Item with the Post Type has been clicked
// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
}
}
}, 30);
});
        </script>
        <style type="text/css">.recentcomments a{display:inline!important;padding:0!important;margin:0!important;}</style>
        <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
        <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://gowash.ancorathemes.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.3.0.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
        <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1467887772524{padding-top:3rem!important;padding-bottom:3rem!important;background-position:center!important;background-repeat:no-repeat!important;background-size:cover!important;}.vc_custom_1467891069196{margin-top:4rem!important;}.vc_custom_1467892034495{height:12cm;margin-top:4rem!important;background-image:url(wp-content/uploads/2016/06/bg1ed21.jpg?id=203)!important;background-position:center!important;background-repeat:no-repeat!important;background-size:cover!important;}.vc_custom_1467891297135{margin-top:4rem!important;}.vc_custom_1467891382925{margin-top:6.5rem!important;}.vc_custom_1480334683565{padding-top:13.7rem!important;padding-bottom:12rem!important;background-image:url(http://gowash.ancorathemes.com/wp-content/uploads/2016/06/bg2.jpg?id=235)!important;background-position:center!important;background-repeat:no-repeat!important;background-size:cover!important;}.vc_custom_1467891437670{margin-top:2rem!important;}.vc_custom_1467724813963{margin-top:4rem!important;margin-bottom:4rem!important;}.vc_custom_1467819802511{background-image:url(http://gowash.ancorathemes.com/wp-content/uploads/2016/06/bg3.jpg?id=256)!important;background-position:center!important;height:12cm;background-repeat:no-repeat!important;background-size:cover!important;}.vc_custom_1467891509822{margin-top:8.5rem!important;}.vc_custom_1467888317203{margin-top:4rem!important;margin-bottom:8.5rem!important;border-top-width:1px!important;background-position:center!important;background-repeat:no-repeat!important;background-size:cover!important;border-top-color:#eaeaea!important;border-top-style:solid!important;}</style><noscript>&lt;style type="text/css"&gt;.wpb_animate_when_almost_visible{opacity:1;}&lt;/style&gt;</noscript><style type="text/css"></style><style type="text/css">.esgbox-margin{margin-right:0px;}</style>
        <!--<script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/common.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/map.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/util.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/geocoder.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/onion.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/marker.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/controls.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/28/5/stats.js"></script>
        -->
    </head>


    <body>
        @include('layouts.header') 
        @yield('content') 

        <script type="text/javascript">(function() {function addEventListener(element, event, handler) {
            if (element.addEventListener) {
            element.addEventListener(event, handler, false);
            } else if (element.attachEvent){
            element.attachEvent('on' + event, handler);
            }
            }function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
            this.value = "http://" + this.value;
            }
            }

            var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
            if (urlFields && urlFields.length > 0) {
            for (var j = 0; j < urlFields.length; j++) {
            addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
            }
            }/* test if browser supports date fields */
            var testInput = document.createElement('input');
            testInput.setAttribute('type', 'date');
            if (testInput.type !== 'date') {

            /* add placeholder & pattern to all date fields */
            var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
            for (var i = 0; i < dateFields.length; i++) {
            if (!dateFields[i].placeholder) {
            dateFields[i].placeholder = 'YYYY-MM-DD';
            }
            if (!dateFields[i].pattern) {
            dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
            }
            }
            }

            })();</script> <script type="text/javascript">
                function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
                }
        </script>
        <link property="stylesheet" rel='stylesheet' id='gowash-messages-style-css' href='wp-content/themes/gowash/fw/js/core.messages/core.messages.css' type='text/css' media='all'/>
        <link property="stylesheet" rel='stylesheet' id='gowash-magnific-style-css' href='wp-content/themes/gowash/fw/js/magnific/magnific-popup.css' type='text/css' media='all'/>
        <link property="stylesheet" rel='stylesheet' id='gowash-swiperslider-style-css' href='wp-content/themes/gowash/fw/js/swiper/swiper.css' type='text/css' media='all'/>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
        <script type='text/javascript' src='wp-includes/js/jquery/ui/datepicker.mine899.js?ver=1.11.4'></script>
        <script type='text/javascript'>
                jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close", "currentText":"Today", "monthNames":["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], "monthNamesShort":["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], "nextText":"Next", "prevText":"Previous", "dayNames":["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], "dayNamesShort":["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], "dayNamesMin":["S", "M", "T", "W", "T", "F", "S"], "dateFormat":"MM d, yy", "firstDay":1, "isRTL":false}); });
        </script>
        <script type='text/javascript' src='wp-content/plugins/booked/js/spin.min7406.js?ver=2.0.1'></script>
        <script type='text/javascript' src='wp-content/plugins/booked/js/spin.jquery7406.js?ver=2.0.1'></script>
        <script type='text/javascript' src='wp-content/plugins/booked/js/tooltipster/js/jquery.tooltipster.min9b70.js?ver=3.3.0'></script>
        <script type='text/javascript'>
                /* <![CDATA[ */
                var booked_js_vars = {"ajax_url":"http:\/\/gowash.ancorathemes.com\/wp-admin\/admin-ajax.php", "profilePage":"", "publicAppointments":"", "i18n_confirm_appt_delete":"Are you sure you want to cancel this appointment?", "i18n_please_wait":"Please wait ...", "i18n_wrong_username_pass":"Wrong username\/password combination.", "i18n_fill_out_required_fields":"Please fill out all required fields.", "i18n_guest_appt_required_fields":"Please enter your name to book an appointment.", "i18n_appt_required_fields":"Please enter your name, your email address and choose a password to book an appointment.", "i18n_appt_required_fields_guest":"Please fill in all \"Information\" fields.", "i18n_password_reset":"Please check your email for instructions on resetting your password.", "i18n_password_reset_error":"That username or email is not recognized."};
                /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/booked/js/functions055b.js?ver=1.9.6'></script>
        <script type='text/javascript'>
                /* <![CDATA[ */
                var TRX_UTILS_STORAGE = {"ajax_url":"http:\/\/gowash.ancorathemes.com\/wp-admin\/admin-ajax.php", "ajax_nonce":"43c2ea2227", "site_url":"http:\/\/gowash.ancorathemes.com", "user_logged_in":"0", "email_mask":"^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$", "msg_ajax_error":"Invalid server answer!", "msg_error_global":"Invalid field's value!", "msg_name_empty":"The name can't be empty", "msg_email_empty":"Too short (or empty) email address", "msg_email_not_valid":"E-mail address is invalid", "msg_text_empty":"The message text can't be empty", "msg_send_complete":"Send message complete!", "msg_send_error":"Transmit failed!", "login_via_ajax":"1", "msg_login_empty":"The Login field can't be empty", "msg_login_long":"The Login field is too long", "msg_password_empty":"The password can't be empty and shorter then 4 characters", "msg_password_long":"The password is too long", "msg_login_success":"Login success! The page will be reloaded in 3 sec.", "msg_login_error":"Login failed!", "msg_not_agree":"Please, read and check 'Terms and Conditions'", "msg_email_long":"E-mail address is too long", "msg_password_not_equal":"The passwords in both fields are not equal", "msg_registration_success":"Registration success! Please log in!", "msg_registration_error":"Registration failed!"};
                /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/plugins/trx_utils/js/trx_utils.js'></script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
       <!-- <script type='text/javascript'>
                        /* <![CDATA[ */
                        var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php", "wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
                        /* ]]> */
        </script>-->
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min91ac.js?ver=2.6.8'></script>
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min330a.js?ver=1.4.1'></script>
       <!-- <script type='text/javascript'>
                        /* <![CDATA[ */
                        var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php", "wc_ajax_url":"\/?wc-ajax=%%endpoint%%", "fragment_name":"wc_fragments"};
                        /* ]]> */
        </script>-->
        <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min91ac.js?ver=2.6.8'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/core.utils.js'></script>
        <script type='text/javascript'>
                /* <![CDATA[ */
                var GOWASH_STORAGE = {"system_message":{"message":"", "status":"", "header":""}, "theme_font":"Roboto Slab", "theme_color":"#414141", "theme_bg_color":"#ffffff", "strings":{"ajax_error":"Invalid server answer", "bookmark_add":"Add the bookmark", "bookmark_added":"Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab &#039;Bookmarks&#039;", "bookmark_del":"Delete this bookmark", "bookmark_title":"Enter bookmark title", "bookmark_exists":"Current page already exists in the bookmarks list", "search_error":"Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.", "email_confirm":"On the e-mail address &quot;%s&quot; we sent a confirmation email. Please, open it and click on the link.", "reviews_vote":"Thanks for your vote! New average rating is:", "reviews_error":"Error saving your vote! Please, try again later.", "error_like":"Error saving your like! Please, try again later.", "error_global":"Global error text", "name_empty":"The name can&#039;t be empty", "name_long":"Too long name", "email_empty":"Too short (or empty) email address", "email_long":"Too long email address", "email_not_valid":"Invalid email address", "subject_empty":"The subject can&#039;t be empty", "subject_long":"Too long subject", "text_empty":"The message text can&#039;t be empty", "text_long":"Too long message text", "send_complete":"Send message complete!", "send_error":"Transmit failed!", "geocode_error":"Geocode was not successful for the following reason:", "googlemap_not_avail":"Google map API not available!", "editor_save_success":"Post content saved!", "editor_save_error":"Error saving post data!", "editor_delete_post":"You really want to delete the current post?", "editor_delete_post_header":"Delete post", "editor_delete_success":"Post deleted!", "editor_delete_error":"Error deleting post!", "editor_caption_cancel":"Cancel", "editor_caption_close":"Close"}, "ajax_url":"http:\/\/gowash.ancorathemes.com\/wp-admin\/admin-ajax.php", "ajax_nonce":"43c2ea2227", "site_url":"http:\/\/gowash.ancorathemes.com", "site_protocol":"http", "vc_edit_mode":"", "accent1_color":"#17c8ca", "accent1_hover":"#20d9c9", "slider_height":"100", "user_logged_in":"", "toc_menu":"float", "toc_menu_home":"1", "toc_menu_top":"1", "menu_fixed":"1", "menu_mobile":"1024", "menu_hover":"fade", "button_hover":"default", "input_hover":"default", "demo_time":"0", "media_elements_enabled":"1", "ajax_search_enabled":"1", "ajax_search_min_length":"3", "ajax_search_delay":"200", "css_animation":"1", "menu_animation_in":"bounceIn", "menu_animation_out":"fadeOutDown", "popup_engine":"magnific", "email_mask":"^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$", "contacts_maxlength":"1000", "comments_maxlength":"1000", "remember_visitors_settings":"", "admin_mode":"", "isotope_resize_delta":"0.3", "error_message_box":null, "viewmore_busy":"", "video_resize_inited":"", "top_panel_height":"0"};
                /* ]]> */
        </script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/core.init.js'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/js/theme.init.js'></script>
        <script type='text/javascript'>
                /* <![CDATA[ */
                var mejsL10n = {"language":"en-US", "strings":{"Close":"Close", "Fullscreen":"Fullscreen", "Turn off Fullscreen":"Turn off Fullscreen", "Go Fullscreen":"Go Fullscreen", "Download File":"Download File", "Download Video":"Download Video", "Play":"Play", "Pause":"Pause", "Captions\/Subtitles":"Captions\/Subtitles", "None":"None", "Time Slider":"Time Slider", "Skip back %1 seconds":"Skip back %1 seconds", "Video Player":"Video Player", "Audio Player":"Audio Player", "Volume Slider":"Volume Slider", "Mute Toggle":"Mute Toggle", "Unmute":"Unmute", "Mute":"Mute", "Use Up\/Down Arrow keys to increase or decrease volume.":"Use Up\/Down Arrow keys to increase or decrease volume.", "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
                var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
                /* ]]> */
        </script>
        <script type='text/javascript' src='wp-includes/js/mediaelement/mediaelement-and-player.min51cd.js?ver=2.22.0'></script>
        <script type='text/javascript' src='wp-includes/js/mediaelement/wp-mediaelement.min4698.js?ver=4.6.3'></script>
        <script type='text/javascript' src='http://maps.google.com/maps/api/js?key=AIzaSyAMd1u2BxTzMh3CCXxBZJoSmFlqQulrsk4'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/core.googlemap.js'></script>
        <script type='text/javascript' src='wp-includes/js/wp-embed.min4698.js?ver=4.6.3'></script>
        <script type='text/javascript' src='wp-content/plugins/trx_utils/shortcodes/theme.shortcodes.js'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/core.messages/core.messages.js'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/magnific/jquery.magnific-popup.min4698.js?ver=4.6.3'></script>
        <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min972f.js?ver=5.0.1'></script>
        <script type='text/javascript' src='wp-content/themes/gowash/fw/js/swiper/swiper.js'></script>

        @yield('js')
        <script type="text/javascript">
                jQuery(document).ready(function($) {
                $('a.side-wrap-menu-link').on('click', function(){
                $('#side-wrap-menu').removeClass('open');
                });
                $("#login-form").submit(function(e){
                e.preventDefault();
                $.ajax({
                type:"POST",
                        url:$(this).attr('action'),
                        data:$(this).serialize(),
                        dataType: 'json',
                        success: function(data){
                        console.log(data);
                        //window.location.href = "{{URL::to('/my-fake-page')}}"
                        window.location.reload();
                        },
                        error: function(data){

                        }
                });
                });
                });
        </script>
    </body>
</html>