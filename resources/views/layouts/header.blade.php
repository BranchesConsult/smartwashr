<?php
$code = country();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Smartwashr - @yield('title')</title>
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <!-- Bootstrap Core CSS -->

        <!-- Custom Fonts -->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700"> 
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <!-- Plugin CSS -->
        {!! Html::style('js/datetime/jquery.datetimepicker.min.css') !!}
        {!! Html::style('vendor/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('vendor/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('css/jquery.toast.css') !!}
        {!! Html::style('css/jquery.toast.min.css') !!}
        {!! Html::style('css/fontello/css/fontello.css') !!}
        {!! Html::style('vendor/magnific-popup/magnific-popup.css') !!}
        <!-- Theme CSS -->
        {!! Html::style('css/camera.css') !!}
        {!! Html::style('css/slicknav.css') !!}
        {!! Html::style('css/creative.css') !!}
        {!! Html::style('css/style.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top">

        <nav class="bg-grey navbar navbar-default navbar-fixed-top">
            <!-------------->
            <div class="container-fluid menu-top1-1">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-top-1">
                            <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand font-normal page-scroll">
                            {{ __('home.24/7') }}
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-top-1">
                        <ul class="nav navbar-nav navbar-right main-menu">
                            <li class="menu_user_login" style="padding-top: 0.38cm;color:#48db44;font-weight: bold;">
                                <?php
                                switch ($code) {
                                    case 'PK':
                                        echo '+92 321 8450305';
                                        break;
                                    case 'USA':
                                        echo '+1 (313) 649-1173';
                                        break;
                                    default:
                                        echo '+96 654 7771807';
                                }
                                ?>
                            </li>
                            @if(!Auth::check())
                            <li class="menu_user_login"><a href="{{ url('/login') }}" class="popup_link popup_login_link icon-user inited" title="">{{ __('home.login') }}</a>
                            <li class="menu_user_login"><a href="{{ url('/register') }}" class="popup_link popup_login_link icon-user inited" title="">{{ __('home.register') }}</a>
                            </li>
                            @else         
                            <div class="dropdown" style='float:left;padding-top: 0.38cm; padding-left: 0.5cm;z-index:9999;'>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                                   aria-expanded="true" style='font-weight: bold;text-decoration: none;'>ACCOUNT<span class="caret"></span></a>
                                <ul class="dropdown-menu" style="padding-right:0;">
                                    <li class="menu_user_login"><a href="{{ route('account') }}">Profile</a></li>
                                    <li class="menu_user_login"><a href="{{route('order')}}">Orders</a></li>
                                </ul>
                            </div>
                            <li class="menu_user_login"><a href="/logout" style='font-weight: bold;' class="popup_link popup_login_link icon-user inited" 
                                                           title="">Logout</a></li>
                            @endif
                            
                            <li class="cart-icon" style='display:none;'>
                                <a href="{{route('getUserInfo')}}" class="">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="count-bubble" id="main-cart-count">
                                        0
                                    </span>
                                </a>
                            </li>
                            <div  style='float:left;padding-top: 0.35cm; padding-left: 0.5cm;display: none;'>
                                <li class="menu_user_login">
                                    {!! Form::open(['url' => route('changeLanguage'), 'id'=>'langChangeFrm']) !!}
                                    <a href="#" onclick="document.getElementById('langChangeFrm').submit()" 
                                       class="popup_link popup_login_link  inited">
                                           <?php echo (\Session::get('language') == 'en') ? 'العربیہ' : 'English' ?>
                                    </a>
                                    {!!Form::close()!!}
                                </li>
                            </div>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </div>
            <!-------------->
            <div class="container-fluid menu-top-2 bg-white">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header relative" style="z-index: 1031">
                        <a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
                            <img src="{{Url::to('img/logo.png')}}" />
                        </a>
                        <div class="clear clearfix"></div>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="top-menu-res" class="jquerySlickNavContainer"></div>
                    <div class="collapse navbar-fixed-top navbar-collapse logo-menu-container">
                        <ul class="nav navbar-nav navbar-right nav-main-menu" id="bs-example-navbar-collapse-1">
                            @if(isset($menupages))
                            @foreach($menupages as $item)
                            <li>
                                @if($item['page']['slug']!='about')
                                <a class="page-scroll" href="/#{{$item['page']['slug']}}">
                                   @else
                                <a class="page-scroll" href="/{{$item['page']['slug']}}">
                                @endif
                                    <span><?php
                                        echo \Session::get('language') == 'ar' ?
                                                $item['title_ar'] : $item['title'];
                                        ?>
                                    </span>
                                </a>
                                
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <div class="clear clearfix"></div>
        @yield('content')
        
        <input type="hidden" id="users_country_code" value="<?php echo $code ?>" />
        <!-- jQuery -->
        {!! Html::script('vendor/jquery/jquery.min.js') !!}
        {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('js/jquery.slicknav.min.js') !!}
        {!! Html::script('js/creative.js') !!}
        {!! Html::script('js/jquery.easing.1.3.min.js') !!}
        <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#bs-example-navbar-collapse-1').slicknav({
                                                appendTo: '#top-menu-res',
                                                label: 'Main Menu'
                                            });
                                        });
        </script>
        @yield('js')
        @include('layouts.common_cart')
        @include('layouts.errors')
        {!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyAMd1u2BxTzMh3CCXxBZJoSmFlqQulrsk4&callback=initMap&libraries=geometry,places&ext=.js',['async', 'defer']) !!}
    </body>
</html>
