<?php
$code = country();
$menupages = \App\Menu::with('page')
    ->where('is_active', '1')
    ->get()
    ->toArray();
if (\Auth::check()) {
    $userId = \Auth::id();
    $userType = \App\User_Type::where('user_id', $userId)->first();
    $userType = $userType['type_id'];
}
?>
        <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Smartwashr - @yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <!-- Bootstrap Core CSS -->

    <!-- Custom Fonts -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
{!! Html::style('js/datetime/jquery.datetimepicker.min.css') !!}
{!! Html::style('vendor/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('vendor/bootstrap/css/bootstrap.min.css') !!}
{!! Html::style('css/jquery.toast.css') !!}
{!! Html::style('css/jquery.toast.min.css') !!}
{!! Html::style('css/fontello/css/fontello.css') !!}
{!! Html::style('vendor/magnific-popup/magnific-popup.css') !!}
<!-- Theme CSS -->
{!! Html::style('css/camera.css') !!}
{!! Html::style('css/slicknav.css') !!}
{!! Html::style('css/creative.css') !!}
{!! Html::style('css/style.css') !!}
@yield('css')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">
<?php
$is_laundry = \App\Http\Controllers\PageController::is_laundry();
?>
<nav class="bg-grey navbar navbar-default navbar-fixed-top">
    <div class="fixed_area">
        <!-------------->
        <div class="container-fluid menu-top1-1">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div style="display: none;" class="navbar-header">
                    <button style="display: none;" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-top-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand font-normal page-scroll">
                        {{ __('home.24/7') }}

                    </a>
                    <span class="padding_area"
                          style="padding-top: 15px;color:#48db44;font-weight: bold;display: inline-block;">
                            <?php
                        switch ($code) {
                            case 'PK':
                                echo '+92 321 8450305';
                                break;
                            case 'USA':
                                echo '+1 (313) 649-1173';
                                break;
                            default:
                                echo '+966 54 777 1807';
                        }
                        ?>
                        </span>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <!--                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-top-1">-->
                <ul style="display: none;" class="nav navbar-nav navbar-right main-menu">
                    @if(!Auth::check())
                        <li class="menu_user_login">
                            <a href="{{ url('/login') }}"
                               class="popup_link popup_login_link icon-user inited"
                               style="font-weight:bold;text-decoration: none;font-size: 14px; "
                               title="">{{ __('home.login') }}
                            </a>
                        <li class="menu_user_login">
                            <a href="{{ url('/register') }}"
                               class="popup_link popup_login_link icon-user inited"
                               style="font-weight:bold;text-decoration: none;font-size: 14px; "
                               title="">{{ __('home.register') }}
                            </a>
                        </li>
                    @else
                        <div class="dropdown"
                             style='float:left;padding-top: 0.38cm; padding-left: 0.5cm;z-index:9999;'>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="true" style='font-weight: bold;text-decoration: none;'>ACCOUNT<span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu" style="padding-right:0;">
                                <li class="menu_user_login login_left">
                                    <a href="{{ route('account') }}">
                                        {!! ($userType == 2)?'Driver':'' !!} Profile
                                    </a>
                                </li>
                                <li class="menu_user_login register_right"><a href="{{route('order')}}">My Orders</a>
                                </li>
                                @if(\Auth::check())
                                    @if(($is_laundry)||(\Auth::user()->is_admin==1))
                                        <li class="menu_user_login"><a href="{{route('viewUsers')}}">Dashboard</a>
                                        </li>
                                    @endif
                                @endif
                                <li class="menu_user_login"><a href="/logout">LogOut</a></li>
                            </ul>
                        </div>
                    @endif
                    <li class="menu_user_login">
                        <a href="/#pricing" title=""><span
                                    style="font-weight: bold;text-decoration: none;font-size: 14px;">
                                    Order Now</span>
                        </a>
                    </li>
                    <li class="cart-icon">
                        <a href="{{route('getUserLocation')}}" class="">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="count-bubble" id="main-cart-count">
                                       {{ \Cart::getContent()->count() }}
                                </span>
                        </a>
                    </li>
                    <div style='float:left;padding-top: 0.35cm; padding-left: 0.5cm;display: none;'>
                        <li class="menu_user_login">
                            {!! Form::open(['url' => route('changeLanguage'), 'id'=>'langChangeFrm']) !!}
                            <a href="#" onclick="document.getElementById('langChangeFrm').submit()"
                               class="popup_link popup_login_link  inited">
                                <?php echo (\Session::get('language') == 'en') ? 'العربیہ' : 'English' ?>
                            </a>
                            {!!Form::close()!!}
                        </li>
                    </div>
                </ul>
                <!--                </div>-->
                <!-- /.navbar-collapse -->
            </div>
        </div>
        <!-------------->
        <div class="container-fluid menu-top-2 bg-white">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header relative header_same_line" style="z-index: 1031">
                    <a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
                        <img src="{{Url::to('img/logo.png')}}"/>
                    </a>
                    <div class="clear clearfix"></div>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="width_area">
                    <div id="top-menu-res" class="jquerySlickNavContainer"></div>
                </div>
                <div class="collapse navbar-fixed-top navbar-collapse logo-menu-container">
                    <ul class="nav navbar-nav navbar-right nav-main-menu" id="bs-example-navbar-collapse-1">
                        @if(isset($menupages))
                            @foreach($menupages as $item)
                                <li>
                                    @if($item['page']['slug']!='about')
                                        <a class="page-scroll" href="/#{{$item['page']['slug']}}">
                                            @else
                                                <a class="page-scroll" href="/{{$item['page']['slug']}}">
                                                    @endif
                                                    <span><?php
                                                        echo \Session::get('language') == 'ar' ?
                                                            $item['title_ar'] : $item['title'];
                                                        ?>
                                    </span>
                                                </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
</nav>
<div class="clear clearfix"></div>
@yield('content')
<footer>
    <section class="bg-grey footer">
        <div class="container">
            <div class="row fontstyle" id="final-footer">
                <div class="col-sm-12 col-md-6 text-left">
                    {{ __('home.copyright') }} {{date('Y')}}.{{ __('home.rights') }}
                    <a href="{{route('page',['slug'=>'terms-and-conditions'])}}">{{ __('home.terms') }}</a>
                    <span style="color: #48db44">&</span>
                    <a href="{{route('page',['slug'=>'privacy-policy'])}}">{{ __('home.policy') }}</a>
                </div>
            </div>
        </div>
    </section>
</footer>
<input type="hidden" id="users_country_code" value="<?php echo $code ?>"/>
<!-- jQuery -->
<script>
    <?php echo file_get_contents(Url::to('vendor/jquery/jquery.min.js')) ?>
</script>
{!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js') !!}
{!! Html::script('js/jquery.slicknav.min.js') !!}
{!! Html::script('js/creative.js') !!}
{!! Html::script('js/jquery.easing.1.3.min.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        $('#bs-example-navbar-collapse-1').slicknav({
            appendTo: '#top-menu-res',
            label: 'Main Menu'
        });
    });
</script>
@yield('js')
@include('layouts.common_cart')
@include('layouts.errors')
{!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyAMd1u2BxTzMh3CCXxBZJoSmFlqQulrsk4&callback=initMap&libraries=geometry,places&ext=.js',['async', 'defer']) !!}
</body>
</html>