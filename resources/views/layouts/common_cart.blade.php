<script>
    $(document).ready(function () {
        $.get('{{route('countCartContents')}}', function (res) {
            $("#main-cart-count").html(res.total_items);
        });
    });
    function cartAddItem(itemId, pName) {
        var qty = $("#qty-" + itemId).val();
        if (qty < 1 || typeof qty == 'undefined' || isNaN(qty)) {
            alert('Please select quantity atleast 1');
            return;
        }
                @if(\Auth::check() && \Auth::user()->active == 1)
        var selectedProduct = $(".product-" + itemId + " input[name='service_price']:checked").val();
        if (typeof selectedProduct == 'undefined') {
            bootbox.alert('Please select one service');
            return;
        }
        var serviceSelected = selectedProduct.split('__')[0];
        var productPrice = selectedProduct.split('__')[1];
        $.ajax({
            url: "{{route('addItemToCart')}}",
            type: "POST",
            async: true,
            dataType: 'json',
            data: {
                item_id: itemId,
                item_name: pName,
                item_price: productPrice,
                service_selected: serviceSelected,
                qty: qty
            },
            success: function (res) {
                //console.log(res.total_items, 'sdsd');
                $("#main-cart-count").html(res.total_items);
                $.toast({
                    heading: 'Success',
                    text: 'Product added to cart',
                    icon: 'success',
                    loader: true, // Change it to false to disable loader
                    position: 'top-right',
                    showHideTransition: 'slide',
                    hideAfter: 5000,
                    allowToastClose: true,
                    loaderBg: '#DB6C0F'  // To change the background
                });
            }
        });
        @else
            alert('Please verify your email before placing order');
        @endif
    }
    function wantOrder(productId) {
        @if(\Auth::check())
$(".pricingSelect").hide();
        var productRow = $('.' + productId);
        productRow.slideDown('slow');
        @else
        bootbox.alert("Please login before placing order!");
        @endif
    }
    function updateQty(productId, rowClicked, unitPrice) {
        var qty = $("#" + rowClicked + " .pro_qty").val();
        $("#checkoutBtn").attr('disabled', true);
        $.ajax({
            url: "{{route('updateQty')}}",
            type: "GET",
            async: false,
            dataType: 'json',
            data: {
                item_id: productId,
                quantity: qty
            },
            success: function (res) {
                //console.log(res.total_items, 'sdsd');  
                $("#" + rowClicked + " .totalPrice").html(unitPrice * parseInt(res.quantity));
//                $.toast({
//                    heading: 'Success',
//                    text: 'Quantity updated.',
//                    icon: 'success',
//                    loader: true, // Change it to false to disable loader
//                    position: 'top-right',
//                    showHideTransition: 'slide',
//                    hideAfter: 5000,
//                    allowToastClose: true,
//                    loaderBg: '#DB6C0F'  // To change the background
//                });
                $("#cart-total-price").html(parseInt(res.total) + 15);
                $("#subTotalCart").html(parseInt(res.total));
                $("#checkoutBtn").attr('disabled', false);
            },
            error: function (xhr, status, errorThrown) {
                //Here the status code can be retrieved like;
                consoel.error(xhr.status, xhr.responseText);
                //The message added to Response object in Controller can be retrieved as following.
                $("#checkoutBtn").attr('disabled', false);
            }
        });
    }
    function deleteCartProduct(productId) {
        bootbox.confirm({
            title: "Destroy planet?",
            message: "Do you want to activate the Deathstar now? This cannot be undone.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    var row = $("#product-" + productId);
                    $.ajax({
                        url: "{{route('deleteCartProduct')}}",
                        type: "GET",
                        async: false,
                        dataType: 'json',
                        data: {
                            item_id: productId
                        },
                        success: function (res) {
                            row.css({
                                'background': '#ff0000'
                            });
                            //console.log(res.total_items, 'sdsd');

                            $.toast({
                                heading: 'Success',
                                text: 'Quantity updated.',
                                icon: 'success',
                                loader: true, // Change it to false to disable loader
                                position: 'top-right',
                                showHideTransition: 'slide',
                                hideAfter: 5000,
                                allowToastClose: true,
                                loaderBg: '#DB6C0F'  // To change the background
                            });
                            row.fadeOut('slow', function () {
                                row.remove();
                                window.location.reload();
                            });
                        }
                    });
                }
            }
        });
    }
</script>