@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('css')
    <style>
        #map {
            width: 100%;
            height: 100%;
        }
    </style>
@stop
@section('main-content')
    <div class="box box-primary" style="width: 100%; height: 600px;">
        {{--<div id="map"></div>--}}
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th>
                        User email
                    </th>
                    <th>
                        User phone
                    </th>
                    <th>
                        Lat, Long
                    </th>
                    <th>
                        Address
                    </th>
                    <th>
                        Voted at
                    </th>
                </tr>
                @foreach($addresses as $key => $val)
                    <tr>
                        <td>
                            {!! $val->user['email'] !!}
                        </td>
                        <td>
                            {!! $val->user['email'] !!}
                        </td>
                        <td>
                            <a target="_blank"
                               href="https://www.google.com/maps/search/?api=1&query={!! $val->lat.','.$val->lng !!}">
                                See on map
                            </a>
                        </td>
                        <td>
                            {!! $val->address !!}
                        </td>
                        <td>
                            {!! date('d/m/Y, H:i', strtotime($val->created_at)) !!}
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $addresses->links() !!}
        </div>
    </div>
@stop

@section('scripts')
    {{--<script type="text/javascript"--}}
    {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAipeohHOXpRFFRqF1QuocDhzQy4jdijYE&libraries=visualization">--}}
    {{--</script>--}}
@stop