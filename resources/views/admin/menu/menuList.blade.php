@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Menus</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Parent_id</th>
                  <th>Page_id</th>
                  <th>Is_Active</th>
                  <th>Action</th>
                </tr>
                @foreach ($menu as $item) 
                <tr>
                  <td>{{$item['id']}}</td>
                  <td style="text-transform: capitalize;">{{$item['title']}}</td>
                  <td>{{$item['parent_id']}}</td>
                  <td>{{$item['page_id']}}</td>
                  <td>{{$item['is_active']}}</td>
                <td><a class='btn btn-success' href="{{route('editMenu',['id'=>$item['id']])}}">Edit</a>
                      <a class='btn btn-success' onclick="return deleteEntity()" href="{{route('deleteMenu',['id'=>$item['id']])}}"> Delete</a></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         </div>

@stop