@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('title')||$errors->has('title_ar')||$errors->has('slug')||$errors->has('content'))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAddMenu')]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Title</label>
                    <input value="{!! old('title') !!}" class="form-control" id="title" placeholder="Enter title" type="text" name="title">
                </div>
                <div class="form-group">
                    <label>Arabic Title</label>
                    <input value="{!! old('title_ar') !!}" class="form-control" id="title_ar" placeholder="Enter title in arabic" type="text" name="title_ar">
                </div>
                <div class="form-group" >
                    <div claass="col-md-6">
                        <label>Page</label>
                        <select name="page_id" class="form-control" required>
                            <option value="">Select Page</option>
                            @foreach ($page as $item)
                            <option value={{$item['id']}}>{{$item['title']}}</option>
                            @endforeach
                        </select>   
                        <span class="text-danger">{!! $errors->first('message') !!}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label>Is_Active</label>
                    <select name="is_active" class="form-control" required>
                        <option value="">Select Is_active</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="text-danger">{!! $errors->first('message') !!}</span>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            {!! Form::open() !!}
        </div>
        @stop

       