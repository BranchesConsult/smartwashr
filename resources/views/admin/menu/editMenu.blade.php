@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Menu</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {!! Form::open(['url' => route('postEditMenu',['id'=>$menuid['id']])]) !!}
    <div class="box-body">
        <div class="form-group">
            <label>Title</label>
            {{ Form::text('title', $menuid['title'],['class' => 'form-control'])}}
        </div>
         <div class="form-group">
                    <label>Arabic Title</label>
                    <input value="{{$menuid['title_ar']}}" class="form-control" id="title_ar" placeholder="Enter title in arabic" type="text" name="title_ar">
                </div>
        
        <div class="form-group">
            <label>Page</label>
            <select name="page_id" class="form-control">
               
                @foreach ($page as $item)
                
                <option value="{{$item['id']}}" <?php echo ($menuid['page_id'] == $item['id']) ? 'selected' : '' ?>>{{$item['title']}}</option>
               
                @endforeach
            </select>       
        </div>      
        <div class="form-group">
            <label>Is_active</label>
            {{ Form::select('is_active', [
                '0' => 'No',
                '1' => 'Yes',
            ], $menuid['is_active']) }}  
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-success">Update</button>
    </div>
    {!! Form::open() !!}
</div>
@stop  