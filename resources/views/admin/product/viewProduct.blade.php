@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            @if(count($products)==0)
                <div class="alert alert-info">
                    Currently, you have no product.
                </div>
            @else
            <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Name (Arabic)</th>
                            <th>Category</th>
                            <th>Washing And Press Price(USD)</th>
                            <th>DryClean And Press Price(USD)</th>
                            <th>Press Price(USD)</th>
                            @if( Auth::user()->is_admin == 1)
                                <th>Action</th>
                            @endif
                        </tr>
                        @foreach($products as $item)
                            <tr>
                                <td>{{$item['name']}}</td>
                                <td>{{$item['name_ar']}}</td>
                                <td>
                                    @foreach($item['category'] as $key => $val)
                                        {!! $val !!}<br/>
                                    @endforeach
                                </td>
                                <td>{{$item['washing_price']}}</td>
                                <td>{{$item['dryclean_price']}}</td>
                                <td>{{$item['press']}}</td>
                                @if( Auth::user()->is_admin == 1)
                                    <td><a class='btn btn-success' href="{{route('editProduct',['id'=>$item['id']])}}">Edit</a>
                                        <a onclick="return deleteEntity()" class='btn btn-success'
                                           href="{{route('deleteProduct',['id'=>$item['id']])}}">Delete</a></td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div align='center'>
                        {{ $products->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
<script type="text/javascript">
    function searchUserType(userTypeId) {
        var searchParams = {
            user_type: userTypeId
        };
        return window.location.href = "{{route('viewUsers')}}?" + decodeURIComponent($.param(searchParams));
        // return window.location.href = "http://smartwasher.dev/admin/users?" + decodeURIComponent($.param(searchParams));
    }
</script>