
@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('name')||$errors->has('name_ar')||$errors->has('drycleanprice')||$errors->has('washingprice')
                ||$errors->has('category')||$errors->has('status')||$errors->has('laundry_id'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postEditProduct',['id'=>$product['id']]),'files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Product Name</label>
                    <input required value="{{$product['name']}}" class="form-control" id="name" placeholder="Enter Product Name" type="text" name="name">
                </div>
                <div class="form-group">
                    <label>Product Name in Arabic</label>
                    <input required value="{{$product['name_ar']}}" class="form-control" id="name_ar" placeholder="Enter Product Name in Arabic" type="text" name="name_ar">
                </div>
                <div class="form-group">
                    <label>Parent Category</label><br>
                    <?php echo ($categories->renderAsMultiple()); ?>
                </div>
                <div class="form-group col-md-12 no-padding">
                    <div class="input-append">
                        <label>Picture(Optional)</label>
                        <input placeholder="Attach your Picture" value="{{$product['picture']}}" class="form-control" id="pic" name="pic" type="text">
                        <a style="margin-top: 15px;"data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-success" type="button">
                            Select
                        </a>
                    </div>
                    <div class="modal fade" id="galleryModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Add Picture</h4>
                                </div>
                                <div class="modal-body">
                                    <iframe width="100%" height="800" 
                                            src="{{URL::to("filemanager/dialog.php?type=1&field_id=pic")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>


                    <div class="col-md-6">
                        @if(!empty($product['picture']))
                        <img class="img-thumbnail img-lg" src="{{$product['picture']}}" />
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label>SmartWashr Dry Clean And Press Price</label>
                    <input required value="{{$product['sw_dryclean_price']}}"  type="text" name="sw_drycleaningprice" id="sw_drycleaningprice" class='form-control' placeholder='Enter Dry Clean Price'/>
                </div>
                <div class="form-group">
                    <label>SmartWashr Washing And Press Price</label>
                    <input required value="{{$product['sw_washing_price']}}"  type="text" name="sw_washingprice" id="sw_washingprice" class='form-control' placeholder='Enter Washing Price'/>
                </div>
                <div class="form-group">
                    <label>SmartWashr Press Price</label>
                    <input required value="{{$product['sw_press']}}"  type="text" name="sw_pressprice" id="sw_pressprice" class='form-control' placeholder='Enter Press Price'/>
                </div>
                
                <div class="form-group">
                    <label>Dry Clean And Press Price</label>
                    <input required value="{{$product['dryclean_price']}}"  type="text" name="drycleaningprice" id="drycleaningprice" class='form-control' placeholder='Enter Dry Clean Price'/>
                </div>
                <div class="form-group">
                    <label>Washing And Press Price</label>
                    <input required value="{{$product['washing_price']}}"  type="text" name="washingprice" id="washingprice" class='form-control' placeholder='Enter Washing Price'/>
                </div>
                <div class="form-group">
                    <label>Press Price</label>
                    <input required value="{{$product['press']}}"  type="text" name="pressprice" id="pressprice" class='form-control' placeholder='Enter Press Price'/>
                </div>
                
                <div class="form-group">
                    <label>Status</label><br>
                    <select required name="status" id="status" class="form-control">
                        <option value="">Select Status</option>
                        <option <?php echo ($product['status'] == '1') ? 'selected' : '' ?> value="1">Active</option>
                        <option <?php echo ($product['status'] == '0') ? 'selected' : '' ?> value="0">In-Active</option>            
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection
@section('scripts')
<!--{!! Html::script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js') !!}
<script type="text/javascript">
    $(document).ready(function(){
        setTimeout(function(){
                        $("#add-client-form").validate();

        }, );

    })
</script>-->
@stop
