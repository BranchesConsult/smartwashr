@extends('adminlte::layouts.app')
@section('htmlheader_title')
Orders
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">       
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                    <?php
                    $count = 1;
                    ?>
                    <tr>
                        <th>S.No.</th>
                        <th>Price</th>
                        <th>Smarwashr Price</th>
                        <th>Product Name</th>
                        <th>Product Service</th>
                        <th>Commission</th>
                        <th>Quantity</th>
                    </tr>
                    <?php $price=0;$sw_price=0;$commission=0?>
                    @foreach($orderDetail as $item)
                    <tr>
                        <td>{{$count}}</td>
                        <td>{{$item['price']}}</td>
                        <td>{{$item['sw_price']}}</td>
                        <?php $sw_price+=$item['sw_price'];
                        $price+=$item['price'];?> 
                         @foreach($item['product'] as $row)
                        <td>{{$row['name']}}</td>
                    @endforeach
                        <td style='text-transform: capitalize;'>{{$item['product_service']}}</td>
                        <td>{{$item['commission']}}</td>
                            <?php $commission+=$item['commission']; ?>
                        <td>{{$item['quantity']}}</td>
                    </tr>     
                    
                    <?php
                    $count++;
                    ?>
                    @endforeach
                </tbody>
            </table>
            <div align='right' style="padding-right:20px;">
                <strong>
                    Sub-Total:SAR {{$price}}<br/>
                    Commission:SAR {{$commission}}<br/>
                    Total:SAR {{$sw_price}}
                </stron>
            </div>
            
            <div align='center'>
                {{ $orderDetail->links() }}
            </div>
        </div>
    </div>
</div>
@endsection