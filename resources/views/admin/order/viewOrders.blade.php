@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Orders
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">
            @if(session('message'))
                <div class="alert alert-success">
                    <h4>{{session('message')}}</h4>
                </div>
            @endif
            @if(count($order)==0)
                <div class="alert alert-success">
                    Currently, you have no orders.
                </div>
            @else
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <?php
                            $count = 1;
                            ?>
                            <th>S.No.</th>
                            <th>Order_ID</th>
                            <th>Laundry</th>
                            <th>Driver</th>
                            <th>User</th>
                            <th>Laundry Price</th>
                            <th>Smartwashr Price</th>
                            <th>Commission</th>
                            <th>Collection Time</th>
                            <th>Delivery Time</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach($order as $item)
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{str_pad($item['id'],6,"0",STR_PAD_LEFT)}}</td>
                                <td>{{$item['laundry']['name']}}</td>
                                <td>{{$item['driver']['email']}}</td>
                                <td>{{$item['user']['email']}}</td>
                                <td>{{$item['total_price']}}</td>
                                <td>{{$item['sw_total_price']}}</td>
                                <td>{{$item['commission']}}</td>
                                <td>{{date('Y-m-d h:i', strtotime($item['collection_date_time']))}}</td>
                                <td>{{date('Y-m-d h:i', strtotime($item['delivery_date_time']))}}</td>
                                <td><?php if ($item['status'] == 0) echo 'Pending';
                                    elseif ($item['status'] == 1) echo 'Received';
                                    elseif ($item['status'] == 2) echo 'In-Progress';
                                    elseif ($item['status'] == 3) echo 'Completed';
                                    elseif ($item['status'] == 4) echo 'Delivered';
                                    elseif ($item['status'] == 6) echo 'Ready for delievery';
                                    elseif ($item['status'] == 5) echo 'Cancelled';
                                    else echo 'Cancelled';?></td>
                                <td>
                                    <a class='btn btn-success'
                                       href="{{route('editOrderForm',['order_id'=>$item['id']])}}">Edit</a>
                                    <a class='btn btn-success'
                                       href="{{route('viewOrderDetail',['order_id'=>$item['id']])}}">View Detail
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $count++;
                            ?>
                        @endforeach
                        </tbody>
                    </table>
                    <div align='center'>
                        {{ $order->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection