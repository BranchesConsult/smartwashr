@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div class="box box-primary">

        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['url' => route('editOrder',['id'=>$order['id']])]) !!}
        <div class="box-body">
            <div class="form-group">
                <label>Status</label><br>
                <select required name="status" class="form-control">
                    <option value="">Select Status</option>
                    <option <?php echo ($order['status'] == '0') ? 'selected' : '' ?> value="0">Pending</option>
                    <option <?php echo ($order['status'] == '1') ? 'selected' : '' ?> value="1">Received</option>
                    <option <?php echo ($order['status'] == '2') ? 'selected' : '' ?> value="2">In-Progress</option>
                    <option <?php echo ($order['status'] == '3') ? 'selected' : '' ?> value="3">Completed</option>
                    <option <?php echo ($order['status'] == '6') ? 'selected' : '' ?> value="6">Ready For Delivery
                    </option>
                    <option <?php echo ($order['status'] == '4') ? 'selected' : '' ?> value="4">Delivered</option>
                    <option <?php echo ($order['status'] == '5') ? 'selected' : '' ?> value="5">Cancelled</option>
                </select>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop
