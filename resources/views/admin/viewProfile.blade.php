@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Profile</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr><th>ID</th><td>{{$users['id']}}</td></tr>
                            <tr><th>Name</th><td>{{$users['name']}}</td></tr>
                            <tr><th>Email</th><td>{{$users['email']}}</td></tr>
                            <tr><th>State/Province</th><td>{{$users['state/province']}}</td></tr>
                            <tr><th>Location</th><td>{{$users['st1']}},{{$users['st2']}},{{$users['city']}}</td></tr>
                            <tr><th>Country</th><td>{{$users['country']}}</td> </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection