                                    <?php 
                                    //dd($search);
                                    ?>
@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">
        <div class="box-header with-border">
            <div class="form-group" >
            {!! Form::open(['url' => route('viewUsers')]) !!}
                <label>Search User</label>
                <div class='input-append'>
                <input  id="email" style='height: 35px;width:30%;'
                       placeholder="Enter User Email" type="email" name="email" value="">
                
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
                {!!Form::close()!!}
            </div>
            <div class="form-group">
                <label>View User</label>
                <select style='height: 35px;width:30%;' required name="searchUserType" id="searchUserType" 
                        class="form-control"onchange="searchUserType(this.value);">
                    <option <?php echo ($selected == '0') ? 'selected' : '' ?> value="">All Users</option>
                    @if( Auth::user()->is_admin == 1)
                    <option <?php echo ($selected == '1') ? 'selected' : '' ?> value="1">Laundry Owners</option>
                    @endif
                    <option <?php echo ($selected == '2') ? 'selected' : '' ?> value="2">Drivers</option>
                    <option <?php echo ($selected == '3') ? 'selected' : '' ?> value="3">Clients</option>
                </select>
            </div>  
        </div>
        @if(count($users)==0)
        <div class="alert alert-success">
            Currently, no Users.
        </div>
        @else
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-bordered">
                <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User type</th>
                        <th>Action</th>
                    </tr>
                    @foreach($users as $item)
                    <tr>
                        <td>{{$item['id']}}</td>
                        <td style="text-transform: capitalize;">{{$item['name']}}</td>
                        <td>{{$item['email']}}</td>
                        <td>
                            <?php $count = 0; ?>
                            @foreach($item['user_type'] as $items)
                            <span class="badge">
                                <?php
                                switch ($items['type_id']) {
                                    case 0:
                                        echo 'Admin';
                                        break;
                                    case 1:
                                        echo 'Laundry owner';
                                        break;
                                    case 2:
                                        echo 'Driver';
                                        $count++;
                                        break;
                                    case 3:
                                        echo 'Client';
                                        break;
                                    default :
                                        echo '';
                                }
                                ?>
                            </span>
                            @endforeach
                        </td>
                        <td><a class='btn btn-success'href="{{route('editUserForm',['id'=>$item['id']])}}">Edit</a>
                            <a onclick="return deleteEntity()" class='btn btn-success'href="{{route('deleteUser',['id'=>$item['id']])}}">Delete</a>
                            @if($count!=0)
                            <a class='btn btn-success'href="{{route('attachLaundry',['id'=>$item['id']])}}">Attach Laundry</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if(count($users)!=1)
            <div align='center'>
                {{ $users->links() }}
            </div>
            @endif
        </div>
        @endif
    </div>
</div>
@endsection
<script type="text/javascript">
    function searchUserType(userTypeId) {
        var searchParams = {};
        if (userTypeId.length > 0) {
            searchParams = {
                user_type: userTypeId
            };
        }
        return window.location.href = "{{route('viewUsers')}}?" + decodeURIComponent($.param(searchParams));
    }
</script>