@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">
       
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('laundry'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAttachLaundry',['user_id'=>$sw_user_id])]) !!}
            <div class="box-body"> 
                <div class="form-group">
                    <label style='text-transform: capitalize;'>Attach {{$sw_user['name']}} with Laundry</label><br>
                    @foreach($laundry as $item)
                    <input  type=checkbox name="laundry[]" value="{{$item['id']}}"
                           <?php echo (in_array($item['id'], $attachlaundry)) ? 'checked' : '' ?>
                           >{{$item['name']}}<br>
                    @endforeach
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection
@section('scripts')
{!! Html::script('js/jquery/dist/jquery.validate.min.js') !!}
@stop
