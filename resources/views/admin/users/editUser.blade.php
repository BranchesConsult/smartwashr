@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('css')
<style>
    #driver_map{
        width: 100%;
        height: 500px;
    }
</style>
@stop
@section('main-content')
<?php
        $sw_lat =($swUser['sw_latitude']==null)? 21.154841 :$swUser['sw_latitude'];
        $sw_long=($swUser['sw_longitude']==null)?39.124556:$swUser['sw_longitude'];
        $ne_long=($swUser['ne_longitude']==null)?39.33056:$swUser['ne_longitude'];
        $ne_lat=($swUser['ne_latitude']==null)?21.263841:$swUser['ne_latitude'];
?>
<div class="container-fluid spark-screen">
    <div class="box">
        <div align="center" style="color:red;">
            @if ($errors->has('password')||$errors->has('password_confirmation'))
            <div>
                @foreach ($errors->all() as $error)
                {{ $error }}<br>        
                @endforeach
            </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            {!! Form::open(['url' => route('postEditUser',['user_id'=>$swUser['id']]),'id'=>'edit-user-form','files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Name</label>
                    <input required value="{{$swUser['name']}}" class="form-control" id="name" placeholder="Enter Clients Name" type="text" name="name">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input required value="{{$swUser['email']}}" type="email" name="email" id="email" class='form-control' placeholder='Email Address'/>
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input value="{{$swUser['phone_number']}}"  type="tel" name="phone" id="phone" class='form-control' placeholder='Phone Number'/>
                </div>

                <div class="form-group col-md-12 no-padding">
                   {{-- <div class="col-md-6 no-padding">
                        <label>Profie Picture(Optional)</label>

                        <input placeholder="Attach your Picture" type="file" name="pic" id="pic" value="{{$swUser['profile_pic']}}">
                    </div>--}}
                     
                <div class="input-append">
                    <label>Profie Picture(Optional)</label>
                    <input placeholder="Attach your Picture" value="{{$swUser['profile_pic']}}" class="form-control" id="pic" name="pic" type="text">
                    <a style="margin: 15px 0px;" data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-success" type="button">
                Select
            </a>
                </div>
                <div class="modal fade" id="galleryModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Picture</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="100%" height="800" 
                                        src="{{URL::to("filemanager/dialog.php?type=1&field_id=pic")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                    <div class="col-md-6">
                        @if(!empty($swUser['profile_pic']))
                        <img class="img-thumbnail img-lg" src="{{$swUser['profile_pic']}}" />
                        @endif
                    </div>

                </div>
                <div class="clear clearfix"></div>

                <div class="form-group">
                    <label>Add User As</label><br>
                    <label>
                        <input type=radio name="user_type[]" 
                               value="1" 
                               <?php echo (in_array(1, $swUser['user_type'])) ? 'checked' : '' ?>> Laundry Owner
                    </label>
                    <label>
                        <input type=radio name="user_type[]" 
                               value="3" 
                               <?php echo (in_array(3, $swUser['user_type'])) ? 'checked' : '' ?>> Client
                    </label>
                    <label>
                        <input type=radio name="user_type[]" 
                               value="2" 
                               <?php echo (in_array(2, $swUser['user_type'])) ? 'checked' : '' ?>> Driver
                    </label>
                </div>

                <div class="form-group">
                    <label>Status</label><br>
                    <select required name="active" class="form-control">
                        <option value="">Select Status</option>
                        <option <?php echo ($swUser['active'] == '1') ? 'selected' : '' ?> value="1">Active</option>
                        <option <?php echo ($swUser['active'] == '0') ? 'selected' : '' ?> value="0">In-Active</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class='form-control'  placeholder='Password'/>
                </div>
                <div class="form-group">
                    <label>Retype Password</label>
                    <input class="form-control" type='password' placeholder="Retype Password" name="password_confirmation" id='password_confirmation'>
                </div>
        <input value="{{$swUser['ne_longitude']}}" type=hidden name="ne_long" id="ne_long"  class='form-control'/>
        <input value="{{$swUser['ne_latitude']}}" type=hidden name="ne_lat" id="ne_lat"  class='form-control'/>
        <input value="{{$swUser['sw_longitude']}}" type=hidden name="sw_long" id="sw_long"  class='form-control'/>
        <input value="{{$swUser['sw_latitude']}}" type=hidden name="sw_lat" id="sw_lat"  class='form-control'/>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
    <div class="form-group">
        <label>Provide Range (If Driver)</label>
        <div id="driver_map"></div>
    </div>
            {!! Form::close() !!}
    </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
            var rectangle;
            var map;
            var infoWindow;

            function initMap() {
                

                var bounds = {
               
                    north: <?php echo  $ne_lat;?>,
                    south: <?php echo $sw_lat;?>,
                    east: <?php echo  $ne_long;?>,
                    west: <?php echo $sw_long;?>
                };
map = new google.maps.Map(document.getElementById('driver_map'), {
                    center: {lat: <?php echo ($ne_lat+$sw_lat)/2; ?>, lng: <?php echo ($ne_long+$sw_long)/2; ?>},
                    zoom: 9
                });
                rectangle = new google.maps.Rectangle({
                    bounds: bounds,
                    editable: true,
                    draggable: true
                });

                rectangle.setMap(map);

                rectangle.addListener('bounds_changed', showNewRect);

                infoWindow = new google.maps.InfoWindow();
            }
            function showNewRect(event) {
                var ne = rectangle.getBounds().getNorthEast();
                var sw = rectangle.getBounds().getSouthWest();
          
                var contentString = '<b>Rectangle moved.</b><br>' +
                        'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
                        'New south-west corner: ' + sw.lat() + ', ' + sw.lng();

                // Set the info window's content and position.
                infoWindow.setContent(contentString);
                infoWindow.setPosition(ne);
                infoWindow.open(map);
                $("#ne_lat").val(ne.lat());
                $("#ne_long").val(ne.lng());
                $("#sw_lat").val(sw.lat());
                $("#sw_long").val(sw.lng());
            }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC263na3AWmXdy9htO50E9kiq2-cgbBsMo&callback=initMap">
</script>
@stop


