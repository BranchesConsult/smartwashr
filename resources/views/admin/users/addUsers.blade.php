@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('css')
<style>
    #driver_map{
        width: 100%;
        height: 500px;
    }
</style>
@stop
@section('main-content')

<div class="container-fluid spark-screen">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('name')||$errors->has('email')||$errors->has('phone')||$errors->has('password1')||$errors->has('password2'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAddUser'), 'id'=>'add-user-form','files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Name</label>
                    <input required value="{!! old('name') !!}" class="form-control" id="name" placeholder="Enter Clients Name" type="text" name="name">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input required value="{!! old('email') !!}" type="email" name="email" id="email" class='form-control' placeholder='Email Address'/>
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input required value="{!! old('phone') !!}"  type="tel" name="phone" id="phone" class='form-control' placeholder='Phone Number'/>
                </div>
                {{-- <div class="form-group">
                    <label>Profie Picture(Optional)</label>                
                <input placeholder="Attach your Picture" type="file" name="pic" id="pic">
        </div>--}}

        <div class="input-append">
            <label>Profie Picture(Optional)</label>
            <input placeholder="Attach your Picture" class="form-control" id="pic" name="pic" type="text">
            <a style="margin: 15px 0px;" data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-success" type="button">
                Select
            </a>
        </div>
        <div class="modal fade" id="galleryModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Picture</h4>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="800" 
                                src="{{URL::to("filemanager/dialog.php?type=1&field_id=pic")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                                                </div>
                        </div>
                    </div>
                </div>
             
           <div class="form-group">
                    <label>Add User As</label><br>
            <input type=radio name="user_type[]" value="1"> Laundry Owner
            <input type=radio name="user_type[]" value="2"> Driver
            <input type=radio name="user_type[]" value="3" checked> Client<br>
        </div>
        <div class="form-group">
            <label>Status</label><br>
            <select required name="status" id="status" class="form-control">
                <option value="">Select Status</option>
                <option value="1">Active</option>
                <option value="0">In-Active</option>
            </select>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input required type="password" name="password" id="password" class='form-control'  placeholder='Password'/>
        </div>
        <div class="form-group">
            <label>Retype Password</label>
            <input required  class="form-control" type='password' placeholder="Retype Password" name="password2" id='password2' >
        </div>
        <input value="" type=hidden name="ne_long" id="ne_long"  class='form-control'/>
        <input value="" type=hidden name="ne_lat" id="ne_lat"  class='form-control'/>
        <input value="" type=hidden name="sw_long" id="sw_long"  class='form-control'/>
        <input value="" type=hidden name="sw_lat" id="sw_lat"  class='form-control'/>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
    </div>
    <div class="form-group">
        <label>Provide Range (If Driver)</label>
        <div id="driver_map"></div>
    </div>
    {!! Form::close() !!}

</div>
</div>
</div>
@endsection
@section('scripts')
{!! Html::script('js/jquery/dist/jquery.validate.min.js') !!}
    <script>
        $('#add-user-form').validate({
    rules: {
        password: {
            pwRestrict: true,
            minlength: 6    
        },
        password2: {
            required: true,
            equalTo:'#password'
         }    

        }
        });
        $.validator.addMethod("pwRestrict", function (value) {
    return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            &&     /\d/.test(value) // has a digit
        }, "Password must have at least six characters with at least one alphabet and one number");
</script>
        <script>

            var rectangle;
            var map;
            var infoWindow;

            function initMap() {
                map = new google.maps.Map(document.getElementById('driver_map'), {
                    
                    center: {lat: 21.209341, lng: 39.227556},
                    zoom: 9
                });

                var bounds = {
                    north: 21.263841,
                    south: 21.154841,
                    east: 39.33056,
                    west: 39.124556
                };

                rectangle = new google.maps.Rectangle({
                    bounds: bounds,
                    editable: true,
                    draggable: true
                });

                rectangle.setMap(map);

                rectangle.addListener('bounds_changed', showNewRect);

                infoWindow = new google.maps.InfoWindow();
            }
            function showNewRect(event) {
                var ne = rectangle.getBounds().getNorthEast();
                var sw = rectangle.getBounds().getSouthWest();
//                var nw = new google.maps.LatLng(ne.lat(), sw.lng());
//                var se = new google.maps.LatLng(sw.lat(), ne.lng());
                //var lat = document.getElementById('#lat');
                //alert(nw);
//                lat.value = nw;
//                var long1 = document.getElementById('#long');
//                long1.value = se;
          
                var contentString = '<b>Rectangle moved.</b><br>' +
                        'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
                        'New south-west corner: ' + sw.lat() + ', ' + sw.lng();

                // Set the info window's content and position.
                infoWindow.setContent(contentString);
                infoWindow.setPosition(ne);
                infoWindow.open(map);
                $("#ne_lat").val(ne.lat());
                $("#ne_long").val(ne.lng());
                $("#sw_lat").val(sw.lat());
                $("#sw_long").val(sw.lng());
            }
                    </script>
                    <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC263na3AWmXdy9htO50E9kiq2-cgbBsMo&callback=initMap">
                    </script>
                        @stop