@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('category')||$errors->has('category_ar')||$errors->has('description')
                ||$errors->has('status'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAddCategory')]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Name</label>
                    <input required value="{!! old('category') !!}" class="form-control" id="category" placeholder="Enter Category" type="text" name="category">
                </div>
                <div class="form-group">
                    <label>Arabic Name</label>
                    <input required value="{!! old('category_ar') !!}" class="form-control" id="category_ar" placeholder="Enter Category in Arabic" type="text" name="category_ar">
                </div>
                <div class="input-append">
                    <label>Category Picture(Optional)</label>
                    <input placeholder="Attach your Picture" class="form-control" id="pic" name="pic" type="text">
                    <a style="margin-top: 15px;margin-bottom: 15px;" data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-success" type="button">
                        Select
                    </a>
                </div>
                <div class="modal fade" id="galleryModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Picture</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="100%" height="800" 
                                        src="{{URL::to("filemanager/dialog.php?type=1&field_id=pic")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <div class='form-group'>
                    <label>Is Parent</label><br>
                    <input type="radio" name="is_parent" value="1" checked>Yes
                    <input type="radio" name="is_parent" value="0">No<br>
                </div>     
                <div class="form-group">
                    <label>Parent Category</label><br>
                    <?php echo ($categories->renderAsDropdown()); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea required value="{!! old('description') !!}" type="text" name="description" id="description" class='form-control' placeholder='Enter Description'></textarea>
                </div>
                <div class="form-group">
                    <label>Select Status</label><br>
                    <select required name="status" id="status" class="form-control">
                        <option value="">Select Status</option>
                        <option value="1">Active</option>
                        <option value="0">In-Active</option>
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection
@section('scripts')
<!--{!! Html::script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js') !!}
<script type="text/javascript">
    $(document).ready(function(){
        setTimeout(function(){
                        $("#add-client-form").validate();

        }, );

    })
</script>-->
@stop
