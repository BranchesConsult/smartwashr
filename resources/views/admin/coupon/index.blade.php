@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Coupon
@endsection
@section('main-content')
    <div class="container-fluid spark-screen">
        <!--Default box -->
        <div class="box">
            <div align="center" style="color:red;">
                @if(session('message'))
                    <h4>{{session('message')}}</h4>
                @endif
                <div>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Code</th>
                        <th>Valid from</th>
                        <th>Valid to</th>
                        <th>Discount</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($coupons as $key => $val)
                        <tr>
                            <td>{!! $val['code'] !!}</td>
                            <td>{!! $val['valid_from'] !!}</td>
                            <td>{!! $val['valid_to'] !!}</td>
                            <td>{!! $val['discount'] !!}%</td>
                            <td>{!! ($val['status'] == 0) ? 'Disabled':'Enabled' !!}</td>
                            <td>
                                <a href="{!! route('edit_coupons_view', ['id' => $val['id']]) !!}"
                                   class="btn btn-warning">
                                    Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {!! $coupons->links() !!}
        </div>
    </div>
@endsection
@section('scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC263na3AWmXdy9htO50E9kiq2-cgbBsMo&callback=initMap">
    </script>
@endsection