@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('css')
    {!! Html::style(asset('datepicker/css/bootstrap-datetimepicker.min.css')) !!}
@stop
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div align="center" style="color:red;">
                    @if(session('message'))
                        <h4>{{session('message')}}</h4>
                    @endif
                    <div>
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                </div>
                {!! Form::open(['url' => route('edit_coupons', ['id' => $coupun['id']])]) !!}
                <input type="hidden" name="code" value="{!! $coupun['code'] !!}"/>
                <div class="box-body">
                    <div class="form-group">
                        <label>Code</label>
                        <input required value="{!! $coupun['code'] !!}" class="form-control" id="code"
                               placeholder="Enter Code" type="text" name="code">
                    </div>
                    <div class="form-group">
                        <label>Discount %age</label>
                        <input required value="{!! $coupun['discount'] !!}" class="form-control" id="discount"
                               placeholder="Enter discount" type="text" name="discount">
                    </div>
                    <div class="form-group">
                        <label>Valid from</label>
                        <input required value="{!! $coupun['valid_from'] !!}" class="form-control date_time"
                               id="valid_from"
                               placeholder="Valid from" type="text" name="valid_from">
                    </div>
                    <div class="form-group">
                        <label>Valid till</label>
                        <input required value="{!! $coupun['valid_to'] !!}" class="form-control date_time"
                               id="valid_to"
                               placeholder="Valid to" type="text" name="valid_to">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Please select</option>
                            <option value="0" {!! ($coupun['status'] == 0) ? 'selected' : '' !!}>Disable</option>
                            <option value="1" {!! ($coupun['status'] == 1) ? 'selected' : '' !!}>Enable</option>
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    {!! Html::script(asset('datepicker/js/moment.js')) !!}
    {!! Html::script(asset('datepicker/js/bootstrap-datetimepicker.min.js')) !!}

    <script>
        $(document).ready(function () {
            $('.date_time').datetimepicker({
                //minDate: moment().format(),
                format: 'YYYY-MM-DD h:mm a'
            });
        });
    </script>
@stop