@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')

<div class="container-fluid spark-screen">

            <!-- Default box -->
            <div class="box">
                @if(count($laundry)==0)
        <div class="alert alert-success">
            Currently, no laundry available.
        </div>
        @else
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Owner ID</th>
                                <th>City</th>
                                @if( Auth::user()->is_admin == 1)
                                <th>Action</th>
                                @endif
                            </tr>
                            @foreach($laundry as $item)
                            
                            <tr>
                                <td>{{$item['id']}}</td>
                                <td style="text-transform: capitalize;">{{$item['name']}}</td>
                                <td>{{$item['code']}}</td>
                                <td>{{$item['owner_id']}}</td>
                                <td>{{$item['city']}}</td>
                                @if( Auth::user()->is_admin == 1)
                                <td><a class='btn btn-success'href="{{route('editLaundry',['id'=>$item['id']])}}">Edit</a>
                                <a onclick="return deleteEntity()" class='btn btn-success'href="{{route('deleteLaundry',['id'=>$item['id']])}}">
                                    Delete
                                </a></td>
                                @endif
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    <div align='center'>
                    {{ $laundry->links() }}
                    </div>
                
        </div>
        @endif
</div>
</div>
@endsection