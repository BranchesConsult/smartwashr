
@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('name')||$errors->has('email')||$errors->has('phone')||$errors->has('city')
                ||$errors->has('owner_id')||$errors->has('st1')||$errors->has('status')||$errors->has('country'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAddLaundry'),'files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label>Laundry Name</label>
                    <input required value="{!! old('name') !!}" class="form-control" id="name" placeholder="Enter Clients Name" type="text" name="name">
                </div>
                {{--<div class="form-group">
                    <label>Laundry Logo(Optional)</label>
                    <input placeholder="Attach your Picture" type="file" name="logo" id="logo">
                </div>--}}
                <div class="input-append">
                    <label>Laundry Logo(Optional)</label>
                    <input placeholder="Attach your Picture" class="form-control" id="logo" value="" name="logo" type="text">
                    <a style="margin: 15px 0px;"class='btn btn-success'data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-primary" type="button">
                        Select
                    </a>
                </div>
                <div class="modal fade" id="galleryModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Logo</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="100%" height="800" 
                                        src="{{URL::to("filemanager/dialog.php?type=1&field_id=logo")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <div class="form-group">
                    <label>Code(Optional)</label>
                    <input value="{!! old('code') !!}" type="text" name="code" id="code" class='form-control' placeholder='Enter Code'/>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input value="{!! old('email') !!}" required type="email" name="email" id="email" class='form-control' placeholder='Enter Email'/>
                </div>
                <div class="form-group">
                    <label>Street 1</label>
                    <textarea required value="{!! old('st1') !!}"  type="text" name="st1" id="st1" class='form-control' placeholder='Street 1'></textarea>
                </div>
                <div class="form-group">
                    <label>Street 2</label>
                    <textarea value="{!! old('st2') !!}"  type="text" name="st2" id="st2" class='form-control' placeholder='Street 2'></textarea>
                </div>
                <div class="form-group">
                    <label>Phone Number</label>
                    <input required value="{!!old('phone')!!}"  type="text" name="phone" id="phone" class='form-control' placeholder='Enter Phone Number(1 each line)'>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input required type="text" name="user_entered_address" id="autocomplete" value="{!!old('user_entered_address')!!}"
                           class='form-control' placeholder='Enter your address'/>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <input readonly="true" required value="{!! old('country') !!}"  type="text" name="country" id="country" class='form-control' placeholder='Enter Country'/>
                </div>
                <div class="form-group">
                    <label>State</label>
                    <input readonly="true" required value="{!! old('state') !!}"  type="text" name="state" id="administrative_area_level_1" class='form-control' placeholder='Enter State'/>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <input readonly="true" required value="{!! old('city') !!}"  type="text" name="city" id="locality" class='form-control' placeholder='Enter City'/>
                </div>
                <div class="form-group">
                    <label>ZIP</label>
                    <input readonly="true" value="{!! old('zip') !!}"  type="number" name="zip" id="postal_code" class='form-control' placeholder='Enter ZIP'/>
                </div>
                <div class="form-group" style="display:<?php echo ($is_admin == false) ? 'none' :'' ?>">
                    <label>Select Laundry Owner</label>
                    <select required name="owner_id" id="owner_id"  class="form-control">
                        <option value="{{\Auth::user()->id}}">Admin</option>
                        @foreach($laundry as $item)
                        <option value="{{$item['user']['id']}}">{{$item['user']['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Status</label><br>
                    <select required name="status" id="status" class="form-control">
                        <option value="">Select Status</option>
                        <option value="1">Active</option>
                        <option value="0">In-Active</option>
                    </select>
                </div>
                <div class="form-group">
                    <input value="{!! old('latitude') !!}" type="hidden" name="latitude" id="latitude" class='form-control' placeholder='Enter Latitude'/>
                </div>
                <div class="form-group">
                    <input value="{!! old('longitude') !!}" type="hidden" name="longitude" id="longitude" class='form-control' placeholder='Enter Longitude'/>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('scripts')
{!! Html::script('/js/map.js') !!}
{!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyC263na3AWmXdy9htO50E9kiq2-cgbBsMo&libraries=places&callback=initAutocomplete') !!}
@stop
