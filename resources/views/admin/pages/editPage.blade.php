@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Page</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div align="center" style="color:red;">
     @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
    @if ($errors->has('title')||$errors->has('content')||$errors->has('content_ar'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br>        
        @endforeach
    </div>
    @endif
    </div>
    {!! Form::open(['url' => route('postEditPage',['id'=>$pageid['id']])]) !!}
    <div class="box-body">
        <div class="form-group">
            <label>Title</label>
            {{ Form::text('title', $pageid['title'],['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Enter Content in English</label>
            {{ Form::textarea('content', $pageid['content'],['class' => 'form-control wysiwyg'])}}
        </div> 
        <div class="form-group">
            <label>Enter Content in Arabic</label>
            {{ Form::textarea('content_ar', $pageid['content_ar'],['class' => 'form-control wysiwyg'])}}
        </div> 
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-success">Update</button>
    </div>
    {!! Form::close() !!}
</div>
{!! Html::script('js/tinymce/tinymce.min.js'); !!}
{!! Html::script('js/tinymce.js'); !!}
@stop
