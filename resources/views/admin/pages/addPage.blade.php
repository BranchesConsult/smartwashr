@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">
       
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            
    <!-- /.box-header -->
    <!-- form start -->
    <div align="center" style="color:red;">
     @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
    @if ($errors->has('title')||$errors->has('content')||$errors->has('content_ar'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br>        
        @endforeach
    </div>
    @endif
    </div>
        
        <form method="POST" action="{{route('postAddPage')}}" novalidate>
    <div class="box-body">
        <div class="form-group">
            <label>Title</label>
            <input required value="{!! old('title') !!}" class="form-control" id="title" placeholder="Enter title" type="text" name="title">
        </div>
        <div class="form-group">
            <label>Enter Content in English </label>
        <textarea required class="form-control wysiwyg" rows="3" placeholder="EnterContent" name="content"></textarea>
        </div> 
        <div class="form-group">
            <label>Enter Content in Arabic</label>
        <textarea required class="form-control wysiwyg" rows="3" placeholder="EnterContent" name="content_ar"></textarea>
        </div> 
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-success">Submit</button>
    </div>
    </form>
</div>
</div>
    </div>

        @stop
{!! Html::script('js/tinymce/tinymce.min.js'); !!}
{!! Html::script('js/tinymce.js'); !!}