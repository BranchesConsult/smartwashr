@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')

<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pages</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th style='width: 200px;'>Title</th>
                  <th style='width: 200px;'>Slug</th>
                  
                  <th>Action</th>
                </tr>
                @foreach ($pages as $item) 
                <tr>
                  <td>{{$item['id']}}</td>
                  <td style='text-transform: capitalize;'>{{$item['title']}}</td>
                  <td>{{$item['slug']}}</td>
                  
                  <td><a class='btn btn-success' href="{{route('editPage',['id'=>$item['id']])}}">Edit</a>
                      <a class='btn btn-success' onclick="return deleteEntity()" href="{{route('deletePage',['id'=>$item['id']])}}"> Delete</a></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         </div>

@stop