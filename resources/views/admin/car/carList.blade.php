@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')

<div class="container-fluid spark-screen">

            <!-- Default box -->
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Driver_Id</th>
                            </tr>
                            @foreach($car as $item)
                            <tr>
                                <td>{{$item['id']}}</td>
                                <td>{{$item['driver_id']}}</td>
                                <td><a class='btn btn-bitbucket'href="{{route('editCar',['id'=>$item['id']])}}">Edit</a>
                                <a onclick="return deleteEntity()" class='btn btn-bitbucket'
                                   href="{{route('deleteCar',['id'=>$item['id']])}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div align='center'>
                    {{ $car->links() }}
                    </div>
                
        </div>
</div>
</div>
@endsection