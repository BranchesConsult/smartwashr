
@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
    <div class="box">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <div align="center" style="color:red;">
                @if(session('message'))
                <h4>{{session('message')}}</h4>
                @endif
                @if ($errors->has('driver_id')||$errors->has('pic'))
                <div>
                    @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                    @endforeach
                </div>
                @endif
            </div>
            {!! Form::open(['url' => route('postAddCar'),'files' => true]) !!}
            <div class="box-body">
                <div class="input-append">
                    <label>Car Regd Book(image)</label>
                    <input placeholder="Attach Car Regd. image" required class="form-control" id="pic" value="" name="pic" type="text">
                    <a data-toggle="modal" href="javascript:;" data-target="#galleryModal" class="btn btn-primary" type="button">
                        Select
                    </a>
                </div>
                <div class="modal fade" id="galleryModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Image</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="100%" height="800" 
                                        src="{{URL::to("filemanager/dialog.php?type=1&field_id=car_regd")}}" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <div class="form-group">
                    <select required name="owner_id" id="driver_id" class="form-control">
                        <option value="">Select Driver ID</option>
                        @foreach($sw_user as $item)
                        <option value="{{$item['id']}}">{{$item['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- /.modal -->
@endsection
@section('scripts')
{!! Html::script('/js/map.js') !!}
{!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyC263na3AWmXdy9htO50E9kiq2-cgbBsMo&libraries=places&callback=initAutocomplete') !!}
@stop
