@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('css')

@stop
@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div align="center" style="color:red;">
                    @if(session('message'))
                        <h4>{{session('message')}}</h4>
                    @endif
                    <div>
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                </div>
                {!! Form::open(['url' => route('push.frm')]) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Notification title</label>
                        <input type="text" class="form-control" required name="notification_title"/>
                    </div>
                    <div class="form-group">
                        <label>Notification text</label>
                        <textarea class="form-control" required name="notification_text"></textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">Send to all</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')

@stop