@extends('layouts.app')
@section('title')
Home
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        Your page content here
    </div>
</section>
@stop
