@extends('layouts.app')
@section('title')
    Your info
@stop
@section('css')
    {!! Html::style('//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css') !!}
    {!! Html::style('//cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.css') !!}
    <style>
        .content-section {
            min-height: 100%;
        }

        .text_map_top {
            width: 100%;
            display: inline-block;
            font-family: Roboto-Regular;
            font-size: 20px;
            color: #47da35;
            text-transform: uppercase;
            margin-top: 146px;
            margin-bottom: 29px;
        }

        .less_area_container {
            width: 90%;
            max-width: 1056px;
            margin: 0 auto;
        }

        .label_for_calander {
            width: 300px;
            display: inline-block;
            float: left;
            border: 1px solid #e5e5e4;
        }

        .label_for_calander i {
            font-size: 20px;
            color: #fff;
            background: #c6c6c6;
            width: 50px;
            float: right;
            text-align: center;
            padding: 13px;

        }

        .date_area {
            border: none;
            padding: 12px 17px;
        }

        .text_map_top_date {
            width: 100%;
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #47da35;
            padding-bottom: 30px;
        }

        .right_area {
            border-top: 1px solid #eaeaea;
            display: inline-block;
        }

        .total_text {
            display: inline-block;
            font-family: Roboto;
            font-size: 14px;
            color: #35d85a;
            float: right;
            font-weight: bold;
            padding-top: 33px;
        }

        .total_text span {
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #71767a;
            margin-left: 130px;
        }

        .total_tex1 {
            display: inline-block;
            font-family: Roboto;
            font-size: 14px;
            color: #35d85a;
            float: right;
            font-weight: bold;
            padding-top: 33px;
        }

        .total_tex1 span {
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #71767a;
            margin-left: 130px;
        }

        .right_section_right {
            width: 310px;
            display: inline-block;
            float: right;
        }

        .chck_area_btn {
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #fff !important;
            background: #48db44;
            width: 90px;
            padding: 10px 0;
            text-align: center;
            float: right;
            margin-top: 25px;
            text-decoration: none;
            border: none;
        }

        .chck_area_btn:hover {
            color: #fff;
            text-decoration: none;
        }

        .area_space {
            padding: 40px 0;
        }

        .table_area {
            display: block;
            margin-top: 40px;
        }

        .dell_refresh {

        }

        .dell_quant {
            width: 200px;
            display: inline-block;
            padding: 11px;
            border: 1px solid #eee;
        }

        .dell_refresh {
            border: none;
        }

        .dell_quant span i {
            float: right;
            padding-left: 20px;
            font-size: 14px;
        }

        .imge_press {
            width: 100%;
            display: inline-block;
            text-align: center;
        }

        .imge_press span {
            width: 100%;
            display: inline-block;
            text-align: center;
        }

        .abay_text {
            padding-top: 52px;
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #71767a;
        }

        .input_nuber_field {
            border: none;
            width: 35%;
        }

        .table > tbody > tr > td {
            border-top: none !important;
            margin-top: 27px !important;
        }

        .spacer_in_tr {
            margin-top: 20px;
        }

        .map_img {
            width: 100%;
            display: inline-block;
        }

        .edit_margin_class img {
            margin: 20px 0;
        }

        .dell_quant {
            margin-top: 39px;
        }

        @media all and (min-width: 961px) and (max-width: 1200px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
                width: 100%;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 768px) and (max-width: 960px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 768px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 600px) {
            .input_field_map_top {
                width: 450px !important;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 480px) {
            .input_field_map_top {
                width: 276px;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        #laundry_map, #user-location-map {
            width: 100%;
            height: 500px;
        }

        .personal_info {
            padding-top: 40px;
        }

        #remaining-order {
            display: none;
        }

        .total_text {
            color: #71767a !important;
        }
    </style>
@stop
@section('content')
    <section class="bg-white content-section">
        <div class="container">
            <div class="less_area_container">
                <div>
                    {!! Form::open(['url'=>route('orderComplete')]) !!}
                    <input type="hidden" name="user-lat" id="user-lat" value="{{$userLat}}"/>
                    <input type="hidden" name="user-lng" id="user-lng" value="{{$userLng}}"/>
                    <input type="hidden" name="driverId" id="driverId" value="{{$driverId}}"/>
                    <input type="hidden" name="laundryId" id="laundryId" value="{{$laundryId}}"/>
                    <div class="col-md-12 personal_info no-padding">
                        <div class="col-md-6 pull-left form-group">
                            <label>
                                {{ __('home.email') }}
                                <input required class="form-control" name="user_email" value="{{$user['email']}}"/>
                            </label>
                        </div>
                        <div class="col-md-6 pull-right form-group">
                            <label>
                                {{ __('home.Phone#') }}
                                <input placeholder="CountryCode Operator Number" required class="form-control"
                                       name="user_phone" value="{{$user['phone_number']}}"/>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-12 personal_info no-padding">
                        <div class="col-md-6 pull-left form-group">
                            <label>
                                Address
                                <textarea required class="form-control" name="user-address" id="user-address"
                                >{{$location}}</textarea>
                            </label>
                        </div>
                        <div class="col-md-6 pull-right form-group">
                            <label>
                                Postcode
                                <input class="form-control"
                                       name="post_code" value="{{$user['zip']}}"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 table_area no-padding">
                        <div class="table-responsive">
                            <table class="table edit_margin_class">
                                <thead>
                                <tr>
                                    <th>{{ __('home.product') }}</th>
                                    <th style=" text-align: center;">{{ __('home.service') }}</th>
                                    <th>{{ __('home.quantity') }}</th>
                                    <th>{{ __('home.unit_price') }}</th>
                                    <th>{{ __('home.total') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cartProducts as $key=>$val)
                                    <tr id="product-{{$key}}">
                                        <td><span class="abay_text">
                                        @if(\Session::get('language')=='en')
                                                    {{$val['name']}}
                                                @else
                                                    @foreach($product as $item)
                                                        @if($item['name']==$val['name'])
                                                            {{$item['name_ar']}}
                                                        @endif
                                                    @endforeach
                                                @endif
                                    </span>
                                        </td>
                                        <td>
                                    <span class="imge_press">
                                        <img src="{{URL::to('img/dryclean-press.png')}}" alt=""/>
                                        <span>
                                            @if($val['attributes']['service_selected']=='washing')
                                                {{ __('home.washing') }}
                                            @elseif($val['attributes']['service_selected']=='dryclean')
                                                {{ __('home.dryclean') }}
                                            @else
                                                {{ __('home.press') }}
                                            @endif
                                        </span>
                                    </span>
                                        </td>
                                        <td>
                                            <label class="dell_quant">

                                                <input
                                                        value="{!! (\Session::get('language')=='en') ? $val['quantity'] : numToArabic($val['quantity']) !!}"
                                                        type="text"
                                                        onblur="updateQty('{{$key}}','product-{{$key}}', '{{$val['price']}}')"
                                                        class="input_nuber_field pro_qty"/>

                                                <span>
                                            <a href="javascript:deleteCartProduct('{{$key}}')">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                                <span>
                                            <a href="javascript:updateQty('{{$key}}','product-{{$key}}', '{{$val['price']}}')">
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                            </label>
                                        </td>
                                        <?php
                                        $price_ar = numToArabic($val['price']);
                                        $total_price_ar = numToArabic($val['price'] * $val['quantity']);
                                        ?>
                                        <td>
                                    <span class="abay_text">
                                        {{ __('home.currency') }}
                                        <span class="unitPrice">
                                        @if(\Session::get('language')=='en')
                                                {{$val['price']}}
                                            @else
                                                {{$price_ar}}
                                            @endif
                                        </span>

                                    </span>
                                        </td>
                                        <td>
                                    <span class="abay_text">
                                        {{ __('home.currency') }}
                                        <span class="totalPrice">
                                            @if(\Session::get('language')=='en')
                                                {{$val['price']*$val['quantity']}}
                                            @else
                                                {{$total_price_ar}}
                                            @endif
                                        </span>
                                    </span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12 no-padding area_space">
                        <div class="col-md-4 no-padding">
                            <span class="text_map_top_date">{{ __('home.collection_time') }}</span>
                            <label class="label_for_calander">
                                <input required type="text" name="collection_date_time" id="date_timepicker_start"
                                       class="date_area dateTimepickerCollection">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </label>
                            <small class="help-block">
                                Collection time must be at least 2 hours ahead from now
                            </small>
                        </div>
                        <div class="col-md-4 no-padding">
                            <span class="text_map_top_date">{{ __('home.delivery_time') }}</span>
                            <label class="label_for_calander">
                                <input required type="text" id="date_timepicker_end" name="delivery_date_time"
                                       class="date_area dateTimepickerDelievery">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </label>
                            <small class="help-block">
                                Delivery time must be at least 24 hours ahead from collection time
                            </small>
                        </div>
                        <div class="col-md-4 no-padding">
                            <span class="text_map_top_date">
                               Laundry instructions
                            </span>
                            <textarea class="form-control" name="laundry_instructions"></textarea>
                        </div>

                    </div>
                    <div class="col-md-12 right_area no-padding">
                        <div class="col-md-3 no-padding">
                            <span class="text_map_top_date">
                            Payment mode
                            </span>
                            <select required name="payment-method" class="form-control">
                                <option value="">
                                    Please select
                                </option>
                                <option value="c_o_d">
                                    Cash on delivery
                                </option>
                                <option disabled value="payfort">
                                    Payfort
                                </option>
                            </select>
                        </div>
                        <div class="right_section_right">
                    <span class="total_text">
                        <?php
                        $cartTotal_ar = numToArabic($cartTotal);
                        $cartSubtotal_ar = numToArabic($cartSubtotal);
                        ?>
                        {{ __('home.subtotal') }}:
                             <span id="subTotalCart">
                        @if(\Session::get('language')=='en')
                                     {{$cartSubtotal}}
                                 @else
                                     {{$cartSubtotal_ar}}
                                 @endif
                             </span>
                        {{ __('home.currency') }}
                    </span>
                            <span class="total_text">
                        
                        Delivery Charges:
                             <span>
                         {{config('const.delivery_charges')}}
                             </span>
                                SAR
                    </span>
                            <br>
                            <span class="total_text">
                        {{ __('home.total') }}:
                        <span id="cart-total-price">
                            @if(\Session::get('language')=='en')
                                {{$cartTotal}}
                            @else
                                {{$cartTotal_ar}}
                            @endif

                        </span>
                                {{ __('home.currency') }}
                    </span>

                        </div>
                        <div class="col-md-12 pull-right no-padding">
                            <button id="checkoutBtn" type="submit" class="chck_area_btn">
                                {{ __('home.checkout') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('js')
    {!! Html::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') !!}
    {!! Html::script('//cdn.jsdelivr.net/g/jquery.ui.timepicker.addon@1.4.5(jquery-ui-timepicker-addon.min.js+jquery-ui-sliderAccess.js)') !!}
    {!! Html::script('js/moment/moment.min.js') !!}
    <script>
        $(function () {
            var startDateTextBox = $('#date_timepicker_start');
            var endDateTextBox = $('#date_timepicker_end');

            startDateTextBox.datetimepicker({
                timeFormat: 'hh:mmTT',
                dateFormat: 'dd-mm-yy',
                minDateTime: new Date,
                controlType: 'select',
                showButtonPanel: false,
                timeInput: true,
                maxTime: '9:00 pm',
                minTime: '8:00 am',

                onSelect: function (selectedDateTime) {
                    var selectedTime = (startDateTextBox.val()).split(' ');
                    var selectedHr = moment(selectedTime[1], 'HH:mm a').format('HH:mm');
                    selectedHr = selectedHr.split(':');
                    selectedHr = selectedHr[0];
                    if (selectedHr >= 18) {
                        var selectedDate = moment(selectedTime[0], 'DD-MM-YYYY').add(1, 'days').format("DD-MM-YYYY HH:mma");
                    } else {
                        var selectedDate = moment(selectedTime[0], 'DD-MM-YYYY').add(1, 'days').format("DD-MM-YYYY HH:mma");
                    }
                    endDateTextBox.datetimepicker('option', 'minDate', selectedDate);
                }
            });
            endDateTextBox.datetimepicker({
                timeFormat: 'hh:mmTT',
                dateFormat: 'dd-mm-yy',
                minDateTime: new Date,
                controlType: 'select',
                timeInput: true,
                showButtonPanel: false,
                maxTime: '9:00 pm',
                minTime: '9:30 am',
                onClose: function (dateText, inst) {
                    if (startDateTextBox.val() != '') {
                        var testStartDate = startDateTextBox.datetimepicker('getDate');
                        var testEndDate = endDateTextBox.datetimepicker('getDate');
                        if (testStartDate > testEndDate)
                            startDateTextBox.datetimepicker('setDate', testEndDate);
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime) {
                    startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
                }
            });
        });
        function initMap() {

        }
    </script>
@stop