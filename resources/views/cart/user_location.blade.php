@extends('layouts.app')
@section('title')
    Your info
@stop
@section('css')
    <style>
        .content-section {
            min-height: 100%;
        }

        .less_area_container {
            width: 90%;
            max-width: 1056px;
            margin: 0 auto;
        }

        .label_for_calander i {
            font-size: 20px;
            color: #fff;
            background: #c6c6c6;
            width: 50px;
            float: right;
            text-align: center;
            padding: 13px;

        }

        .total_text span {
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #71767a;
            margin-left: 130px;
        }

        .total_tex1 span {
            display: inline-block;
            font-family: Roboto-Medium;
            font-size: 16px;
            color: #71767a;
            margin-left: 130px;
        }

        .dell_quant span i {
            float: right;
            padding-left: 20px;
            font-size: 14px;
        }

        .imge_press {
            width: 100%;
            display: inline-block;
            text-align: center;
        }

        .imge_press span {
            width: 100%;
            display: inline-block;
            text-align: center;
        }

        .table > tbody > tr > td {
            border-top: none !important;
            margin-top: 27px !important;
        }

        #user-location-map {
            height: 400px;
            width: 100%;
        }

        .edit_margin_class img {
            margin: 20px 0;
        }

        .dell_quant {
            margin-top: 39px;
        }

        @media all and (min-width: 961px) and (max-width: 1200px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
                width: 100%;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 768px) and (max-width: 960px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 768px) {
            .text_map_top {
                margin-top: 56px;
            }

            .input_field_map_top {
                margin-top: 56px;
            }

            .less_area_container {
                width: 100%;
            }

            .col-md-4 {
                width: 100%;
                text-align: center;
            }

            .right_area {
                width: 100%;
                text-align: center;
            }

            .col-md-6 {
                text-align: center;
            }

            .label_for_calander {
                width: 300px;
                display: block;
                float: none;
                border: 1px solid #e5e5e4;
                margin: 0 auto;
            }

            .text_map_top_date {
                padding-top: 21px;
            }

            .area_space {
                padding: 42px 0;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 600px) {
            .input_field_map_top {
                width: 450px !important;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        @media all and (min-width: 100px) and (max-width: 480px) {
            .input_field_map_top {
                width: 276px;
            }

            .imge_press {
                width: 100%;
                display: inline-block;
                text-align: center;
            }

            .imge_press span {
                width: 100%;
                display: inline-block;
                text-align: center;
            }
        }

        .personal_info {
            padding-top: 40px;
        }

        #remaining-order {
            display: none;
        }
    </style>
@stop
@section('content')
    <section class="bg-white content-section">
        <div class="container">
            <div class="less_area_container">
                <div class="col-md-12">
                    {!! Form::open(['url'=>route('getDriverInLocation'), 'methos'=>'get', 'id'=>'get-near-driver']) !!}
                    <input type="hidden" name="user-lat" id="user-lat"/>
                    <input type="hidden" name="user-lng" id="user-lng"/>
                    <div class="form-group">
                        <label>
                            Please drag to your location on map
                        </label>
                        <input required readonly type="text" class="input_field_map_top form-control" name="location"
                               id="user-location">
                    </div>
                    <div class="form-group">
                        <div id="user-location-map"></div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            Check availibility
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop
@section('js')
    <script>
        $(function () {
            //Ajax
            $("#get-near-driver").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('methos'),
                    data: {
                        lat: $("#user-lat").val(),
                        lng: $("#user-lng").val()
                    },
                    success: function (result) {
                        console.log(result.id);
                        if (result.id != 'undefined' && result.id > 0) {
                            window.location.href = '{{route("getUserInfo")}}?user-lat=' + $("#user-lat").val() + '&user-lng=' + $("#user-lng").val() + "&driver=" + result.id + "&address_location=" + $("#user-location").val();
                        } else {
                            alert("Sorry! We do not operate in your area.");
                        }
                    }
                });
            });
        });


        ////////Getting user current location
        var autocompletePickup;
        function initMap() {
            getLocation();
        }


        ////////Getting user current location
        function getCurrentLocation(position) {
            //var myLatLng = {lat: parseFloat(21.2854), lng: parseFloat(39.2376)};
            //if (position.length > 0) {
            var myLatLng = {lat: parseFloat(position.coords.latitude), lng: parseFloat(position.coords.longitude)};
            $("#user-lng").val(position.coords.latitude);
            $("#user-lat").val(position.coords.longitude);
            var geocoder = new google.maps.Geocoder();             // create a geocoder object
            var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);    // turn coordinates into an object
            geocoder.geocode({'latLng': location}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {           // if geocode success
                    var add = results[0].formatted_address;
                    $("#user-location").val(add);
                }
            });
            //}

            var map = new google.maps.Map(document.getElementById('user-location-map'), {
                zoom: 10,
                center: myLatLng
            });
//             var service=<?php //echo $service; ?>
//            var triangleCoords = [];
//            $.each (service function(k,v){
//                triangleCoords=[
//                {lat: v.ne_latitude, lng: v.ne_longitude},
//                {lat: v.sw_latitude, lng: v.sw_longitude}
//                ];
//                });
                //console.log(triangleCoords);   
//        var triangleCoords = [
//            new google.maps.LatLng(21.697687707545228, 39.27013519531249),
//        new google.maps.LatLng( 21.444644047936553, 39.086103851562484),       
//        ];
//          var bermudaTriangle = new google.maps.Polygon({
//          paths: triangleCoords,
//          strokeColor: '#FF0000',
//          strokeOpacity: 0.8,
//          strokeWeight: 2,
//          fillColor: '#FF0000', 
//          fillOpacity: 0.35
//        });
//        bermudaTriangle.setMap(map);

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                //title: 'Hello World!',
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true,
                //suppressMarkers: true,
                crossOnDrag: true
            });


            google.maps.event.addListener(marker, 'dragend', function (marker) {
                    var latLng = marker.latLng;
                    currentLatitude = latLng.lat();
                    currentLongitude = latLng.lng();
                    $("#user-lng").val(currentLongitude);
                    $("#user-lat").val(currentLatitude);
                    var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': latlng}, function (results, status) {
                        if (status !== google.maps.GeocoderStatus.OK) {
                            alert(status);
                        }
                        // This is checking to see if the Geoeode Status is OK before proceeding
                        if (status == google.maps.GeocoderStatus.OK) {
                            //console.log(results);
                            $("#user-location").val(results[1].formatted_address);
                            $("#user-lng").val(currentLongitude);
                            $("#user-lat").val(currentLatitude);
                        }
                    });

                }
            );
        }

        function getLocation() {
            //Default to jaddah
            var position = {
                coords: {
                    latitude: 21.2854,
                    longitude: 39.2376
                }
            };
            // getCurrentLocation(position);
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        coords: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        }
                    };

                    getCurrentLocation(pos);
                }, function () {
                    getCurrentLocation(position);
                });
            } else {
                // Browser doesn't support Geolocation
                alert('Your browser is out dated, please uprade it to use smartwashr')

            }
        }
    </script>
@stop