@extends('layouts.app')
@section('title')
Home
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Thankyou
            <hr class="light" />
        </h1>
        <p>
            Thank you for your order. Our driver will contact you soon at your given phone number
        </p>
    </div>
</section>
@stop
