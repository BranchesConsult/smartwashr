@extends('layouts.default')
@section('content')
<div class="home page page-id-5 page-template-default gowash_body body_style_wide body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
    <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://gowash.ancorathemes.com/" data-separator="yes"></a><a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <style>
                .fixed-nav-bar {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9999;
  width: 100%;
  height: 50px;
  background-color: white;
}
            </style>
            <nav class="fixed-nav-bar hidden-lg">
  <!-- Fixed navigation bar content -->
            <div class="header_mobile navbar navbar-default navbar-fixed-top" id="headerMobile" >
                <div class="content_wrap">
                    <div class="menu_button icon-menu"></div>
                    <div class="logo">
                        <a href="#">
                            <img src="img/logo_100x180_2.png" class="logo_main" alt="" width="324" height="124">
                        </a>
                    </div>
                </div>
                <div class="side_wrap" id="side-wrap-menu">
                    <div class="close">Close</div>
                    <div class="panel_top">
                        
                        <nav class="menu_main_nav_area">
                             <?php
                        $menupages=App\Menu::with('page')->get()->toArray();
                        ?>
                            <ul id="menu_mobile" class="menu_main_nav">
                          @foreach($menupages as $item)
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-60">
                                    <a class="side-wrap-menu-link" href="/#{{$item['page']['slug']}}">
                                    <span>{{$item['title']}}</span>
                                </a>
                                </li>
                          @endforeach
                            </ul>
                            {{--<ul>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-61">
                                    <a href="#"><span>About</span></a></li>
                            </ul> 
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-253">
                                    <a href="about/index.html"><span>About</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-64">
                                    <a href="#"><span>Features</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62">
                                            <a href="shortcodes/index.html"><span>Shortcodes</span></a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63">
                                            <a href="typography/index.html"><span>Typography</span></a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                            <a href="support/index.html"><span>Support</span></a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                            <a href="customization/index.html"><span>Customization</span></a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65">
                                            <a href="video-tutorials/index.html"><span>Video Tutorials</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-164"><a href="shop/index.html"><span>Shop</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-133"><a href="#"><span>Gallery</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134"><a href="gallery-cobbles/index.html"><span>Gallery Cobbles</span></a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135"><a href="gallery-grid/index.html"><span>Gallery Grid</span></a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-138"><a href="gallery-masonry/index.html"><span>Gallery Masonry</span></a></li>
                                    </ul>
                                </li>
                            </ul>

                            <ul><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-188"><a href="#"><span>Services</span></a></li>
                            </ul>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210"><a href="our-services/index.html"><span>All Services</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-189"><a href="dry-cleaning/index.html"><span>Single Service</span></a></li>
                            </ul>

                            <ul>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-225"><a href="pricing/index.html"><span>Pricing</span></a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-117"><a href="#"><span>Blog</span></a>

                            </ul>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-122"><a href="blog-classic/index.html"><span>Classic Style</span></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121"><a href="blog-masonry-2-columns/index.html"><span>Style Masonry 2 Columns</span></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-120"><a href="blog-masonry-3-columns/index.html"><span>Style Masonry 3 Columns</span></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119"><a href="blog-portfolio-2-columns/index.html"><span>Style Portfolio 2 Columns</span></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a href="blog-portfolio-3-columns/index.html"><span>Style Portfolio 3 Columns</span></a></li>
                            </ul>

                            <ul>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199"><a href="contacts/index.html"><span>Contacts</span></a></li>
                                <li class="item_button menu-item menu-item-type-post_type menu-item-object-page menu-item-280"><a href="order/index.html"><span>Order</span></a></li>
                            </ul> --}}
                        </nav>
                        <div class="login"></div> </div>
                    <div class="panel_bottom">
                    </div>
                </div>
                <div class="mask"></div>
            </div> 
 </nav>
            <div class="mobile hidden-lg" style="padding-top: 2cm;"></div>
            
            <section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_home" id="slider_down">
                <link href="http://fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all"/>
                <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

                    <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                        <ul>  
                            <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://gowash.ancorathemes.com/wp-content/uploads/2016/07/slide1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                <img src="wp-content/uploads/2016/07/slide1.jpg" alt="" title="slide1" width="1920" height="743" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                <div class="tp-caption Default-Title   tp-resizeme  content_wrap" id="slide-1-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-96" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap;">Save Time for More<br>
                                    Important Things </div>

                                <div class="tp-caption Default-Descr    content_wrap" id="slide-1-layer-2" data-x="center" data-hoffset="" data-y="center" data-voffset="47" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap;text-transform:uppercase;">We�ll take care about cleanness </div>


                                <div class="tp-caption Default-Clear    content_wrap" id="slide-1-layer-4" data-x="center" data-hoffset="" data-y="center" data-voffset="-5" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap;"><div class="sc_line sc_line_position_center_center sc_line_style_solid" style="width:230px;border-top-style:solid;border-top-color:rgba(255,255,255,0.5);"></div> </div>
                            </li>

                            <li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://gowash.ancorathemes.com/wp-content/uploads/2016/07/slide2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                <img src="wp-content/uploads/2016/07/slide2.jpg" alt="" title="slide2" width="1920" height="743" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                <div class="tp-caption Default-Title   tp-resizeme  content_wrap" id="slide-5-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-96" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap;">Freshly Cleaned Clothes <br>
                                    With A Tap Of Finger </div>

                                <div class="tp-caption Default-Descr    content_wrap" id="slide-5-layer-2" data-x="center" data-hoffset="" data-y="center" data-voffset="47" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap;text-transform:uppercase;">you don't need to do anything </div>


                                <div class="tp-caption Default-Clear    content_wrap" id="slide-5-layer-4" data-x="center" data-hoffset="" data-y="center" data-voffset="-5" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; white-space: nowrap;"><div class="sc_line sc_line_position_center_center sc_line_style_solid" style="width:230px;border-top-style:solid;border-top-color:rgba(255,255,255,0.5);"></div> </div>
                            </li>

                            <li data-index="rs-4" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://gowash.ancorathemes.com/wp-content/uploads/2016/07/slide3-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 3" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                <img src="wp-content/uploads/2016/07/slide3.jpg" alt="" title="slide3" width="1920" height="743" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                <div class="tp-caption Default-Title   tp-resizeme  content_wrap" id="slide-4-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-96" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap;">Dry Cleaning & Laundry<br>
                                    to Your Door </div>

                                <div class="tp-caption Default-Descr    content_wrap" id="slide-4-layer-2" data-x="center" data-hoffset="" data-y="center" data-voffset="47" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; white-space: nowrap;text-transform:uppercase;">We'll deliver back to you, anytime and anywhere </div>


                                <div class="tp-caption Default-Clear    content_wrap" id="slide-4-layer-4" data-x="center" data-hoffset="" data-y="center" data-voffset="-5" data-width="['auto']" data-height="['auto']" data-visibility="['on','off','off','off']" data-type="text" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 16; white-space: nowrap;"><div class="sc_line sc_line_position_center_center sc_line_style_solid" style="width:230px;border-top-style:solid;border-top-color:rgba(255,255,255,0.5);"></div> </div>
                            </li>
                        </ul>
                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                            var htmlDivCss = "";
                            if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                            } else {
                            var htmlDiv = document.createElement("div");
                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                            }
                        </script>
                        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                        var htmlDivCss = ".tp-caption.Default-Title,.Default-Title{color:rgba(255,255,255,1.00);font-size:55px;line-height:60px;font-weight:400;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Descr,.Default-Descr{color:rgba(255,255,255,1.00);font-size:14px;line-height:26px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Button,.Default-Button{color:rgba(255,255,255,1.00);font-size:14px;line-height:16px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}.tp-caption.Default-Clear,.Default-Clear{color:rgba(255,255,255,1.00);font-size:14px;line-height:22px;font-weight:700;font-style:normal;font-family:Roboto Slab;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}";
                        if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                        var htmlDiv = document.createElement("div");
                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <script type="text/javascript">
                        /******************************************
                         -  PREPARE PLACEHOLDER FOR SLIDER  -
                         ******************************************/

                        var setREVStartSize = function () {
                        try {
                        var e = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                        e.c = jQuery('#rev_slider_1_1');
                        e.gridwidth = [1920];
                        e.gridheight = [745];
                        e.sliderLayout = "auto";
                        if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                        var u = (e.c.width(), jQuery(window).height());
                        if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c)
                                jQuery.each(c, function (e, i) {
                                u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                        }
                        f = u
                        } else
                                void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                        e.c.closest(".rev_slider_wrapper").css({height: f})

                        } catch (d) {
                        console.log("Failure at Presize of Slider:" + d)
                        }
                        };
                        setREVStartSize();
                        var tpj = jQuery;
                        var revapi1;
                        tpj(document).ready(function () {
                        if (tpj("#rev_slider_1_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                        } else {
                        revapi1 = tpj("#rev_slider_1_1").show().revolution({
                        sliderType: "standard",
                                jsFileLocation: "//gowash.ancorathemes.com/wp-content/plugins/revslider/public/assets/js/",
                                sliderLayout: "auto",
                                dottedOverlay: "none",
                                delay: 9000,
                                navigation: {
                                keyboardNavigation: "off",
                                        keyboard_direction: "horizontal",
                                        mouseScrollNavigation: "off",
                                        mouseScrollReverse: "default",
                                        onHoverStop: "off",
                                        bullets: {
                                        enable: true,
                                                hide_onmobile: true,
                                                hide_under: 1300,
                                                style: "ares",
                                                hide_onleave: true,
                                                hide_delay: 200,
                                                hide_delay_mobile: 1200,
                                                direction: "horizontal",
                                                h_align: "center",
                                                v_align: "bottom",
                                                h_offset: 0,
                                                v_offset: 40,
                                                space: 5,
                                                tmp: '<span class="tp-bullet-title"></span>'
                                        }
                                },
                                visibilityLevels: [1240, 1024, 778, 480],
                                gridwidth: 1920,
                                gridheight: 745,
                                lazyType: "none",
                                shadow: 0,
                                spinner: "spinner0",
                                stopLoop: "off",
                                stopAfterLoops: - 1,
                                stopAtSlide: - 1,
                                shuffle: "off",
                                autoHeight: "off",
                                disableProgressBar: "on",
                                hideThumbsOnMobile: "off",
                                hideSliderAtLimit: 0,
                                hideCaptionAtLimit: 0,
                                hideAllCaptionAtLilmit: 0,
                                debugMode: false,
                                fallbacks: {
                                simplifyAll: "off",
                                        nextSlideOnWindowFocus: "off",
                                        disableFocusListener: false,
                                }
                        });
                        }
                        }); /*ready*/
                    </script>
                    <script>
                        var htmlDivCss = unescape(".ares.tp-bullets%20%7B%0A%7D%0A.ares.tp-bullets%3Abefore%20%7B%0A%09content%3A%22%20%22%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.ares%20.tp-bullet%20%7B%0A%09width%3A13px%3B%0A%09height%3A13px%3B%0A%09position%3Aabsolute%3B%0A%09background%3Argba%28229%2C%20229%2C%20229%2C%201%29%3B%0A%09border-radius%3A50%25%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.ares%20.tp-bullet%3Ahover%2C%0A.ares%20.tp-bullet.selected%20%7B%0A%09background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%7D%0A.ares%20.tp-bullet-title%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20color%3A136%2C%20136%2C%20136%3B%0A%20%20font-size%3A12px%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20font-weight%3A600%3B%0A%20%20right%3A27px%3B%0A%20%20top%3A-4px%3B%20%20%0A%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%20visibility%3Ahidden%3B%0A%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20transition%3Atransform%200.3s%3B%0A%20%20-webkit-transition%3Atransform%200.3s%3B%0A%20%20line-height%3A20px%3B%0A%20%20white-space%3Anowrap%3B%0A%7D%20%20%20%20%20%0A%0A.ares%20.tp-bullet-title%3Aafter%20%7B%0A%20%20%20%20width%3A%200px%3B%0A%09height%3A%200px%3B%0A%09border-style%3A%20solid%3B%0A%09border-width%3A%2010px%200%2010px%2010px%3B%0A%09border-color%3A%20transparent%20transparent%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%3B%0A%09content%3A%22%20%22%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20right%3A-10px%3B%0A%09top%3A0px%3B%0A%7D%0A%20%20%20%20%0A.ares%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20visibility%3Avisible%3B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%7D%0A%0A.ares%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%20%7B%0A%20%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%7D%0A.ares%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3Atransparent%20transparent%20transparent%20rgba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%7D%0A.ares.tp-bullets%3Ahover%20.tp-bullet-title%20%7B%0A%20%20visibility%3Ahidden%3B%0A%20%20%0A%7D%0A.ares.tp-bullets%3Ahover%20.tp-bullet%3Ahover%20.tp-bullet-title%20%7B%0A%20%20%20%20visibility%3Avisible%3B%0A%20%20%20%20transform%3AtranslateX%280px%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%280px%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A%2F%2A%20VERTICAL%20%2A%2F%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%20%7B%20right%3Aauto%3B%20left%3A27px%3B%20%20transform%3Atranslatex%2820px%29%3B%20-webkit-transform%3Atranslatex%2820px%29%3B%7D%20%20%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%2010px%2010px%200%20%21important%3B%0A%20%20border-color%3A%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%20transparent%3B%0A%20%20right%3Aauto%20%21important%3B%0A%20%20left%3A-10px%20%21important%3B%20%20%20%0A%7D%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20transparent%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%20transparent%20%21important%3B%0A%7D%0A%0A%0A%0A%2F%2A%20HORIZONTAL%20BOTTOM%20%26%26%20CENTER%20%2A%2F%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet-title%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet-title%20%7B%20top%3A-35px%3B%20left%3A50%25%3B%20right%3Aauto%3B%20transform%3A%20translateX%28-50%25%29%20translateY%28-10px%29%3B-webkit-transform%3A%20translateX%28-50%25%29%20translateY%28-10px%29%3B%20%7D%20%20%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet-title%3Aafter%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%2010px%200px%2010px%3B%0A%20%20border-color%3A%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%20transparent%20transparent%3B%0A%20%20right%3Aauto%3B%0A%20%20left%3A50%25%3B%0A%20%20margin-left%3A-10px%3B%0A%20%20top%3Aauto%3B%0A%20%20bottom%3A-10px%3B%0A%20%20%20%20%0A%7D%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%20transparent%20transparent%3B%0A%7D%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet%3Ahover%20.tp-bullet-title%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A%2F%2A%20HORIZONTAL%20TOP%20%2A%2F%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%20%7B%20top%3A25px%3B%20left%3A50%25%3B%20right%3Aauto%3B%20transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B-webkit-transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B%20%7D%20%20%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%200%2010px%2010px%2010px%3B%0A%20%20border-color%3A%20%20transparent%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%3B%0A%20%20right%3Aauto%3B%0A%20%20left%3A50%25%3B%0A%20%20margin-left%3A-10px%3B%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A-10px%3B%0A%20%20%20%20%0A%7D%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20transparent%20transparent%20%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%3B%0A%7D%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A");
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                        var htmlDiv = document.createElement('div');
                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                </div>  </section>
            <div class="page_content_wrap page_paddings_no" style="width:100%;">
                <div class="content_wrap">
                    <div class="content">
                        <article class="itemscope post_item post_item_single post_featured_center post_format_standard post-5 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
                            <section class="post_content" itemprop="articleBody">
                                
                                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1467891069196">
                                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                    <div class="vc_column-inner ">
                                                                        <div class="wpb_wrapper">
                                                                            <div id="sc_services_2009937026_wrap" class="sc_services_wrap">
                                                                <div id="how-it-works" class="sc_services sc_services_style_services-1 sc_services_type_images " style="width:100%;">
                                                                    <h2 class="sc_services_title sc_item_title">Delivering Clean Clothes and Peace of Mind</h2>
                                                                    <h6 class="sc_services_subtitle sc_item_subtitle">reasons to choose us:</h6><div class="sc_columns columns_wrap">
                                                            <div class='row'>
                                                                <div class="col-md-4 column_padding_bottom">
                                                                    <div id="sc_services_2009937026_1" class="sc_services_item sc_services_item_1 odd first">
                                                                    <div class="sc_services_item_featured post_featured">
                                                                        <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-3.jpg" data-title="Professional Care">     
                                                                            <a class="hover_icon hover_icon_link" href="#how-it-works"><img class="wp-post-image" width="370" height="370" alt="Professional Care" src="img/img_2.png"></a> </div>
                                                                    </div>
                                                                    <div class="sc_services_item_content">
                                                                        <h5 class="sc_services_item_title"><a href="">Book through your Smart washr app</a></h5>
                                                                        <div class="sc_services_item_description">
                                                                            <p>In one click select the pick-up time that best suits you</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 column_padding_bottom"> 
                                                                <div id="sc_services_2009937026_2" class="sc_services_item sc_services_item_2 even">
                                                                    <div class="sc_services_item_featured post_featured">
                                                                        <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-2.jpg" data-title="Fast Delivery">
                                                                            <a class="hover_icon hover_icon_link" href="#how-it-works"><img class="wp-post-image" width="370" height="370" alt="Fast Delivery" src="img/img_3.png"></a> </div>
                                                                    </div>
                                                                    <div class="sc_services_item_content">
                                                                        <h5 class="sc_services_item_title"style="padding-top: 1.9rem;"><a href="">Pick Up Pilot</a></h5>
                                                                        <div class="sc_services_item_description">
                                                                            <p>Your favorite laundry delivered freshly cleaned direct to you</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 column_padding_bottom"> 
                                                                        <div id="sc_services_2009937026_3" class="sc_services_item sc_services_item_3 odd">
                                                                    <div class="sc_services_item_featured post_featured">
                                                                        <div class="post_thumb" data-image="http://gowash.ancorathemes.com/wp-content/uploads/2016/06/service-1.jpg" data-title="Excellent Results">
                                                                            <a class="hover_icon hover_icon_link" href="#how-it-works"><img class="wp-post-image" width="370" height="370" alt="Excellent Results" src="img/img_4.png"></a> </div>
                                                                    </div>
                                                                    <div class="sc_services_item_content">
                                                                        <h5 class="sc_services_item_title"><a href="">Delivery At Your Doorstep</a></h5>
                                                                        <div class="sc_services_item_description">
                                                                            <p>Your favorite laundry delivered freshly cleaned direct to you</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                </div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="smart-washr-near-me" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1467892034495">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="sc_section banner inverse_color margin_top_null margin_bottom_null alignleft">
                                                    <div class="sc_section_inner">
                                                        <h2 class="sc_section_title sc_item_title">Order Laundry and Dry Cleaning on Your Phone</h2>
                                                        <h6 class="sc_section_subtitle sc_item_subtitle" style='font-size:0.65cm'>free collection and delivery</h6>
                                                        <div class="sc_section_content_wrap">
                                                            <a href="#" class="sc_button sc_button_round sc_button_style_border sc_button_size_small alignleft margin_top_null margin_right_small margin_bottom_null  sc_button_iconed icon-icon_apple">download on the app store</a>
                                                            <a href="#" class="sc_button sc_button_round sc_button_style_border sc_button_size_small alignleft margin_top_null margin_right_null margin_bottom_null  sc_button_iconed icon-icon_android">android app on google play</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width"></div>
                                <div id='services' class="vc_row wpb_row vc_row-fluid vc_custom_1467891297135">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div id="sc_services_1543219316_wrap" class="sc_services_wrap">
                                                    <div id="sc_services_1543219316" class="sc_services sc_services_style_services-1 sc_services_type_icons " style="width:100%;">
                                                        <h2 class="sc_services_title sc_item_title">Clean Clothes Have Never Been This Easy!</h2><h6 class="sc_services_subtitle sc_item_subtitle">how our service works:</h6><div class="sc_columns columns_wrap"><div class="column-1_4 column_padding_bottom"> <div id="sc_services_1543219316_1" class="sc_services_item sc_services_item_1 odd first">
                                                                    <span class="sc_icon icon-icon1"></span> <div class="sc_services_item_content">
                                                                        <h4 class="sc_services_item_title">Sign Up</h4>
                                                                        <div class="sc_services_item_description">
                                                                            <p>All members receive bonuses and/or discounts. Sign up for more information.</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="column-1_4 column_padding_bottom"> <div id="sc_services_1543219316_2" class="sc_services_item sc_services_item_2 even">
                                                                    <span class="sc_icon icon-icon2"></span> <div class="sc_services_item_content">
                                                                        <h4 class="sc_services_item_title">Pick Up</h4>
                                                                        <div class="sc_services_item_description">
                                                                            <p>If you are in an urgent need of laundry, we can come right to you for a pick-up.</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="column-1_4 column_padding_bottom"> <div id="sc_services_1543219316_3" class="sc_services_item sc_services_item_3 odd">
                                                                    <span class="sc_icon icon-icon3"></span><div class="sc_services_item_content">
                                                                        <h4 class="sc_services_item_title">Cleaning</h4>
                                                                        <div class="sc_services_item_description">
                                                                            <p>We use premium materials, technologies and guarantee treatment with care.</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="column-1_4 column_padding_bottom"> <div id="sc_services_1543219316_4" class="sc_services_item sc_services_item_4 even">
                                                                    <span class="sc_icon icon-icon4"></span>
                                                                    <div class="sc_services_item_content">
                                                                        <h4 class="sc_services_item_title">Delivery</h4>
                                                                        <div class="sc_services_item_description">
                                                                            <p>Free delivery for every order upon prior request within 24 hours after cleaning</p> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                {{--<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid gradient_2 vc_custom_1467891382925"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="sc_testimonials_815820319" class="sc_testimonials sc_testimonials_style_testimonials-1 " style="width:100%;"><div class="sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_controls_side" data-interval="5232" data-slides-min-width="250"><div class="slides swiper-wrapper"><div class="swiper-slide" data-style="width:100%;" style="width:100%;"> <div id="sc_testimonials_815820319_1" class="sc_testimonial_item">
                                <div class="sc_testimonial_content"><p>Everything about your company is great: from quality products and impeccable customer support                                                                                                             to various services. Thank you!</p>
                                                                                                                                                   </div>
                                                                                                                                            <div class="sc_testimonial_avatar"><img class="wp-post-image" width="85" height="85" alt="Laura Green" src="wp-content/uploads/2016/06/testimonials1-85x85.jpg"></div>
                                                                    <div class="sc_testimonial_author"><span class="sc_testimonial_author_name">Laura Green</span><span class="sc_testimonial_author_position">NY</span></div>
                                                                                                                                    </div>
                                                                                                                            </div><div class=                                                                                                                                        "swiper-slide" data-style="width:100%;" style="width:100%;"> <div id="sc_testimonials_815820319_2" class="sc_testimonial_item">
                                                                                                                                    <div class="sc_testimonial_content"><p>I recommend your services to all my friends and colleagues. I love                                                                                                                                        d how fast and efficient the help from your tech team came.</p>
                                                                                                                                                   </div>
                                                                                                                                    <div class="sc_testimonial_avatar"><img class="wp-post-image" width="85" height="85" alt="Lisa Santos" src="wp-content/uploads/2016/06/testimonials2-85x85.jpg"></div>
                                                                    <div class="sc_testimonial_author"><span class="sc_testimonial_author_name">Lisa Santos</span><span class="sc_testimonial_author_position">AZ</span></div>
                                                                                                                            </div>
                                                                                                                    </div><div class="swiper-slide" d                                                                                                                                ata-style="width:100%;" style="width:100%;"> <div id="sc_testimonials_815820319_3" class="sc_testimonial_item">
                                                                                                                            <div class="sc_testimonial_content"><p>I know that my family deserves the best. It was the right decision f                                                                                                                                or us to use this company for their great services!</p>
                                                                                                                                                   </div>
                                                                                                                            <div class="sc_testimonial_avatar"><img class="wp-post-image" width="85" height="85" alt="Meredith Jones" src=                                                                                                                                "wp-content/uploads/2016/06/testimonials3-85x85.jpg"></div>
                                                                                                                                <div class="sc_testimonial_author"><span class="sc_testimonial_author_name">Meredith Jones</span><span class="sc_testimonial_author_po                                                                                                                            sition">OR</span></div>
                                                                                                                                                                       </div>
                                                                                    </div></div><div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div><div class="sc_slider_pagination_wrap"></div></div></div></div></div></div></div>--}}
                                                        {{--     <div class="vc                                                        _row-full-width"></div>
                                                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1480334683565 scheme_original inverse_colors"><div class="wpb_column vc_column_container vc_col-sm-12"><div cla                                                                        ss="vc_column-inner "><div class="wpb_wrapper">
                                                                        <                                                                            div class="columns_wrap sc_columns columns_nofluid sc_columns_count_4">
                                                                                                                               <div class="column-1_4 sc_column_item sc_column_item_1 odd first">
                                                                                                                                                                 <div id="sc_skills_diagram_603335474" class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills                                                                                        ">
                                                                                    <div cl                                                                                            ass="sc_skills_item sc_skills_style_2 odd first">
                                                                                        <div class="sc_skills_count">
                                                                                                                                      <div class="sc_skills_tota                                                                                        l" data-start="0" data-stop="4000" data-step="40" data-max="4000" data-                                                                                        speed="16" data-duration="1600" data-ed="">0
                                                                                                                                            </div>

                                                                                                                                                            </div>
                                                                                                                                  <div class="sc_skills_info">
                                                                                 <div class="sc_skills_label">Client                                                                            s per Week</div>
                                                                                        </div>  
                                                                                </div>     
                                                                                                                                                                  </div>    
                                                                            </div>
                                                                                                             <div class="column-1_4 sc_column_item s                                                                                            c_column_item_2 even">
                                                                                <div id="sc_skills_diagram_1261587453" class="sc_skills sc_skills_counter" data-type="counter" data-caption="S                                                                                            kills">
                                                                                                                                                           <div class="sc_skills_item sc_skills_style_2 odd first                                                                                        ">
                                                                                        <div class="sc_skills_count">
                                                                                                                       <div class="                                                                                        sc_skills_total" data-start="0" data-stop="17000" data-step="170" data-ma                                                                                    x="17000" data-speed="37" data-duration="3700" data-ed="">0
                                                                                         </div>
                                                                                                                </div>
                                                                                                                                      <div class="sc_skills_info"><div class="sc_skills_label">Pounds of Laundry per Week
                                                                                                                                            </div>  
                                                                                                                                                                </div>  
                                                                                                                                                                 </div>  
                                                                                </div>    
                                                                            </div>
                                                                                                     <div class="column-1_4 sc_column_item sc_column_item_3 odd">
                                                                                                           <div id="sc_skills_diagram_2033425610" class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                  <div class="sc_skills_item sc_skills_style_2 odd first">
                                 <div clas                                                                            s="sc_skills_count">
                                                                                                                      <div class="sc_skills_total" data-start="0" data-stop="167" data-step="2" data-max="167" data-speed="32" data-duration="2672" data-ed="">0
                                                                                            </div>                                                                        
                                                                                        </div>
                                                                                        <div class="sc_skills_info"><div class="sc_skills_label">People in Team</div>

                                                                                                   </div>

                                                                                                                                 </div>

                                                                                            </div>
                                        </div>
                                                                                                         <div class="column-1_4 sc_column_item sc_column_item_4 even"><div id="sc_skills_diagram_1174031792" class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills"><div class="sc_skills_item sc_skills_style_2 odd first"><div class="sc_skills_count"><div class="sc_skills_total" data-start="0" data-stop="194000" data-step="1940" data-max="194000" data-speed="20" data-duration="2000" data-ed="">0</div></div><div class="sc_skills_info"><div class="sc_skills_label">Pieces of Dry Cleaning</div>
                                                                        </div>  
                                                                                    </div>  
                                                                                </div>  
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>                                        
                                                            </div>

                                                        </div>--}}
                                <div class="vc_row-full-width">
                                </div>
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1467891437670">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="sc_promo sc_promo_style_1 sc_promo_size_large">
<div class="sc_promo_inner">
<div class="sc_promo_image" style="background-image:url(wp-content/uploads/2016/06/bg_banner.jpg);width:49%;left: 0;">
    
</div><div class="sc_promo_block sc_align_none" style="width: 51%; float: right;">
<div class="sc_promo_block_inner">
<h2 class="sc_promo_title sc_item_title">Shirt Service from $2 per Shirt</h2>
<div class="sc_promo_button sc_item_button">
<a href="#pricing" class="sc_button sc_button_round sc_button_style_border sc_button_size_small inverse_color ">
Full price list
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1467724813963" id="pricing">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="sc_section aligncenter">
<div class="sc_section_inner">
<h2 class="sc_section_title sc_item_title">Dry Cleaning and Laundry Made Simple</h2>
<h6 class="sc_section_subtitle sc_item_subtitle">how our service works:</h6>
<div class="sc_section_content_wrap">
<div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3" >
{{--<div class="column-1_3 sc_column_item sc_column_item_1 odd first">
<div class="sc_price_block sc_price_block_style_1">
<div class="sc_price_block_subtitle">
                                                        <span>6 clothes</span>
                                                    </div>
    <div class="sc_price_block_title">
                                                        <span>Dry Cleaning / Launder & Press</span>
                                                    </div>
                                                    <div class="sc_price_block_description">
                                                        <ul>
                                                            <li>Shirts &#8211; $2.50</li>
                                                            <li>Pants &#8211; $7</li>
                                                            <li>Blouses &#8211; $7</li>
                                                        </ul>
                                                    </div><div class="sc_price_block_money">
                                                        <div class="sc_price">
                                                            <span class="sc_price_currency">$</span>
                                                            <span class="sc_price_money">9</span>
                                                            <span class="sc_price_info">
                                                                <span class="sc_price_penny">00</span>
                                                                <span class="sc_price_period_empty"></span>
                                                                    
                                                            </span>
                                                        </div>
                                                    </div>
    <div class="sc_price_block_link">
        <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
    </div>
</div>
</div>--}}
{{--   <div class="column-1_3 sc_column_item sc_column_item_2 even">
                                                        <div class="sc_price_block sc_price_block_style_1">
                                                            <div class="sc_price_block_subtitle">
                                                                <span>12 clothes</span>
                                                            </div>
                                                            <div class="sc_price_block_title">
                                                                <span>Wash & Fold</span>
                                                            </div><div class="sc_price_block_description">
                                                        <ul>
                                                            <li>Wash &amp; Fold &#8211; $1.75 / lb.</li>
                                                            <li>Comforters &#8211; $30</li>
                                                            <li>Blnkets &#8211; $15</li>
                                                        </ul>
                                                    </div>
                                                            <div class="sc_price_block_money">
                                                                <div class="sc_price">
                                                                    <span class="sc_price_currency">$
                                                                    </span>
                                                                    <span class="sc_price_money">19</span>
                                                                    <span class="sc_price_info">
                                                                        <span class="sc_price_penny">00</span>
                                                                        <span class="sc_price_period_empty">
                                                                            
                                                                        </span>
                                                                            
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="sc_price_block_link">
                                                                <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
                                                            </div>
                                                        </div>
                                                    </div>--}}
    {{--<div class="column-1_3 sc_column_item sc_column_item_3 odd">
        <div class="sc_price_block sc_price_block_style_1">
            <div class="sc_price_block_subtitle">
                <span>20 clothes</span>
            </div>
            <div class="sc_price_block_title"><span>
                    Hang Dry</span>
            </div><div class="sc_price_block_description">
                                                        <ul>
                                                            <li>Undergarments &#8211; $1</li>
                                                            <li>Synthetics &#8211; $2.50</li>
                                                            <li>Sweaters &#8211; $7</li>
                                                        </ul>
                                                        <p></div>
            <div class="sc_price_block_money">
                <div class="sc_price">
                    <span class="sc_price_currency">$</span>
                    <span class="sc_price_money">29</span>
                    <span class="sc_price_info">
                        <span class="sc_price_penny">00</span>
                        <span class="sc_price_period_empty"></span>
                            
                    </span>
                </div>
            </div>
            <div class="sc_price_block_link">
                <a href="#" class="sc_button sc_button_round sc_button_style_filled sc_button_size_small">more details</a>
            </div>
        </div>
    </div>--}}
        <?php
        $category=App\Category::with('product')->get()->toArray();
        $product=App\Product::get()->toArray();
//       // debugArr($category);die;
//        $category=null;
        ?>
    <style>
        .price{
     
    width:80%;
    height: 500px;
    overflow-y: scroll;
    overflow-x: hidden;
    margin:auto;
    text-transform:capitalize;
    text-align:left;
        }
    </style>
    @if($product==null)
    <div>
        <h2>Currently no offers available</h2>
    </div>
    @endif
     @if($product!=null)
  <div class='table-responsive price'>
      <table class="table">
        <thead>
            <tr>
                <th width="40%"><span class="text">Item</span></th>
                <th width="20%"><span class="text">Dry Clean & Press</span></th>
                <th width="20%"><span class="text">Wash & Press</span></th>
                <th width="20%"><span class="text">Only Press</span></th>
            </tr>
        </thead>
        <tbody>
             @foreach($product as $row)
            {{--@foreach($row['product'] as $item)--}}
          <tr> 
              <td>{{$row['name']}}</td>
              <td>{{$row['dryclean_price']}}</td> 
              <td>{{$row['washing_price']}}</td>
              <td>{{$row['press']}}</td>
          </tr>
            {{--@endforeach--}}
            @endforeach
        </tbody>
    </table>
      @endif
  </div>

</div>
</div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                            </div>
                    <div id="community" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1467819802511">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section banner inverse_color margin_top_null margin_bottom_null alignleft"style="padding-top: 12rem;">
                                        <div class="sc_section_inner">
                                            <h6 class="sc_section_title sc_item_title">Smartwashr is committed to devote its best efforts in promoting community development and appreciates suggestions from local partners to achieve this objective.

                                                            Unclaimed clothes (more then sixty days) will be offered to local charity organizations.</h6>
                                            <h6 class="sc_section_subtitle sc_item_subtitle">free collection and delivery</h6>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                {{--    <div class="vc_row wpb_row vc_row-fluid vc_custom_1467891509822">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title">
                                                Dry Cleaning and Laundry Made Simple
                                            </h2>
                                                                        <h6 class="sc_section_subtitle sc_item_subtitle">
                                                                            how our services work:</h6>
                                                                        <div class="sc_section_content_wrap">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <style type="text/css">a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;-moz-transition:all .4s linear;-o-transition:all .4s linear;-ms-transition:all .4s linear;transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px;margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden;border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden;border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden;border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");filter:gray;-webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");-webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");filter:gray;-webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");-webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85)));background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000',endColorstr='#d9000000',GradientType=0)}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85)));background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000',endColorstr='#d9000000',GradientType=0)}.eg-jefferson-wrapper{-webkit-border-radius:5px!important;-moz-border-radius:5px!important;border-radius:5px!important;-webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC)!important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0)));background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000',endColorstr='#00131313',GradientType=1)}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0)));background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000',endColorstr='#00131313',GradientType=1)}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;-moz-transition:0.4s ease-in-out;-o-transition:0.4s ease-in-out;transition:0.4s ease-in-out;filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");filter:gray;-webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");-webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px;bottom:-5px;position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");-webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");filter:gray;-webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans";font-size:13px;color:#fff;margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px;font-weight:700;text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px;font-size:15px;font-weight:700;text-align:center}</style>
                                                                    <style type="text/css">.minimal-light .navigationbuttons,.minimal-light .esg-pagination,.minimal-light .esg-filters{text-align:center}.minimal-light .esg-filterbutton,.minimal-light .esg-navigationbutton,.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton a{color:#999;margin-right:5px;cursor:pointer;padding:0px 16px;border:1px solid #e5e5e5;line-height:38px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;font-size:12px;font-weight:700;font-family:"Open Sans",sans-serif;display:inline-block;background:#fff;margin-bottom:5px}.minimal-light .esg-navigationbutton *{color:#999}.minimal-light .esg-navigationbutton{padding:0px 16px}.minimal-light .esg-pagination-button:last-child{margin-right:0}.minimal-light .esg-left,.minimal-light .esg-right{padding:0px 11px}.minimal-light .esg-sortbutton-wrapper,.minimal-light .esg-cartbutton-wrapper{display:inline-block}.minimal-light .esg-sortbutton-order,.minimal-light .esg-cartbutton-order{display:inline-block;vertical-align:top;border:1px solid #e5e5e5;width:40px;line-height:38px;border-radius:0px 5px 5px 0px;-moz-border-radius:0px 5px 5px 0px;-webkit-border-radius:0px 5px 5px 0px;font-size:12px;font-weight:700;color:#999;cursor:pointer;background:#fff}.minimal-light .esg-cartbutton{color:#333;cursor:default!important}.minimal-light .esg-cartbutton .esgicon-basket{color:#333;font-size:15px;line-height:15px;margin-right:10px}.minimal-light .esg-cartbutton-wrapper{cursor:default!important}.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton{display:inline-block;position:relative;cursor:pointer;margin-right:0px;border-right:none;border-radius:5px 0px 0px 5px;-moz-border-radius:5px 0px 0px 5px;-webkit-border-radius:5px 0px 0px 5px}.minimal-light .esg-navigationbutton:hover,.minimal-light .esg-filterbutton:hover,.minimal-light .esg-sortbutton:hover,.minimal-light .esg-sortbutton-order:hover,.minimal-light .esg-cartbutton a:hover,.minimal-light .esg-filterbutton.selected{background-color:#fff;border-color:#bbb;color:#333;box-shadow:0px 3px 5px 0px rgba(0,0,0,0.13)}.minimal-light .esg-navigationbutton:hover *{color:#333}.minimal-light .esg-sortbutton-order.tp-desc:hover{border-color:#bbb;color:#333;box-shadow:0px -3px 5px 0px rgba(0,0,0,0.13)!important}.minimal-light .esg-filter-checked{padding:1px 3px;color:#cbcbcb;background:#cbcbcb;margin-left:7px;font-size:9px;font-weight:300;line-height:9px;vertical-align:middle}.minimal-light .esg-filterbutton.selected .esg-filter-checked,.minimal-light .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;color:#fff;background:#000;margin-left:7px;font-size:9px;font-weight:300;line-height:9px;vertical-align:middle}</style>

                                                                    <style type="text/css">.eg-default-services-element-12{font-size:12px;line-height:15px;color:#17c8ca;font-weight:700;display:block;text-align:center;clear:both;margin:14px 0px 0px 0px;padding:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;background-color:rgba(255,255,255,0);position:relative;z-index:2!important;font-family:"Roboto Slab";text-transform:uppercase}.eg-default-services-element-10{font-size:25px!important;line-height:32px!important;color:#ffffff!important;font-weight:400!important;padding:0px 0px 20px 0px!important;border-radius:0px 0px 0px 0px!important;background-color:rgba(255,255,255,0)!important;z-index:2!important;display:block;font-family:"Roboto Slab"!important}.eg-default-services-element-11{font-size:14px;line-height:17px;color:#ffffff;font-weight:400;display:block;text-align:center;clear:both;margin:10px 0px 0px 0px;padding:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;background-color:rgba(255,255,255,0);position:relative;z-index:2!important;font-family:"Roboto Slab"}</style>
                                                                    <style type="text/css">.eg-default-services-element-10:hover{font-size:25px!important;line-height:32px!important;color:#17c8ca!important;font-weight:400!important;border-radius:0px 0px 0px 0px!important;background-color:rgba(255,255,255,0)!important;font-family:"Roboto Slab"!important}</style>
                                                                    <style type="text/css">.eg-default-services-element-10-a{display:inline-block!important;float:none!important;clear:both!important;margin:14px 10px 16px 10px!important;position:relative!important}</style>
                                                                    <style type="text/css">.eg-default-services-container{background-color:rgba(73,73,73,0.70)}</style>
                                                                    <style type="text/css">.eg-default-services-content{background-color:#ffffff;padding:0px 0px 0px 0px;border-width:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;border-color:transparent;border-style:double;text-align:left}</style>
                                                                    <style type="text/css">.esg-grid .mainul li.eg-default-services-wrapper{background-color:#3f424a;padding:0px 0px 0px 0px;border-width:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;border-color:transparent;border-style:none}</style>



                                                                    <article class="myportfolio-container minimal-light" id="esg-grid-4-1-wrap">

                                                                        <div id="esg-grid-4-1" class="esg-grid" style="background-color: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">



                                                                            <ul>

                                                                                <li class="filterall filter-how-our-services-work filter-fabric filter-pressing eg-default-services-wrapper eg-post-id-77" data-date="1464873352">

                                                                                    <div class="esg-media-cover-wrapper">

                                                                                        <div class="esg-entry-media"><img src="wp-content/uploads/2016/06/post-7.jpg" alt=""></div>

                                                                                        <div class="esg-entry-cover">

                                                                                            <div class="esg-overlay eg-default-services-container"></div>
                                                                                            <div class="esg-center eg-post-77 eg-default-services-element-12">June 2, 2016</div>
                                                                                            <div class="esg-center eg-post-77 eg-default-services-element-10-a"><a class="eg-default-services-element-10 eg-post-77" href="focus-on-more-important-things/index.html" target="_self">Focus on more important things.</a></div>
                                                                                            <div class="esg-center eg-post-77 eg-default-services-element-11">1</div>
                                                                                        </div> 
                                                                                    </div> 
                                                                                </li> 

                                                                                <li class="filterall filter-how-our-services-work filter-dry-cleaning filter-rinse eg-default-services-wrapper eg-post-id-75" data-date="1465996431">

                                                                                    <div class="esg-media-cover-wrapper">

                                                                                        <div class="esg-entry-media"><img src="wp-content/uploads/2016/06/post-6.jpg" alt=""></div>

                                                                                        <div class="esg-entry-cover">

                                                                                            <div class="esg-overlay eg-default-services-container"></div>
                                                                                            <div class="esg-center eg-post-75 eg-default-services-element-12">June 15, 2016</div>
                                                                                            <div class="esg-center eg-post-75 eg-default-services-element-10-a"><a class="eg-default-services-element-10 eg-post-75" href="dont-wait-for-cleaning-until-its-too-late/index.html" target="_self">Don't wait for cleaning until it's too late!</a></div>
                                                                                            <div class="esg-center eg-post-75 eg-default-services-element-11">3</div>
                                                                                        </div> 
                                                                                    </div> 
                                                                                </li> 

                                                                                <li class="filterall filter-how-our-services-work filter-rinse filter-washing eg-default-services-wrapper eg-post-id-70" data-date="1466514824">

                                                                                    <div class="esg-media-cover-wrapper">

                                                                                        <div class="esg-entry-media"><img src="wp-content/uploads/2016/06/post-5.jpg" alt=""></div>

                                                                                        <div class="esg-entry-cover">

                                                                                            <div class="esg-overlay eg-default-services-container"></div>
                                                                                            <div class="esg-center eg-post-70 eg-default-services-element-12">June 21, 2016</div>
                                                                                            <div class="esg-center eg-post-70 eg-default-services-element-10-a"><a class="eg-default-services-element-10 eg-post-70" href="ordering-a-rinse-is-easy/index.html" target="_self">Ordering a rinse is easy!</a></div>
                                                                                            <div class="esg-center eg-post-70 eg-default-services-element-11">2</div>
                                                                                        </div> 
                                                                                    </div> 
                                                                                </li> 
                                                                            </ul>



                                                                        </div> 
                                                                    </article>

                                                                    <div class="clear"></div>
                                                                            <script type="text/javascript">
                                                                                function eggbfc(winw, resultoption) {
                                                                                var lasttop = winw,
                                                                                        lastbottom = 0,
                                                                                        smallest = 9999,
                                                                                        largest = 0,
                                                                                        samount = 0,
                                                                                        lamoung = 0,
                                                                                        lastamount = 0,
                                                                                        resultid = 0,
                                                                                        resultidb = 0,
                                                                                        responsiveEntries = [
                                                                                        {width: 1400, amount: 3},
                                                                                        {width: 1170, amount: 3},
                                                                                        {width: 1024, amou                                                                            nt: 3},
                                                                                        {width: 960, amount: 3},
                                                                                        {width: 778, amount: 2},
                                                                                        {width: 640, amount: 2},
                                                                                        {width: 480, amount: 1}
                                                                                        ];
                                                                                if (responsiveEntries != undefined && responsiveEntries.length > 0)
                                                                                        jQuery.each(responsiveEntries, function (index, obj) {
                                                                                        var curw = obj.width != undefined ? obj.width : 0,
                                                                                                cura = obj.amount != undefined ? obj.amount : 0;
                                                                                        if (smallest > curw) {
                                                                                        smallest = curw;
                                                                                        samount = cura;
                                                                                        resultidb = index;
                                                                                        }
                                                                                        if (largest < curw) {
                                                                                        largest = curw;
                                                                                        lamount = cura;
                                                                                        }
                                                                                        if (curw > lastbottom && curw <= lasttop) {
                                                                                        lastbottom = curw;
                                                                                        lastamount = cura;
                                                                                        resultid = index;
                                                                                        }
                                                                                        })
                                                                                        if (smallest > winw) {
                                                                                lastamount = samount;
                                                                                resultid = resultidb;
                                                                                }
                                                                                var obj = new Object;
                                                                                obj.index = resultid;
                                                                                obj.column = lastamount;
                                                                                if (resultoption == "id")
                                                                                        return obj;
                                                                                else
                                                                                        return lastamount;
                                                                                }
                                                                                if ("even" == "even") {
                                                                                var coh = 0,
                                                                                        container = jQuery("#esg-grid-4-1");
                                                                                var cwidth = container.width(),
                                                                                        ar = "4:4",
                                                                                        gbfc = eggbfc(jQuery(window).width(), "id"),
                                                                                        row = 1;
                                                                                ar = ar.split(":");
                                                                                aratio = parseInt(ar[0], 0) / parseInt(ar[1], 0);
                                                                                coh = cwidth / aratio;
                                                                                coh = coh / gbfc.column * row;
                                                                                var ul = container.find("ul").first();
                                                                                ul.css({display: "block", height: coh + "px"});
                                                                                }
                                                                                var essapi_4;
                                                                                jQuery(docum                                                                            ent).ready(function () {
                                                                                essapi_4 = jQuery("#esg-grid-4-1").tpessential({
                                                                                gridID: 4,
                                                                                        layout: "even",
                                                                                        forceFullWidth: "off",
                                                                                        lazyLoad: "off",
                                                                                        row: 1,
                                                                                        loadMoreAjaxToken: "c148c9b202",
                                                                                        loadMoreAjaxUrl: "http://gowash.ancorathemes.com/wp-admin/admin-ajax.php",
                                                                                        loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                                                                                        ajaxContentTarget: "ess-grid-ajax-container-",
                                                                                        ajaxScrollToOffset: "0",
                                                                                        ajaxCloseButton: "off",
                                                                                        ajaxContentSliding: "on",
                                                                                        ajaxScrollToOnLoad: "on",
                                                                                        ajaxNavButton: "off",
                                                                                        ajaxCloseType: "type1",
                                                                                        ajaxCloseInner: "false",
                                                                                        ajaxCloseStyle: "light",
                                                                                        ajaxClosePosition: "tr",
                                                                                        space: 30,
                                                                                        pageAnimation: "fade",
                                                                                        paginationScrollToTop: "off",
                                                                                        spinner: "spinner0",
                                                                                        evenGridMasonrySkinPusher: "off",
                                                                                        lightBoxMode: "single",
                                                                                        animSpeed: 1000,
                                                                                        delayBasic: 1,
                                                                                        mainhoverdelay: 1,
                                                                                        filterType: "single",
                                                                                        showDropFilter: "hover",
                                                                                        filterGroupClass: "esg-fgc-4",
                                                                                        googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                                                                                        aspectratio: "4:4",
                                                                                        responsiveEntries: [
                                                                                        {width: 1400, amount: 3},
                                                                                        {width:                                                                             1170, amount: 3},
                                                                                        {width: 1024, am                                                                            ount: 3},
                                                                                        {width: 960, amount: 3},
                                                                                        {width: 778, amount: 2},
                                                                                        {width: 640, amount: 2},
                                                                                        {width: 480, amount: 1}
                                                                                        ]
                                                                                });
                                                                                });
                                                                                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div></div></div></div></div></div></div>--}}
<div class="vc_row wpb_row vc_row-fluid vc_custom_1467888317203">
<div class="wpb_column vc_column_container vc_col-sm-6">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="sc_section">
<div class="sc_section_inner">
<div class="sc_section_content_wrap">
<h3 class="sc_title sc_title_regular margin_top_tiny margin_bottom_tiny" style=" margin-bottom: 0.5rem !important;">
<span style="font-size: 1.165em;" >Don�t miss any action!</span></h3>
                                                                            <div class="wpb_text_column wpb_content_element ">
                                                                                <div class="wpb_wrapper">
                                                                                    <p>Sign up for our newsletter and keep up with our latest news.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div></div></div></div></div></div>
<div class="wpb_column vc_column_container vc_col-sm-6" id="contact">
    <div class="vc_column-inner ">
        <div class="wpb_wrapper">
            <div class="sc_section">
                <div class="sc_section_inner">
                                                                                            <div class="sc_section_content_wrap">
                                                                            <div class="wpb_text_column wpb_content_element ">
                                                                                <div class="wpb_wrapper">
                                                                                    <script type="text/javascript">(function () {
                                                                                        if (!window.mc4wp) {
                                                                                        window.mc4wp = {
                                                                                        listeners: [],
                                                                                                forms: {
                                                                                                on: function (event, callback) {
                                                                                                window.mc4wp.listeners.push({
                                                                                                event: event,
                                                                                                        callback: callback
                                                                                                });
                                                                                                }
                                                                                                }
                                                                                        }
                                                                                        }
                                                                                        })();
                                                                                                                </script> <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-470" method="p                                                                                                                ost" data-id="470" data-name=""><div class="mc4wp-form-fields"><div class="mailchimp_form">
                                                                                                                                     <div class="mailchimp_wrap">
                                                                                                                <input type="email" name="EMAIL" placeholder="Your email address" required />
                                                                                                                <button>Subscribe</button>
                                                                                                            </div>
                                                                                                        </div><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"/></div><input type="hidden" name="_mc4wp_timestamp" value="1488461409"/><input type="hidden" name="_mc4wp_form_id" value="470"/><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"/></div><div class="mc4wp-response"></div></form> 
                                                                                            </div>
                                                                                        </div>
                                                                                </div></div></div></div></div></div></div>
                                                        </section>  
                                                        </article>   <section class="related_wrap related_wrap_empty"></section>
                                                    </div>  
                                                </div>  
                                            <center><h1 style="color: black">SMARTWAHR IN SAUDI ARABIA</h1></center>
                                            <center><h5 style="color: black; margin-top:-200px;">We Offer Dry Cleaning & Laundry Services in Saudi Arabia</h5></center>
                                            <section>
                                                <div id="sc_googlemap_1241876470" class="sc_googlemap" style="width:100%;height:336px;position:relative" data-zoom="16" data-style="greyscale">
                                                    <div id="sc_googlemap_1241876470_1" 
                                                         class="sc_googlemap_marker" 
                                                         data-title="" data-description="" 
                                                         data-address="London" data-latlng="" 
                                                         data-point="wp-content/uploads/2016/07/logo_100x180_2.png"></div>

                                                </div> 
                                                <div id="contactform" class="container text-center">

                                                    <!-- Example row of columns -->
                                                    <div class="row">
                                                        <div class="col-md-6 col-md-offset-6 col-sm-12 col-xs-12">
                                                            <div class="contact-form-area">
                                                                <form id="contactus" method="POST" action="{{route('contactUs')}}">
                                                                    <h3 style="color:white;font-weight:bold">Contact Us</h3>	
                                                                    <div id="message"></div>
                                                                    <div class="col-md-6 form-group" style="padding:0px 5px 0px 0px">
                                                                        <input class="form-control" id="name" name="name" placeholder="Your Name" required type="text">
                                                                    </div>

                                                                    <div class="col-md-6 form-group" style="padding:0px 0px 0px 5px">
                                                                        <input class="form-control" id="contactemail" name="email" placeholder="Your Email" required type="email">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <input class="form-control" id="subject" name="subject" placeholder="Your Subject" required type="text">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <textarea class="form-control" id="contactmessage" name="message" rows="7" placeholder="Message" required></textarea>
                                                                    </div>

                                                                    <button type="submit" class="btn btn-primary contact-btn">SEND</button>
                                                                </form>
                                                            </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <footer class="footer_wrap widget_area scheme_original">
                                                <div class="footer_wrap_inner widget_area_inner" style="padding-top:4rem; padding-bottom:1rem; " >
                                                    <div class="content_wrap">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <aside> 
                                                                    <div class="widget_inner">
                                                                        <div class="logo">
                                                                            <a href="index.html"><img src="img/logo_100x180_2.png" class="logo_main" alt="" width="324" height="124"></a>
                                                                        </div>
                                                                        <div class="logo_descr">Smart Washr enables laundry industry to know their customers on a personal level and reach them with meaningful offers. Our value add proposition includes a door step pick up and delivery services that are unmatched by any other solution. </div>
                                                                    </div>
                                                                </aside>
                                                            </div>

                                                            {{--<aside id="recent-posts-3" class="widget_number_2 column-1_4 widget widget_recent_entries"> <h3 class="widget_title">Blog Feed</h3> <ul>
                                    <li>
                                        <a href="m3-e36-cabrio/index.html">Date</a>
                                                            <span class="post-date">march 2, 2017</span                                                                                >
                                                                                                                           </li>
                                                                                                                                             <li>
                                                                                                <a href="ordering-a-rinse-is-easy/index.html">Ordering a rinse is easy!</a>
                                                                                                <span class="post-date">march 2, 2017</span>
                                    </li>
                                    <li>
                                        <a href="dont-wait-for-cleaning-until-its-too-late/index.html">Don&#8217;t wait for cleaning until it&#8217;s too late!</a>
                                                                                                <span class="post-date">march 1, 2017</span>                                                                                                                        </li>
                                                                            </ul>
                                                                        </aside>

                                                                        <div class="col-md-4">
                                                                            <aside>
                                                                                <h3 class="widget_title">Services</h3><div class="menu-footer-menu-container">
                                                                                    <ul id="menu-footer-menu" class="menu">
                                                                                        <li id="menu-item-282" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-282"><a href="shirt-laundry/index.html">Laundry</a></li>
                                                                                        <li id="menu-item-284" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-284"><a href="silk-and-suede/index.html">Dry Clean</a></li>
                                                                                        <li id="menu-item-285" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-285"><a href="alternations-and-repairs/index.html">Rugs</a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </aside>
                                                                        </div>--}}
                                                                        <div class="col-md-6">
                                                                            <aside>
                                                                                <h3 class="widget_title">Contact Info</h3> <div class="textwidget">
                                                                                    Jeddah, KSA<div class="info icon-phone">0096 654 7771807</div>
                                                                                    Lahore, PK<div class="info icon-phone">0092 321 8450305</div>
                                                                                    <span style="text-decoration: underline;">info@smartwashr.com<a class="__cf_email__" href="cdn-cgi/l/email-protection.html" data-cfemail=".com"></a><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function (t, e, r, n, c, a, p) {
                                                                                        try {
                                                                                        t = document.currentScript || function () {
                                                                                        for (t = document.getElementsByTagName('script'), e = t.length; e--; )
                                                                                                if (t[e].getAttribute('data-cfhash'))
                                                                                                return t[e]
                                                                                        }();
                                                                                        if (t && (c = t.previousSibling)) {
                                                                                        p = t.parentNode;
                                                                                        if (a = c.getAttribute('data-cfemail')) {
                                                                                        for (e = '', r = '0x' + a.substr(0, 2) | 0, n = 2; a.length - n; n += 2)
                                                                                                e += '%' + ('0' + ('0x' + a.substr(n, 2) ^ r).toString(16)).slice( - 2);
                                                                                        p.replaceChild(document.createTextNo)                                                                                                  de(decodeURIComponent(e)), c)
                                                                                        }
                                                                                        p.removeChild(t)
                                                                                        }
                                                                                        } catch (u) {
                                                                                        }
                                                                                        }()/* ]]> */</script></span>

                                    <br><div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny"><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div><div class="sc_socials_item"><a href="#" target="_blank" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div></div></div>
                                                                                    </aside>
                                                                                    </div>
                                                                                    </div>
                                                                                    </div>  
                                                                                    </div> 
                                                                                    </footer>  
                                                                                    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
                                                                                    <!--[if lte IE 9]>
                                                                                    <script type='text/javascript' src='http://gowash.ancorathemes.com/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.0.9'></script>
                                                                                    <![endif]-->
                                                                                    @section('js')
                                                                                    {!! Html::script('../js/contactUs.js') !!}
                                                                                    @endsection
                                                                                  
<div class="copyright_wrap copyright_style_text  scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
            <div class="copyright_text">Copyright&copy; - {{date('Y')}}.All rights reserved. <a href="{{route('termsAndCondition')}}">Terms of Use</a> & <a href="{{route('privacyPolicy')}}">Privacy Policy</a></div>
        </div>
    </div>
</div>
@stop
