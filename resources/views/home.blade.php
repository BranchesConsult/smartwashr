@extends('layouts.app')
@section('title')
    Home
@stop
@section('content')
    <?php
    if (\Session::get('language') == 'en') {
        $lang = 'en';
    } else
        $lang = 'ar';
    ?>
    <header class="fontstyle">
        <div class="no-padding slider-camera">
            <div class="relative" data-src="{{URL::to('img/slide1.jpg')}}">
                <div class="col-md-6 caption-header" style='{{ __('home.whysmartwashr-style') }}'>
                    <h1 class="heading-white">
                        {{ __('home.whysmartwashr_moto1') }}
                    </h1>
                    <hr/>
                    <p>
                        {{ __('home.moto1_p') }}
                    </p>
                </div>
            </div>
            <div class="relative" data-src="{{URL::to('img/slide2.jpg')}}">
                <div class="col-md-6 caption-header" style='{{ __('home.whysmartwashr-style') }}'>
                    <h1 class="heading-white">
                        {{ __('home.whysmartwashr_moto1') }}
                    </h1>
                    <hr/>
                    <p>
                        {{ __('home.moto1_p') }}
                    </p>
                </div>
            </div>
            <div class="relative" data-src="{{URL::to('img/slide3.jpg')}}">
                <div class="col-md-6 caption-header" style='{{ __('home.whysmartwashr-style') }}'>
                    <h1 class="heading-white">
                        {{ __('home.whysmartwashr_moto1') }}
                    </h1>
                    <hr/>
                    <p>
                        {{ __('home.moto1_p') }}
                    </p>
                </div>
            </div>
        </div>
    </header>
    <section class="bg-white page-content fontstyle margiin_area" id="how-it-works">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading ">
                        {{ __('home.howitworks_moto1') }}
                    </h2>
                    <div class="help">
                        {{ __('home.howitworks_reason') }}

                    </div>
                    <hr class="light">
                    <div class="col-md-4">
                        <img src="img/img_2.png"/>
                        <h4>
                            {{ __('home.howitworks_moto2') }}
                        </h4>
                        <hr/>
                        <p>
                            {{ __('home.moto2_p') }}
                        </p>
                    </div>
                    <div class="col-md-4">
                        <img src="img/img_3.png"/>

                        <h4 style="{{ __('home.howitworks-style') }}">
                            {{ __('home.howitworks_moto3') }}
                        </h4>
                        <hr/>
                        <p>
                            {{ __('home.howitworks_moto3_p') }}
                        </p>
                    </div>
                    <div class="col-md-4">
                        <img src="img/img_4.png"/>
                        <h4 style="{{ __('home.howitworks-style') }}">
                            {{ __('home.howitworks_moto4') }}
                        </h4>
                        <hr/>
                        <p>
                            {{ __('home.howitworks_moto4_p') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="smart-washr-near-me" class="bg-white fontstyle">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-left">
                    <h1 class="heading-white">
                        {{ __('home.smartwashr_near_me_moto1') }}
                    </h1>
                    <hr class="primary">
                    <h2 class="heading-white">
                        <strong>
                            {!!  __('home.smartwashr_near_me_collection') !!}
                        </strong>
                    </h2>
                    <div class="col-lg-4 mr8 no-padding mtb10" style="width:8cm;">
                        <a href="#" class="">
                            <img src="img/apple.png" style="width:7cm;"/>
                        </a>
                    </div>
                    <div class="col-lg-4  no-padding mr8 mtb10">
                        <a href="#" class="">
                            <img src="img/google.png" style="width:7cm;"/>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 text-left">
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white page-content fontstyle" id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">
                        {!! __('home.services_moto1') !!}
                    </h2>
                    <div class="help">
                        {{ __('home.services_how_it_works') }}
                    </div>
                    <hr class="light">
                    <div class="col-md-12 no-padding">
                        <div class="col-md-3 ">
                            <div class="icon-container relative">
                                <div class="icon-next">
                                    <i class="glyphicon glyphicon-menu-right"></i>
                                </div>
                                <div class="badge icon-badge ">
                                    <i class="sc_icon icon-icon1"></i>
                                </div>
                            </div>
                            <h3>
                                {{ __('home.services_signup') }}
                            </h3>
                            <hr/>
                            <p>{{ __('home.services_signup_p') }} </p>
                        </div>
                        <div class="col-md-3 relative">
                            <div class="icon-container relative">
                                <div class="icon-next">
                                    <i class="glyphicon glyphicon-menu-right"></i>
                                </div>
                                <div class="badge icon-badge ">
                                    <i class="sc_icon icon-icon2"></i>
                                </div>
                            </div>
                            <h3>
                                {{ __('home.services_pickup') }}
                            </h3>
                            <hr/>
                            <p>
                                {{ __('home.services_pickup_p') }}
                            </p>
                        </div>
                        <div class="col-md-3 relative">
                            <div class="icon-container relative">
                                <div class="icon-next">
                                    <i class="glyphicon glyphicon-menu-right"></i>
                                </div>
                                <div class="badge icon-badge ">
                                    <i class="sc_icon icon-icon3"></i>
                                </div>
                            </div>
                            <h3>
                                {{ __('home.services_cleaning') }}
                            </h3>
                            <hr/>
                            <p>
                                {{ __('home.services_cleaning_p') }}
                            </p>
                        </div>
                        <div class="col-md-3 relative">
                            <div class="icon-container relative">

                                <div class="badge icon-badge ">
                                    <i class="sc_icon icon-icon4"></i>
                                </div>
                            </div>
                            <h3>
                                {{ __('home.services_delivery') }}
                            </h3>
                            <hr/>
                            <p>
                                {{ __('home.services_delivery_p') }}
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="bg-white page-content fontstyle" id="pricing" style='padding-top: 0px;'>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-heading text-center">
                        {{ __('home.pricing_moto1') }}
                    </h2>
                    <div class="help text-center">
                        {{ __('home.pricing') }}
                    </div>
                    <hr class="light">

                    @if($category==null)
                        <div>
                            <h2>{{ __('home.pricing_no_offers') }}</h2>
                        </div>
                    @endif

                    @if($category!=null)
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped price-table">
                                <thead>
                                <tr>
                                    <th>
                                        {!!__('home.pricing_item') !!}
                                    </th>
                                    <th class="price">
                                        {!!__('home.pricing_dryclean') !!}
                                    </th>
                                    <th class="price">
                                        {!!__('home.pricing_washing') !!}
                                    </th>
                                    <th class="price">
                                        {!!__('home.pricing_press') !!}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                                </thead>
                                <!--Loop hre-->
                                <tbody>
                                @foreach($category as $item)
                                    <tr>
                                        <td colspan="5" style='text-transform: capitalize'>
                                            <strong>{{$item['name']}}</strong></td>
                                    </tr>
                                    @foreach($item['product'] as $row)
                                        <tr>
                                            @if(\Session::get('language')=='en')
                                                <td style='text-transform: capitalize;'>{{$row['name']}}</td>
                                                <td class="price">{{$row['sw_dryclean_price']*$rate}}</td>
                                                <td class="price">{{$row['sw_washing_price']*$rate}}</td>
                                                <td class="price">{{$row['sw_press']*$rate}}</td>
                                                <td class="price">
                                                    <button onclick="wantOrder('product-{{$row['id']}}')"
                                                            class="btn btn-success">
                                                        {!!__('home.order_now') !!}
                                                    </button>
                                                    <div class="pricingSelect product-{{$row['id']}}">
                                                        @if($row['sw_dryclean_price'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="dryclean__{{$row['sw_dryclean_price']}}"/>
                                                                {!!__('home.pricing_dryclean') !!}
                                                            </label>
                                                        @endif
                                                        @if($row['sw_washing_price'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="washing__{{$row['sw_washing_price']}}"/>
                                                                {!!__('home.pricing_washing') !!}
                                                            </label>
                                                        @endif
                                                        @if($row['sw_press'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="press__{{$row['sw_press']}}"/>
                                                                {!!__('home.pricing_press') !!}
                                                            </label>
                                                        @endif
                                                        <label>

                                                            <input style="width: 138px;margin-right: 12px;margin-bottom: 12px;" type="number" name="product_quantity"
                                                                   id="qty-{{$row['id']}}" class="form-control pull-left" min="1" required
                                                                   value="1"/>
                                                            Quantity
                                                        </label>
                                                        <button class="btn btn-primary"
                                                                onclick="cartAddItem('{{$row['id']}}', '{{$row['name']}}')"
                                                        >
                                                            Submit Order
                                                        </button>
                                                    </div>
                                                </td>
                                            @else
                                                <td>{{$row['name_ar']}}</td>
                                                <td class="price"><?php echo numToArabic($row['sw_dryclean_price']); ?></td>
                                                <td class="price"><?php echo numToArabic($row['sw_washing_price']); ?></td>
                                                <td class="price"><?php echo numToArabic($row['sw_press']); ?></td>
                                                <td class="price">
                                                    <button onclick="wantOrder('product-{{$row['id']}}')"
                                                            class="btn btn-success">
                                                        {!!__('home.order_now') !!}
                                                    </button>
                                                    <div class="pricingSelect product-{{$row['id']}}">
                                                        @if($row['sw_dryclean_price'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="dryclean__{{$row['sw_dryclean_price']}}"/>
                                                                {!!__('home.pricing_dryclean') !!}
                                                            </label>
                                                        @endif
                                                        @if($row['sw_washing_price'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="washing__{{$row['sw_washing_price']}}"/>
                                                                {!!__('home.pricing_washing') !!}
                                                            </label>
                                                        @endif
                                                        @if($row['sw_press'] != '-')
                                                            <label>
                                                                <input type="radio" name="service_price"
                                                                       value="press__{{$row['sw_press']}}"/>
                                                                {!!__('home.pricing_press') !!}
                                                            </label>
                                                        @endif
                                                        <button class="btn btn-primary"
                                                                onclick="cartAddItem('{{$row['id']}}', '{{$row['name']}}')">
                                                            {!!__('home.submit_now') !!}

                                                        </button>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td colspan="5" style='text-transform: capitalize'>
                                        <strong>Disclaimer:</strong> <br/>
                                        Delivery Fee is 10 SAR<br/>
                                        Price is Subject to Change<br/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section id="community" class="bg-white fontstyle">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <p class="heading-white">
                        {{ __('home.community') }}
                    </p>
                    <hr class="primary">
                    <p class="heading-white" style="font-weight: 700">
                        <strong>{{ __('home.community_collection') }}</strong>
                    </p>
                </div>
                <div class="col-lg-6 text-left">

                </div>
            </div>
        </div>
    </section>
    <section class="bg-white fontstyle" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1>
                        {{ __('home.contact') }}
                    </h1>
                </div>
            </div>
        </div>
        <div class="contact-form-container relative">
            <div id="map-contact" class=""></div>
            <div class="container pull-right contact-form relative">
                <div class="col-md-6 pull-right no-padding">
                    <form id="contactus" method="POST" files = 'true' action="{{route('contactUs')}}">
                        <h2 class="heading-white text-center">{{ __('home.contact') }}</h2>
                        <div id="message"></div>
                        <div class="col-md-6 form-group" style="padding:0px 5px 0px 0px">
                            <input class="form-control" id="name" name="name" placeholder="{{ __('home.name') }}"
                                    type="text">
                        </div>

                        <div class="col-md-6 form-group" style="padding:0px 0px 0px 5px">
                            <input class="form-control" id="contactemail" name="email"
                                   placeholder="{{ __('home.email') }}" required="" type="email">
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="subject" name="subject"
                                   placeholder="{{ __('home.contact_subject') }}" required="" type="text">
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" id="contactmessage" name="message" rows="4"
                                      placeholder="{{ __('home.contact_msg') }}" required=""></textarea>
                        </div>
                        <div class="form-group" align='center'>
                           
                              {!! app('captcha')->display()!!}
                            
                            <button type="submit" class="btn btn-primary contact-btn" id="load"
                                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{ __('home.processing') }}">{{ __('home.send') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-white footer fontstyle">
        <div class="container">
            <div class="col-md-6">
                <img src="img/logo.png"/>
                <hr/>
                <p>
                    {{ __('home.footer') }}   </p>
            </div>
            <div class="col-md-6">
                <div class="mb34"></div>
                <h3>
                    {{ __('home.footer-contact') }}
                </h3>
                <hr/>
                <ul class="contact-footer">
                    @if($code=='SA')
                        <li>
                            {{ __('home.footer_KSA') }}
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>+966 54 777 1807
                        </li>
                        <br class="lightSpeedIn"/>
                    @elseif($code=='PK')
                        <li>
                            {{ __('home.footer_PK') }}
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>+92 321 845 0305
                        </li>
                        <br/>
                    @elseif($code=='USA')
                        <li>
                            {{ __('home.footer_USA') }}
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>+1 313 649 1173
                        </li>
                        <br/>
                    @else
                        <li>
                            {{ __('home.footer_KSA') }}
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>+966 54 777 1807
                        </li>
                    @endif
                    <li>
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:info@smartwashr.com">info@smartwashr.com</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@stop
@section('js')
    {!! Html::script('js/maps.js') !!}
    {!! Html::script('js/camera.min.js') !!}
    {!! Html::script('js/contactUs.js') !!}
    <script>
        $(document).ready(function () {
            //Camera
            $('.slider-camera').camera({//here I declared some settings, the height and the presence of the thumbnails
                height: '400px',
                pagination: false,
                thumbnails: false
            });
        });
    </script>
@endsection