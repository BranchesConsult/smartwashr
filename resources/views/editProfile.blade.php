@extends('layouts.app')
@section('title')
Profile
@stop
@section('content')
<section class="bg-white content-section">
    <style>
        .profile {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;
        }

        .profile:hover {
            box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
        }
    </style>
    <div class="container">
        <div class="box box-primary">
            @if(session('message'))
            <div class="alert alert-success" style='background-color: #009900;'>
                <p>{{session('message')}}</p>
            </div>
            @endif
            @if($errors && ! $errors->isEmpty() )
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {!! Form::open(['url' => route('postUpdateProfile'), 'files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="box-header with-border">
                            <h3 class="box-title">Personal Profile</h3>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input value="{{$swUser['name']}}" class="form-control" id="first_name" placeholder="Enter first name" type="text" name="first_name">
                            <span class="text-danger">{!! $errors->first('Name') !!}</span>
                        </div>

                        <div class="form-group">
                            
                                <label>Phone Number</label>
                                <input required value="{{$swUser['phone_number']}}"  type="tel" name="phone" id="phone" class='form-control' placeholder='Phone Number'/>
                            
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Profie Picture(Optional)</label>
                                <input placeholder="Attach your Picture" type="file" name="pic" id="pic" value="/uploads/{{$swUser['profile_pic']}}">
                            </div>
                        </div>
                        <div class="form-group">
                                @if(!empty($swUser['profile_pic']))
                                <a target="_blank" href="uploads/{{$swUser['profile_pic']}}">
                                    <img class='profile' src="uploads/{{$swUser['profile_pic']}}" alt="Profile_pic" style="width:150px">
                                </a>
                                @endif
                        </div>  
                        <div class="form-group">
                            <div>
                                <label>Password</label>
                                <input type="password" name="password" id="password" class='form-control'  placeholder='Password'/>
                            </div>    
                        </div>
                        <div class="form-group">
                            <div>
                                <label>Retype Password</label>
                                <input class="form-control" type='password' placeholder="Retype Password" name="password_confirmation" id='password_confirmation'>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            </section>
            @stop
