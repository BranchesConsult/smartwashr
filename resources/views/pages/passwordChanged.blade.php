@extends('layouts.app')
@section('title')
Password Updated
@stop
@section('content')
<style>
    .content-section{
        min-height: 68%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Password Change Request
            <hr class="light" />
        </h1>
        <p>
            Your password has been updated successfully!!!
        </p>
    </div>
</section>
@stop

