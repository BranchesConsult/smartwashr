@extends('layouts.app')
@section('title')
Home
@stop
@section('content')
<style>
    .content-section{
        min-height: 68%;
    }
    .resend{
        margin-top: 25px;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        @if(session('message'))
        <div class="alert alert-success">
            <h4>{{session('message')}}</h4>
        </div>
        @endif
        <h1>
            Welcome to Smartwashr
            <hr class="light" />
        </h1>
        <p>
            You have not verified your email yet.
            Kindly verify your email to proceed. 
            <br/>
            <a class='btn btn-primary resend'href="{{route('resendEmail')}}">Resend Email</a>
        </p>
    </div>
</section>
@stop
