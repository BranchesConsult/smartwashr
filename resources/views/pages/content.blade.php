@extends('layouts.app')
@section('title')
Profile
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h2>
            <?php
            echo \Session::get('language') == 'en' ?
                    $page['title'] : $page['menu']['title_ar'];
            ?>
            <hr class="light" />
        </h2>
        <p>
            <?php
            echo \Session::get('language') == 'en' ?
                    $page['content'] : $page['content_ar'];
            ?>
        </p>
    </div>
</section>
@stop
