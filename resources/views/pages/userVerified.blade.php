@extends('layouts.app')
@section('title')
Home
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Account Verified
            <hr class="light" />
        </h1>
        <p>
           Welcome to Smartwashr<br/>
           Thanks for verifying your account. 
        </p>
    </div>
</section>
@stop
