@extends('layouts.app')
@section('title')
Home
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Registration Request Received
            <hr class="light" />
        </h1>
        <p>
            Thank you for registration, an email has been sent on your account. 
            <br/>
            Kindly verify your email to proceed.
        </p>
    </div>
</section>
@stop
