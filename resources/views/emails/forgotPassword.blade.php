@extends('emails.emailLayout')
@section('content')
    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <h2 style="margin:0px;padding: 9px 0;">Welcome</h2>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <p>
                                Your password change request has been received , kindly click the below link:<br>
                                <a href="smartwashr.com/password/reset"
                                   style="display: block;
                                                                width: 115px;
                                                                height: 20px;
                                                                background: #4db748;
                                                                padding: 10px;
                                                                text-align: center;
                                                                border-radius: 5px;
                                                                color: white;
                                                                font-weight: bold;
                                                                text-decoration:none;
                                                                padding-top: 0.35cm;
                                                                margin-top: 6px;">
                                    Login here!
                                </a>

                                <span style='color:#222222;padding:8px 0;display: inline-block;'> Regards,</span>

                                <br>
                                <img src={{URL::to('/img/logo.png')}} alt=""/>
                                <span style='color:#222222;padding:8px 0;display: inline-block;'>Smartwashr Team</span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@stop
                   