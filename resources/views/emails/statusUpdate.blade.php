@extends('emails.emailLayout')
@section('content')
    <?php
    $orderStatuses = [
        0 => 'Your order is now Pending',
        1 => 'Your order has been Received',
        2 => 'Your order is In-Progress',
        3 => 'Your order has been Completed and will be delivered soon',
        4 => 'Your order has been Delivered, Thank you for your loyalty',
        5 => 'Your order has been Cancelled',
        6 => 'Your order is Ready For Delivery',
    ];
    ?>
    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td height='15'></td>
            </tr>
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center' style='padding-bottom: 5px;'>
                            Dear {{$name}}, <br><br>
                            <?php
                            echo str_replace('Your order', 'Your order ' . str_pad($order_id, 6, "0", STR_PAD_LEFT), $orderStatuses[$status]);
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@stop
        