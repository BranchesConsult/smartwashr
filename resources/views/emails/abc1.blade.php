<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Smartwashr Registration</title>
        {!! Html::style('fonts/fontawesome-webfont.woff') !!}
        {!! Html::style('fonts/fontawesome-webfont.ttf') !!}
        <style type="text/css">
            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }
            p{
                text-align:left;
                color:#222222;
                font-size:14px;
                font-weight:normal;
                line-height:19px;
            }

            a.link1{
                color:#382F2E;
            }
            a.link2{
                font-size:16px;
                text-decoration:none;
                color:#ffffff;
            }

            h2{
                text-align:left;
                color:#222222;; 
                font-size:19px;
                font-weight:normal;
            }
            div,p,ul,h1{
                margin:0;
            }

            .bgBody{
                background: #ffffff;
            }
            .bgItem{
                background: #ffffff;
            }
            .green_area {
                width: 100%;
                display: inline-block;
                background: #4db748 !important;
                padding: 10px 0px;
                float: left;
            }
        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"ffffff",
            "link":"382F2E",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"222222"
            }
        </script>
    </head>
    <body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
        <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center"  style='font-family:Helvetica, Arial,serif;'>
            <tbody>
                <td>
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            </td>
                        </tr>
                        <tr>
                            <td height='75' class="spechide"></td>
                        <tr>
                            <td class='movableContentContainer ' valign='top'>
                                <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            </td>
                        </tr>
                        <tbody>
                            <tr>
                                <div class="green_area">
                                    <span class="text" style="color: white;    padding: 0 10px;">
                                        Welcome To SmartWashr!
                                    </span>
                                </div>
                            </tr>
                        </tbody>       
                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align='left'>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align='center'>
                                                <h2 style="margin:0px;padding: 9px 0;">Welcome</h2>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align='left'>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align='center'>
                                                <p>
                                                    We are deligthed to welcome you on Smartwashr.Your Credentials are,<br>
                                                        <strong>Username:</strong> abc<br>
                                                        <strong>Password:</strong> password<br>
                                                            <a href=""
                                                                style="display: block;
                                                                width: 115px;
                                                                height: 20px;
                                                                background: #4db748;
                                                                padding: 10px;
                                                                text-align: center;
                                                                border-radius: 5px;
                                                                color: white;
                                                                font-weight: bold;
                                                                text-decoration:none;
                                                                padding-top: 0.35cm;
                                                                margin-top: 6px;">
                                                                Login here!
                                                            </a>
                                                       
                                                            <span style='color:#222222;padding:8px 0;display: inline-block;'> Regards,</span>
                                                            
                                                            <br>
                                                                <img src={{URL::to('/img/logo.png')}} alt=""/>
                                                                <span style='color:#222222;padding:8px 0;display: inline-block;'>Smartwashr Team</span>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #DDDDDD;">                                                  
                    <tbody>
                    
                        <tr>
                        <td valign="top" class="specbundle">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" align='center'>
                                    <p  style='text-align:left;font-size:14px;
                                        font-weight:normal;line-height:20px;'>
                                            <span style='font-weight:bold;'></span>
                                                <br/>
                                            Copyright&copy; - {{date('Y')}}.All rights reserved.
                                                <br/>
                                    </p>
                                </div>
                            </div>
                        </td>
                        <td valign='top' width='52'>
                            <div class="contentEditableContainer contentFacebookEditable">
                                <div class="contentEditable">
                                    <a target='_blank' href="#"><img style="padding-top: 10px;"src="{{URL::to('/img/facebook.png')}}" width='40' height='40' alt='facebook icon' data-default="placeholder" data-max-width="52" data-customIcon="true"></a>
                                </div>
                            </div>
                        </td>
                        <td valign="top" width="16">&nbsp;</td>
                            <td valign='top' width='52'>
                                <div class="contentEditableContainer contentFacebookEditable">
                                    <div class="contentEditable">
                                        <a target='_blank' href="#"><img style="padding-top: 10px;"src="{{URL::to('/img/twitter.png')}}" width='40' height='40' alt='twitter icon' data-default="placeholder" data-max-width="52" data-customIcon="true"></a>
                                    </div>
                                </div>
                            </td>
                         
                        </tr>
                       
                </div>
            </tbody>
        </table>
    </body>
</html>
