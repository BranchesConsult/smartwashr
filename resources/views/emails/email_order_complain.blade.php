@extends('emails.emailLayout')
@section('content')
    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>

                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td height='15'></td>
            </tr>
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center' style='padding-bottom: 5px;'>
                            Hello, <br><br>
                            {{ $feedback }}
                        </div>
                    </div>
                </td>
            </tr>

        </table>
    </div>
@stop
