<style>
    td {
        text-align: left;
        padding: 6px;
    }

    * {
        color: #626065;
        font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
        font-size: 12px;

    }

    .seperate-height {
        clear: both;
        height: 30px;
    }

    tr.heading {
        background: #1685EB;
    }

    tr.heading th {
        color: #fff;
        padding: 5px;
    }

    .txt-head {
        color: #1685EB;
        font-weight: bold;
    }

    .summery {
        line-height: 22px;
    }
</style>
<table width="100%">
    <tr>
        <td colspan="2" align="center">
            <img src="{{URL::to('img/logo_new.png')}}"/>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td style="vertical-align: top;">
                        <img src="{{URL::to('img/location-icon-blue.png')}}" width="16px" height="16px"/>
                    </td>
                    <td style="vertical-align: top;">
                        3333 Arafat St.<br/>
                        AlAndalus District<br/>
                        AlShahwan Commercial Center 4th Floor
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="vertical-align: top;">
                        <img src="{{URL::to('img/phone_font.png')}}" width="16px" height="16x"/>
                    </td>
                    <td style="vertical-align: top;">
                        +966 54 777 1807
                    </td>
                </tr>
            </table>
        </td>
        <td style="vertical-align: top;">
            <p style="text-align: right;">
                Invoice #: {!! str_pad($id,6,"0",STR_PAD_LEFT) !!}<br/>
                Created: {!! date("F, j, Y") !!}
            </p>
        </td>
    </tr>
</table>
<div class="seperate-height"></div>
<table width="100%">
    <tr style="background: #1685EB">
        <th style="color: #fff; padding: 5px;">
            Payment method
        </th>
    </tr>
    <tr>
        <td>
            Cash on delievery
        </td>
    </tr>
</table>
<div class="seperate-height"></div>
<table width="100%">
    <tr class="heading">
        <th>
            Product
        </th>
        <th>
            Service
        </th>
        <th>
            Qty.
        </th>
        <th>
            Unit Price
        </th>
        <th>
            Total Price
        </th>
    </tr>
    <?php $sub_total = 0;?>
    @foreach($pdf as $item)
        <tr>
            @foreach($item['product'] as $row)
                <td>
                    {{$row['name']}}
                </td>
            @endforeach
            <td>
                {{ucfirst($item['product_service'])}}
            </td>
            <td>
                {{$item['quantity']}}
            </td>
            <td>
                {{$item['sw_price']/$item['quantity']}}
            </td>
            <td>
                {{$item['sw_price']}}
            </td>
            <?php $sub_total += $item['sw_price']; ?>
        </tr>
    @endforeach
    <tr style="border-top: 2px solid #CECECE;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="summery">
            <span class="txt-head">Delivery Charges:</span> SAR {{config('const.delivery_charges')}}<br/>
            <span class="txt-head">Total:</span> SAR {{$sub_total+config('const.delivery_charges')}}<br/>
        </td>
    </tr>
</table>