@extends('emails.emailLayout')
@section('content')
    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <h2 style="margin:0px;padding: 9px 0;">Order placed</h2>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <p>
                                Dear {{$name}},<br/>
                                You have one order from client. His details are:
                                Order Id: {{$order_id}}<br/>
                                Phone: {{$client_phone}}<br/>
                                Email: {{$client_email}}<br/>
                                StreetAddress: {{$client_address}}<br/>
                                Notes for laundry: {!! (!empty($laundry_instructions)) ? $laundry_instructions : 'N/A' !!}
                                Map: <a href="{{$user_glocation}}">{{$user_glocation}}</a><br/>
                                Collection time: {{$collection_date_time}}<br/>
                                Delievery time: {{$delivery_date_time}}<br/>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@stop
