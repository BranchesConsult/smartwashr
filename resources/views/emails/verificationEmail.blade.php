@extends('emails.emailLayout')
@section('content')
                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align='left'>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align='center'>
                                                <h2 style="margin:0px;padding: 9px 0;">Welcome</h2>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align='left'>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align='center'>
                                                <p >
                                   Verify your email to proceed.
                                    <a href="{{route('verificationEmail',['email'=>$email])}}"
                                                                style="display: block;
                                                                width: 115px;
                                                                height: 20px;
                                                                background: #4db748;
                                                                padding: 10px;
                                                                text-align: center;
                                                                border-radius: 5px;
                                                                color: white;
                                                                font-weight: bold;
                                                                text-decoration:none;
                                                                padding-top: 0.35cm;
                                                                margin-top: 6px;">
                                                                Click here!
                                                            </a>
                                    <br>
                                    Regards,
                                    <br>
                                        <br>
                                            <img src={{URL::to('/img/logo.png')}} alt=""/></br>
                                    <span style='color:#222222;'>Smartwashr Team</span>
                                  </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
@stop
                   