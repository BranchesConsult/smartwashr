@extends('layouts.app')
@section('title')
Order Details
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
    .order{
        background-color: #48db44; 
        color:white;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Order Detail
            <hr class="light" />
        </h1>
        <table class="table table-hover" style="border: 1px solid black; border-collapse: collapse;" >
            <thead class='order'>
                <tr>
                    <th>S.No.</th>
                        <th>Price</th>
                        <th>Product</th>
                        <th>Product Service</th>
                        <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                ?>
                @foreach($orderDetail as $item)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$item['sw_price']}}</td>
                    @foreach($item['product'] as $row)
                        <td><img class="img-thumbnail" style='width:50px;height:50px;'
                                 src="{{$row['picture']}}" />
                            {{$row['name']}}</td>
                    @endforeach
                    <td>{{$item['product_service']}}</td>
                    <td>{{$item['quantity']}}</td>
                </tr>
                <?php
                $count++;
                ?>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@stop
