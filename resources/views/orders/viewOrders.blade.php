@extends('layouts.app')
@section('title')
Orders History
@stop
@section('content')
<style>
    .content-section{
        min-height: 100%;
    }
    .order{
        background-color: #48db44; 
        color:white;
    }
</style>
<section class="bg-white content-section">
    <div class="container">
        <h1>
            Orders
            <hr class="light" />
        </h1>
        @if(count($order)==0)
        <div class="alert alert-success">
            You haven't placed any order yet.
        </div>
        @else
        <table class="table table-hover" style="border: 1px solid black; border-collapse: collapse;" >
            <thead class='order'>
                <tr>
                    <th>S.No</th>
                    <th>Collection datetime</th>
                    <th>Delivery datetime</th>
                    <th>Status</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                ?>
                @foreach($order as $item)
                <tr>
                    <td>{{$count}}</td>                    
                    <td>{{date('F d, Y h:mA', strtotime($item['collection_date_time']))}}</td>
                    <td>{{date('F d, Y h:mA',strtotime($item['delivery_date_time']))}}</td>
                    <td><?php if ($item['status']==0) echo 'Pending'; 
                            elseif($item['status']==1) echo 'Received';
                            elseif($item['status']==2) echo 'In-Progress';
                            elseif($item['status']==3) echo 'Completed';
                            elseif($item['status']==4) echo 'Delivered';
                            else echo 'Cancelled';?></td>
                    <td>{{$item['sw_total_price']}} SAR</td>
                    <td>
                        <a class='btn btn-success' href="{{route('orderDetail',['order_id'=>$item['id']])}}">
                            View Detail</a>
                    </td>
                </tr>
                <?php
                $count++;
                ?>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</section>
@stop
