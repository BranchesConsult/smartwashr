<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//Route::get('/lang', 'PageController@numToArabic');
//Route::get('/lang/{lang}', 'LanguageController@index')->name('lang');


Route::get('test-reciept', 'Api\OrdersController@testReciept');

Route::get('/receipt', function () {
    return view('emails.abc1');
});
Route::any('/sendreceipt', 'OrderController@sendReceipt');
Route::get('/getDriver', 'CartController@getNearByDriver');
Route::any('resend-email', 'PageController@resendEmail')->name('resendEmail');
Route::any('cart/sendreceipt', 'OrderController@sendReceipt');

Route::group(['middleware' => 'client_verified'], function () {
    Route::get('/profile', 'PageController@editProfileForm')->name('account');
    Route::post('/profile', 'PageController@editProfile')->name('postUpdateProfile');
    Route::get('/order', 'OrderController@order')->name('order');
    Route::get('order/{order_id}', 'OrderController@orderDetail')->name('orderDetail');
    Route::group(['prefix' => 'cart', 'middleware' => 'auth'], function () {
        Route::post('add-item', 'CartController@addItem')->name('addItemToCart');
        Route::get('your-info', 'CartController@userInfo')->name('getUserInfo');
        Route::get('detail', 'CartController@cartDetail');
        Route::get('count-contents', 'CartController@countCartContents')->name('countCartContents');
        Route::get('update-qty', 'CartController@updateQty')->name('updateQty');
        Route::get('delete-product', 'CartController@deleteCartProduct')->name('deleteCartProduct');
        Route::post('complete', 'CartController@orderComplete')->name('orderComplete');
        Route::get('near-laundry', 'CartController@getNearByLaundry')->name('getNearByLaundry');
        Route::get('driver-availibility', 'CartController@getNearByDriver')->name('getDriverInLocation');
        Route::get('get-location', 'CartController@getUserLocation')->name('getUserLocation');
    });
});
Route::get('/country', 'PageController@country');
Route::post('/', 'LanguageController@language')->name('changeLanguage');
Route::get('/', 'PageController@home')->name('home');
Route::post('contactUs', 'ContactUsController@contactUs')->name('contactUs');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/registration_request', 'PageController@registrationRequest')->name('registrationRequest');
Route::get('/convert', 'PageController@convert')->name('convert');
Route::group(['middleware' => 'web'], function () {
    Route::post('user/login', 'Auth\LoginController@ajax_login')->name('ajaxLogin');
    Route::get('/registration/activate/{code}', 'ContactUsController@activate')->name('activate');
    Route::get('/welcome', function () {
        return view('emails.abc');
    });
});
Route::any('/verifyUser/{email}', 'PageController@verifyUser')->name('verification');
Route::any('/verifyEmail/{email}', 'PageController@verifyEmail')->name('verificationEmail');
Route::group(['prefix' => 'admin', 'middleware' => 'web'], function () {
    Route::get('/', 'Auth\LoginController@showAdminLoginForm');
    Route::group(['middleware' => 'is_admin'], function () {
        Route::get('voted-areas', 'Admin\VotedAddressController@index')->name('getVotedAddress');
        //Route::group(['middleware' => 'is_laundry'], function () {
        //ORDER routes for admin
        Route::get('/order', 'Admin\OrderController@viewOrders')->name('viewOrders');
        Route::get('/editorder/{order_id}', 'Admin\OrderController@editOrderForm')->name('editOrderForm');
        Route::post('/editorder/{order_id}', 'Admin\OrderController@editOrder')->name('editOrder');
        Route::get('/orderdetail/{order_id}', 'Admin\OrderController@orderDetail')->name('viewOrderDetail');
        Route::get('/seen/notification', 'Admin\OrderController@notificationSeen')->name('orderSeenByAdmin');
        //LAUNDRY routes for admin
        Route::get('/laundry', 'Admin\LaundryController@viewLaundry')->name('viewLaundry');
        Route::get('/addlaundry', 'Admin\LaundryController@addLaundryForm')->name('addLaundry');
        Route::post('/addlaundry', 'Admin\LaundryController@addUpdateLaundry')->name('postAddLaundry');
        Route::get('/editlaundry/{laundry_id}', 'Admin\LaundryController@editLaundryForm')->name('editLaundry');
        Route::post('/editlaundry/{laundry_id}', 'Admin\LaundryController@addUpdateLaundry')->name('postEditLaundry');
        Route::get('/deletelaundry/{laundry_id}', 'Admin\LaundryController@deleteLaundry')->name('deleteLaundry');
        //USER routes for admin
        Route::get('/users', 'Admin\UserController@viewUsers')->name('viewUsers');
        Route::get('/adduser', 'Admin\UserController@addUserForm')->name('addUser');
        Route::post('/adduser', 'Admin\UserController@addUser')->name('postAddUser');
        Route::get('/edituser/{user_id}', 'Admin\UserController@editUserForm')->name('editUserForm');
        Route::post('/edituser/{user_id}', 'Admin\UserController@editUser')->name('postEditUser');
        Route::get('/deleteuser/{user_id}', 'Admin\UserController@deleteUser')->name('deleteUser');
        Route::get('/attachlaundry/{user_id}', 'Admin\UserController@attachLaundryForm')->name('attachLaundry');
        Route::post('/attachlaundry/{user_id}', 'Admin\UserController@attachLaundry')->name('postAttachLaundry');
        //PRODUCT routes for admin
        Route::get('/addproduct', 'Admin\ProductController@addProductForm')->name('addProduct');
        Route::post('/addproduct', 'Admin\ProductController@addUpdateProduct')->name('postAddProduct');
        Route::get('/editproduct/{product_id}', 'Admin\ProductController@editProductForm')->name('editProduct');
        Route::post('/editproduct/{product_id}', 'Admin\ProductController@addUpdateProduct')->name('postEditProduct');
        Route::get('/deleteproduct/{product_id}', 'Admin\ProductController@deleteProduct')->name('deleteProduct');
        Route::get('/products', 'Admin\ProductController@viewProducts')->name('viewProducts');
        //CATEGORY routes for admin
        Route::get('/addcategory', 'Admin\CategoryController@addCategoryForm')->name('addCategory');
        Route::post('/addcategory', 'Admin\CategoryController@addUpdateCategory')->name('postAddCategory');
        Route::get('/editcategory/{category_id}', 'Admin\CategoryController@editCategoryForm')->name('editCategory');
        Route::post('/editcategory/{category_id}', 'Admin\CategoryController@addUpdateCategory')->name('postEditCategory');
        Route::get('/deletecategory/{category_id}', 'Admin\CategoryController@deleteCategory')->name('deleteCategory');
        Route::get('/category', 'Admin\CategoryController@viewCategories')->name('viewCategories');
        //PAGES routes for admin
        Route::get('/pages', 'Admin\PageController@loadPages')->name('viewPages');
        Route::get('/addpage', 'Admin\PageController@addPageForm')->name('addPage');
        Route::post('page/add', 'Admin\PageController@addPage')->name('postAddPage');
        Route::get('page/edit/{id}', 'Admin\PageController@editPage')->name('editPage');
        Route::post('page/edit/{id}', 'Admin\PageController@updateEditPage')->name('postEditPage');
        Route::get('page/delete/{id}', 'Admin\PageController@deletePage')->name('deletePage');
        //MENU routes for admin
        Route::get('/addmenu', 'Admin\MenuController@addMenuForm')->name('addMenu');
        Route::get('/menus', 'Admin\MenuController@loadMenus')->name('viewMenus');
        Route::post('/menu/add', 'Admin\MenuController@addMenu')->name('postAddMenu');
        Route::get('menu/edit/{id}', 'Admin\MenuController@editMenu')->name('editMenu');
        Route::post('menu/edit/{id}', 'Admin\MenuController@updateEditMenu')->name('postEditMenu');
        Route::get('menu/delete/{id}', 'Admin\MenuController@deleteMenu')->name('deleteMenu');
        //CAR routes for admin
        Route::get('/addcar', 'Admin\CarController@addCarForm')->name('addCar');
        Route::get('/cars', 'Admin\CarController@loadCars')->name('viewCars');
        Route::post('/car/add', 'Admin\CarController@addCar')->name('postAddCar');
        Route::get('car/edit/{id}', 'Admin\CarController@editCar')->name('editCar');
        Route::post('car/edit/{id}', 'Admin\CarController@updateEditCar')->name('postEditCar');
        Route::get('car/delete/{id}', 'Admin\CarController@deleteCar')->name('deleteCar');
        //Coupon code
        Route::get('coupons', 'Admin\CouponController@index')->name('all_coupons');
        Route::get('coupons/add', 'Admin\CouponController@add')->name('add_coupons');
        Route::post('coupons/add', 'Admin\CouponController@addPost')->name('add_coupons');
        Route::get('coupons/edit/{id}', 'Admin\CouponController@edit')->name('edit_coupons_view');
        Route::post('coupons/edit/{id}', 'Admin\CouponController@postEdit')->name('edit_coupons');
//Push notification
        Route::post('push/view', 'Admin\PushNotificationController@sendToAll')->name('push.frm');
        Route::get('push/view', 'Admin\PushNotificationController@frmSendToAll')->name('push.frm');
    });
});

//ROUTES FOR LAUNDRY
Route::group(['prefix' => 'laundry', 'middleware' => 'web'], function () {
    Route::group(['middleware' => 'is_laundry'], function () {
        //ORDER routes for laundry
        Route::get('/order', 'Admin\OrderController@viewOrders')->name('viewOrders');
        Route::get('/editorder/{order_id}', 'Admin\OrderController@editOrderForm')->name('editOrderForm');
        Route::post('/editorder/{order_id}', 'Admin\OrderController@editOrder')->name('editOrder');
        Route::get('/orderdetail/{order_id}', 'Admin\OrderController@orderDetail')->name('viewOrderDetail');
        Route::get('/seen/notification', 'Admin\OrderController@notificationSeen')->name('orderSeenByAdmin');
        //LAUNDRY routes for laundry
        Route::get('/laundry', 'Admin\LaundryController@viewLaundry')->name('viewLaundry');
        Route::get('/addlaundry', 'Admin\LaundryController@addLaundryForm')->name('addLaundry');
        Route::post('/addlaundry', 'Admin\LaundryController@addUpdateLaundry')->name('postAddLaundry');
        Route::get('/editlaundry/{laundry_id}', 'Admin\LaundryController@editLaundryForm')->name('editLaundry');
        Route::post('/editlaundry/{laundry_id}', 'Admin\LaundryController@addUpdateLaundry')->name('postEditLaundry');
        Route::get('/deletelaundry/{laundry_id}', 'Admin\LaundryController@deleteLaundry')->name('deleteLaundry');
        //USER routes for laundry
        Route::any('/users', 'Admin\UserController@viewUsers')->name('viewUsers');
        //Route::post('/searchuser', 'Admin\UserController@searchUser')->name('searchUser');
        Route::get('/adduser', 'Admin\UserController@addUserForm')->name('addUser');
        Route::post('/adduser', 'Admin\UserController@addUser')->name('postAddUser');
        Route::get('/edituser/{user_id}', 'Admin\UserController@editUserForm')->name('editUserForm');
        Route::post('/edituser/{user_id}', 'Admin\UserController@editUser')->name('postEditUser');
        Route::get('/deleteuser/{user_id}', 'Admin\UserController@deleteUser')->name('deleteUser');
        //PRODUCT routes for laundry
        Route::get('/products', 'Admin\ProductController@viewProducts')->name('viewProducts');
        //CATEGORY routes for laundry
        Route::get('/category', 'Admin\CategoryController@viewCategories')->name('viewCategories');
    });
});
Route::get('/{slug}', 'PageController@page')->name('page');

