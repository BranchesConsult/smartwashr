<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//'middleware' => 'auth:api'
Route::group(['prefix' => 'v1'], function () {
    Route::get('page/{slug}', 'Api\ContactController@getStaticPage');
    /**
     * Auth
     */
    Route::post('user-social-login', 'Api\UserController@userRegisterOrLogin');
    Route::post('login', 'Api\UserController@authenticate');
    Route::any('register', 'Api\UserController@create');
    Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::any('/sendpush', 'Api\OrdersController@sendPushMessage');
    Route::post('/resend-email', 'Api\UserController@resendEmail')->name('resendEmail');
    /**
     * Product
     */
    Route::get('product', 'Api\ProductController@index');
    /**
     * Laundries
     */
    //Route::post('nearest-laundry', 'Api\LaundryController@getNearByLocation');
    Route::get('laundries', 'Api\LaundryController@laundries');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('contact-feedback', 'Api\ContactController@ContactFeed');
        Route::get('order/get-detail/{order_id}', 'Api\OrdersController@getById');
        Route::get('order/cancel', 'Api\OrdersController@cancelOrder');
        Route::resource('order', 'Api\OrdersController');
        Route::post('change/password', 'Api\UserController@changePasswordWhenLogin');
        Route::get('client-orders', 'Api\OrdersController@orderHistory');
        Route::get('delivered-orders', 'Api\OrdersController@delieveredOrders');
        Route::post('password/change', 'Api\UserController@changePassword');
        Route::post('nearest-drivers', 'Api\UserController@getNearByDriver');
        Route::get('order-history', 'Api\DriverController@orderHistory');
        Route::get('client-location/{order_id}', 'Api\DriverController@clientLocation');
        Route::post('driver-orders', 'Api\DriverController@searchOrder');
        Route::group(['prefix' => 'driver'], function () {
            Route::get('orders-drivers/{status_id?}', 'Api\DriverController@driverOrders');//Driver orders with status
        });
        Route::group(['prefix' => 'order'], function () {
            //    Route::get('/', 'Api\OrdersController@getById');
            Route::post('update-status', 'Api\OrdersController@updateOrderStatus');
            Route::post('reschedule', 'Api\OrdersController@rescheduleOrder');
        });
        Route::any('update_profile', 'Api\UserController@updateProfile');
        Route::resource('user', 'Api\UserController');
        Route::resource('driver', 'Api\DriverController');
        Route::delete('delete-user', 'Api\UserController@deleteUser');

        //Addresses
        Route::resource('user-addresses', 'Api\UserAddressController');
        Route::post('user-addresses-manual-edit', 'Api\UserAddressController@updateWithId');
        Route::get('user-address-delete/{id}', 'Api\UserAddressController@destroy');

        //Spacial for ios
        Route::post('user-add-address', 'Api\UserAddressController@store');


        Route::post('user-addresses/edit/{id}', 'Api\UserAddressController@updateManual');
        Route::post('vote-location', 'Api\UserAddressController@voteAddress');
        Route::post('order-complain', 'Api\OrdersController@addOrderComplain');

        //Coupon
        Route::post('add-coupon', 'Api\OrdersController@apply_coupon');
    });
});