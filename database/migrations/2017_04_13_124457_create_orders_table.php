<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable();
            $table->integer('laundry_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('is_read_admin')->default(0);
            $table->tinyInteger('is_read_laundry')->default(0);
            $table->integer('driver_id')->unsigned();
            $table->string('payment_method', 25)->nullable();
            $table->double('total_price');
            $table->double('sw_total_price');
            $table->double('commission');
            $table->integer('discount');
            $table->string('discount_type',100);
            $table->string('post_code', 20);
            $table->string('latitude', 50);
            $table->string('longitude', 50);
            $table->text('address_one');
            $table->text('address_two');
            $table->text('delivery_instruction');
            $table->tinyInteger('status')->default(0)->comment='0 for pending,1 for received,'
                    . '2 for in-progress,3 for completed,4 for delivered,5 for cancel';
            $table->timestamp('collection_date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('delivery_date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('orders', function($table) {
            $table->foreign('laundry_id')->references('id')->on('laundry')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('orders');
    }

}
