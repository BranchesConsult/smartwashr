<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('activation_code',255)->nullable();
            $table->boolean('active')->default(false);
            $table->string('password');
            $table->string('phone_number')->nullable();
            $table->string('profile_pic')->nullable();
            $table->tinyInteger('is_admin')->default(0);
            $table->string('st1')->nullable();
            $table->string('st2')->nullable();
            $table->string('city')->nullable();
            $table->string('state/province')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->rememberToken()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::dropIfExists('users');
    }

}
