<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Category extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->ENUM('status',[0,1])->nullable();
            $table->integer('parent_id')->default(0);
            $table->string('name')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('description')->nullable();
            $table->string('picture')->nullable();            
            $table->string('slug')->nullable();
            $table->timestamps();
            $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
