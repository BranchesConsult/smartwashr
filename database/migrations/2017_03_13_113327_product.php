<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->ENUM('status', [0, 1]);
            $table->string('name');
            $table->string('name_ar')->nullable();
            $table->string('dryclean_price');
            $table->string('washing_price');
            $table->string('press_price');
            $table->string('sw_dryclean_price');
            $table->string('sw_washing_price');
            $table->string('sw_press_price');
            $table->string('picture')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('product');
    }

}
