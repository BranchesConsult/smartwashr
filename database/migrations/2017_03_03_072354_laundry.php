<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Laundry extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('laundry', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status');
            $table->string('code');
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')->references('id')->on('users');            
            $table->string('email', 50);
            $table->string('phone');
            $table->string('state');
            $table->string('st1');
            $table->string('st2')->nullable();
            $table->string('user_entered_address', 255);//Google address of autofill
            $table->string('city');
            $table->string('country');
            $table->string('latitude', 50);
            $table->string('longitude', 50);
            $table->integer('zip')->nullable();
            $table->string('logo',512)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('laundry');
    }

}
