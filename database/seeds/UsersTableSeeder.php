<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'name' => 'Activation Sources',
            'email' => 'muneeb@branchezconsulting.com',
            'password' => bcrypt('password'),
            'phone_number'=>'00923434696961',
            'profile_pic' => ''
        ]);
    }

}
